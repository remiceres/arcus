# FindSQLite3
# -----------
#
# Try to find the SQLite3 C/C++ database library
#
# Defined variables once found:
#
# ::
#
#   SQLite3_FOUND - System has SQLite3
#   SQLite3_INCLUDE_DIRS - The SQLite3 include directory
#   SQLite3_LIBRARIES - The libraries needed to use SQLite3

find_path(
    SQLite3_INCLUDE_DIRS
    NAMES sqlite3.h
)

mark_as_advanced(SQLite3_INCLUDE_DIRS)

find_library(
    SQLite3_LIBRARIES
    NAMES sqlite3
)

mark_as_advanced(SQLite3_LIBRARIES)

find_package_handle_standard_args(
    SQLite3
    FOUND_VAR SQLite3_FOUND
    REQUIRED_VARS SQLite3_INCLUDE_DIRS SQLite3_LIBRARIES
)
