var indexSectionsWithContent =
{
  0: "acdefghilnopqrstuv~",
  1: "acdehilnopqrsu",
  2: "acdehlopqrsuv",
  3: "acdefghilopqrstuv~",
  4: "g",
  5: "r",
  6: "lv",
  7: "l",
  8: "a"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "defines",
  8: "pages"
};

var indexSectionLabels =
{
  0: "Tout",
  1: "Structures de données",
  2: "Fichiers",
  3: "Fonctions",
  4: "Variables",
  5: "Définitions de type",
  6: "Énumérations",
  7: "Macros",
  8: "Pages"
};

