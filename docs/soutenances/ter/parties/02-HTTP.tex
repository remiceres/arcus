\section{Communication avec les serveurs}

\tikzset{
    >=latex,
    event/.style={
        circle,
        inner sep=2pt
    },
    period/.style={
        rounded rectangle,
        minimum height=4pt,
        inner sep=0pt
    },
    queue elt/.style={
        draw,
        minimum width=.6cm,
        minimum height=.585cm
    },
    small text/.style={
        font=\footnotesize,
        inner xsep=0pt
    },
    httpblock/.style={
        rectangle, draw,
        inner xsep=5pt, inner ysep=0pt, align=center,
        rectangle split, rectangle split horizontal,
        rectangle split parts=2
    }
}

\newcommand{\queue}[3]{
    \path[draw, thick, visible on=<2>]
        coordinate (#1) at ($(#2)+(.4,0)$)
        (#2) --++ (#3, 0)
        ++ (-#3, .6) --++ (#3, 0);
}

\begin{frame}{Objectifs}{Communication sur le protocole HTTP}
\begin{itemize}
    \item HTTP permet à un \alert{client} de manipuler les ressources d'un \alert{serveur.}
    \item Un client soumet une \alert{requête} à un serveur qui lui renvoie une \alert{réponse.}
\end{itemize}

\pause

\begin{center}
\begin{tikzpicture}
    \node[
        rectangle, draw, fill=red!30,
        inner sep=5pt
    ] (HTTP) {HTTP};

    \node[
        fit=(HTTP), inner xsep=.5cm, inner ysep=.3cm,
        draw, thick
    ] (application-frame) {};

    \node[anchor=south] at (application-frame.north)
        {\bfseries\footnotesize ARCUS};

    \node[
        yshift=.25cm,
        label={[below=1cm, name=serveur-file-label]\texttt{/toto.txt}}
    ] (serveur-file) at (11, 0) {\includegraphics{file}};

    \node[
        fit=(serveur-file)(serveur-file-label), minimum width=2cm,
        align=center, draw, thick
    ] (serveur-frame) {};

    \node[anchor=south] at (serveur-frame.north)
        {\bfseries\footnotesize SERVEUR};

    \node[
        visible on=<3->,
        httpblock, yshift=1cm,
        label={[visible on=<3->]above:\textit{\footnotesize Requête}}
    ] (req) at ($(HTTP)!0.5!(serveur-frame)$) {
        \nodepart{one}
            \strut {\footnotesize\bfseries Verbe}\\
            \strut \texttt{GET}
        \nodepart{two}
            \strut {\footnotesize\bfseries URL}\\
            \strut \texttt{/toto.txt}
    };

    \node[
        visible on=<4->,
        httpblock, yshift=-1cm,
        label={[visible on=<4->]above:\textit{\footnotesize Réponse}}
    ] (res) at ($(HTTP)!0.5!(serveur-frame)$) {
        \nodepart{one}
            \strut {\footnotesize\bfseries Code}\\
            \strut \texttt{200}
        \nodepart{two}
            \strut {\footnotesize\bfseries Contenu}\\
            \strut \texttt{Ceci est un exemple}
    };

    \path[visible on=<3->]
        (HTTP) edge[bend left=10] (req)
        (req) edge[->, bend left=10] (serveur-frame);

    \path[visible on=<4->]
        (serveur-frame) edge[bend left=10] (res)
        (res) edge[->, bend left=10] (HTTP);
\end{tikzpicture}
\end{center}
\end{frame}

\begin{frame}{Objectifs}{Fonctionnement asynchrone}
\centering
\tabtitle{Traitement synchrone}\\[1em]
\begin{tikzpicture}
    % Flèche du fil d'exécution
    \path[draw, thick, ->]
        coordinate (main-thread) at (0, -3)
        (0, -3) --++ (13, 0)
        coordinate (main-thread-end);

    % Traitement de la requête A
    \node[
        period, fill=red!60!black, minimum width=3cm,
        right=.5cm of main-thread,
        label={[small text, below=.3cm]Attente}
    ] (traitement-A) {};

    \path[draw, dotted]
        (traitement-A.west) -- ++(0, .4) coordinate (req-A)
        (traitement-A.east) -- ++(0, .4) coordinate (res-A);

    \node[small text, anchor=210] at (req-A) {Requête A};
    \node[small text, anchor=330] at (res-A) {Réponse A};

    % Traitement de la requête B
    \node[
        period, fill=blue!60!black, minimum width=3.5cm,
        right=1.2cm of traitement-A,
        label={[small text, below=.3cm]Attente}
    ] (traitement-B) {};

    \path[draw, dotted]
        (traitement-B.west) -- ++(0, .4) coordinate (req-B)
        (traitement-B.east) -- ++(0, .4) coordinate (res-B);

    \node[small text, anchor=210] at (req-B) {Requête B};
    \node[small text, anchor=330] at (res-B) {Réponse B};

    % Traitement de la requête C
    \node[
        period, fill=yellow!60!black, minimum width=3cm,
        right=1cm of traitement-B,
        label={[small text, below=.3cm]Attente}
    ] (traitement-C) {};

    \path[draw, dotted]
        (traitement-C.west) -- ++(0, .4) coordinate (req-C)
        (traitement-C.east) -- ++(0, .4) coordinate (res-C);

    \node[small text, anchor=210] at (req-C) {Requête C};
    \node[small text, anchor=330] at (res-C) {Réponse C};
\end{tikzpicture}

\vfill
\pause

\tabtitle{Traitement asynchrone}\\[1em]
\begin{tikzpicture}
    % Flèche du fil d'exécution
    \path[draw, thick, ->]
        coordinate (main-thread) at (0, -3)
        (0, -3) --++ (13, 0)
        coordinate (main-thread-end);

    % Requête A
    \node[
        event, fill=red!60!black,
        right=.5cm of main-thread,
        label={[small text]above:Requête A}
    ] (req-A) {};

    % Requête B
    \node[
        event, fill=blue!60!black,
        right=1.5cm of main-thread,
        label={[above=.5cm, name=req-B-label, small text]
            above:Requête B}
    ] (req-B) {};

    \path[draw, dotted] (req-B) -- (req-B-label);

    % Requête C
    \node[
        event, fill=yellow!60!black,
        right=6cm of main-thread,
        label={[small text]above:Requête C}
    ] (req-C) {};

    % Réponse A
    \node[
        event, fill=red!60!black,
        right=3cm of req-A,
        label={[small text]above:Réponse A}
    ] (res-A) {};

    \path[draw=red!30!black, ->] (req-A) edge[bend right=20] (res-A);

    % Réponse B
    \node[
        event, fill=blue!60!black,
        right=3.5cm of res-A,
        label={[above=.5cm, name=res-B-label, small text]
            above:Réponse B}
    ] (res-B) {};

    \path[draw, dotted] (res-B) -- (res-B-label);
    \path[draw=blue!30!black, ->] (req-B) edge[bend right=20] (res-B);

    % Réponse C
    \node[
        event, fill=yellow!60!black,
        right=3cm of res-B,
        label={[small text]above:Réponse C}
    ] (res-C) {};

    \path[draw=yellow!30!black, ->] (req-C) edge[bend right=20] (res-C);
\end{tikzpicture}
\end{frame}

% \begin{frame}{Modélisation}
% \centering
% \begin{tikzpicture}
%     \umlclass{Sender}{}{
%         + send(req : Request) : Future<Response>\\
%     }
%
%     \umlclass[below=.5cm of Sender.south west]{Request}{
%         -- url : string\\
%         -- headers : Header[*]\\
%         -- verb : Verb\\
%         -- out\_stream : OutStream\\
%         -- in\_stream : InStream\\
%     }{}
%
%     \umldep{Sender}{Request}
%
%     \umlclass[below=.5cm of Sender.south east]{Response}{
%         -- code : int\\
%         -- headers : Header[*]\\
%     }{}
%
%     \umldep{Sender}{Response}
% \end{tikzpicture}
% \end{frame}

\begin{frame}{Implémentation}{Communication sur HTTP}
    \begin{minipage}{.2\textwidth}
        \includegraphics[width=3cm]{logo-curl}
    \end{minipage}\hfill%
    \begin{minipage}{.75\textwidth}
        \begin{itemize}
            \item Bibliothèque créée en 1997.
            \item Utilisée par de nombreuses applications.
            \item Portable sur la grande majorité des systèmes.
        \end{itemize}
    \end{minipage}
    \vfill
    \lstinputlisting[language=C++]{parties/curl.cpp}
\end{frame}

\begin{frame}{Implémentation}{Fonctionnement asynchrone}
\centering
\onslide<2>{Implémentation via la bibliothèque standard de multi-threading C++.\\[1em]}
\begin{tikzpicture}
    % Flèches du temps
    \path[draw, thick]
        coordinate (main-thread) at (0, 0)
        (0, 0) --++ (11, 0)
        coordinate (main-thread-mid);

    \path[draw, dashed, ->, thick]
        (main-thread-mid) --++ (2, 0)
        node[
            visible on=<2>,
            yshift=.5cm, left,
            align=right, small text
        ] {Tâche\\principale};

    \path[draw, thick, visible on=<2>]
        coordinate (send-thread) at (0, -3)
        (0, -3) --++ (11, 0)
        coordinate (send-thread-mid);

    \path[draw, dashed, ->, thick, visible on=<2>]
        (send-thread-mid) --++ (2, 0)
        node[
            visible on=<2>,
            yshift=-.5cm, left,
            align=right, small text
        ] {Tâche d'envoi et\\de réception};

    % Soumission de la requête B
    \node[
        event, fill=blue!60!black,
        label={[above=0cm, small text]Requête B}
    ] (req-B) at (.5, 0) {};

    % File d'attente initiale
    \queue{queue-1}{1, -1.8}{2}

    \node[
        visible on=<2>,
        queue elt, fill=blue!30!white,
        anchor=south west
    ] (queue-1B) at (queue-1) {B};

    \node[
        visible on=<2>,
        queue elt, fill=red!30!white,
        anchor=south west
    ] (queue-1A) at ($(queue-1)+(.6,0)$) {A};

    \node[visible on=<2>, font=\footnotesize, yshift=.6cm]
        at ($(queue-1A)!.5!(queue-1B)$)
        {File d'attente};

    \draw[
        visible on=<2>,
        ->, rounded corners=4pt
    ] (req-B) |- (queue-1B.west);

    % Traitement de la requête A
    \node[
        visible on=<2>,
        period, fill=red!60!black, minimum width=1.5cm, xshift=.75cm,
        label={[visible on=<2>, below=.3cm, small text]
            Traitement de A}, anchor=west
    ] (traitement-A) at (queue-1A.east|-send-thread) {};

    \draw[
        visible on=<2>,
        ->, rounded corners=4pt
    ] (queue-1A.east) -| (traitement-A.west);

    % Réception de la réponse A
    \node[
        event, fill=red!60!black,
        label={[above=0cm, small text]Réponse A}
    ] (res-A) at (traitement-A.east|-main-thread) {};

    % Soumission de la requête C
    \node[
        event, fill=yellow!60!black, xshift=-8pt,
        label={[above=.4cm, name=req-C-label, small text]Requête C}
    ] (req-C) at (traitement-A|-main-thread) {};

    \draw[dotted] (req-C-label) -- (req-C);

    % File d'attente numéro 2
    \queue{queue-2}{$(queue-1-|traitement-A.east)+(.5,0)$}{2}

    \node[
        visible on=<2>,
        queue elt, fill=yellow!30!white,
        anchor=south west,
    ] (queue-2C) at (queue-2) {C};

    \node[
        visible on=<2>,
        queue elt, fill=blue!30!white,
        anchor=south west
    ] (queue-2B) at ($(queue-2)+(.6,0)$) {B};

    \draw[
        visible on=<2>,
        ->, rounded corners=4pt
    ] (req-C) |- (queue-2C.west);

    \draw[visible on=<2>, ->] (traitement-A.east) -- (res-A)
        node [
            above=.1cm, midway, sloped,
            font=\footnotesize\itshape,
            fill=white
        ] {notifie};

    % Traitement de la requête B
    \node[
        visible on=<2>,
        period, fill=blue!60!black, minimum width=2cm, xshift=.75cm,
        label={[below=.3cm, small text, visible on=<2>]
            Traitement de B}, anchor=west
    ] (traitement-B) at (queue-2B.east|-send-thread) {};

    \draw[
        visible on=<2>,
        ->, rounded corners=4pt
    ] (queue-2B.east) -| (traitement-B.west);

    % Réception de la réponse B
    \node[
        event, fill=blue!60!black,
        label={[above=0cm, small text]Réponse B}
    ] (res-B) at (traitement-B.east|-main-thread) {};

    \draw[visible on=<2>, ->] (traitement-B.east) -- (res-B)
        node [
            above=.1cm, midway, sloped,
            font=\footnotesize\itshape,
        ] {notifie};

    % File d'attente numéro 3
    \queue{queue-3}{$(queue-2-|traitement-B.east)+(1.1,0)$}{1.4}
    \node[
        visible on=<2>,
        queue elt, fill=yellow!30!white,
        anchor=south west
    ] (queue-3C) at (queue-3) {C};

    % Flèches pour le premier slide
    \path[visible on=<1>, draw=red!30!black, ->]
        (0, -.6)..controls (2, -.5) and ($(res-A)+(-2,-.5)$)..(res-A);

    \path[visible on=<1>, ->, draw=blue!30!black]
        (req-B) edge[->, bend right=15] (res-B);

    \path[visible on=<1>, draw=yellow!30!black]
        (req-C)..controls ($(req-C)+(2,-.5)$) and (9,-.5)..(11, -.5)
        edge[->, dashed] (13, -.5);
\end{tikzpicture}
\end{frame}
