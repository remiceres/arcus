CREATE TABLE entry (
    id CHARACTER(36) PRIMARY KEY,
    name VARCHAR(255),
    type VARCHAR(4) CHECK(type IN ('file', 'dir')),
    checksum VARCHAR(255)
);

CREATE TABLE entry_parent_of (
    parent CHARACTER(36),
    child CHARACTER(36),
    FOREIGN KEY(parent) REFERENCES entry(id) ON DELETE CASCADE,
    FOREIGN KEY(child) REFERENCES entry(id) ON DELETE CASCADE,
    PRIMARY KEY(parent, child)
);

CREATE TABLE service (
    id CHARACTER(36) PRIMARY KEY,
    provider VARCHAR(255),
    auth VARCHAR(255)
);

CREATE TABLE chunk (
    id CHARACTER(36) PRIMARY KEY,
    chunk_order INTEGER,
    entry_id CHARACTER(36),
    service_id CHARACTER(36),
    FOREIGN KEY(entry_id) REFERENCES entry(id) ON DELETE CASCADE,
    FOREIGN KEY(service_id) REFERENCES service(id) ON DELETE CASCADE
);

CREATE TABLE info (
    name VARCHAR(255),
    version INTEGER
);
