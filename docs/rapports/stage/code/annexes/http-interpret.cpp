HTTP::Sender sender;

HTTP::Request req;
req.setVerb(HTTP::Verb::GET);
req.setUrl("https://example.com");

HTTP::Response res = sender.send(req).get();

if (res.getCode() == 200)
{
    std::ifstream out("resultat.html");
    res.setOutStream(out);
    res.finish().get();

    std::cout << "Tout s'est bien passé. Résultat dans 'resultat.html'.\n";
    return EXIT_SUCCESS;
}
else
{
    std::cerr << "Code d'erreur du serveur : " << res.getCode() << ".\n";
    return EXIT_FAILURE;
}
