HTTP::Sender sender;

HTTP::Request req;
req.setVerb(HTTP::Verb::POST);
req.setUrl("https://example.com");

HTTP::Response res = sender.send(req).get();

std::cout << res.getCode() << "\n";
std::cout << res.getHeaderSet().getHeaders("Date") << "\n";
