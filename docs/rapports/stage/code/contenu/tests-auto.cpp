#include <iostream>
using std::cout;

int main()
{
    if (ajoute(5, 3) != 8)
    {
        cout << "ajoute(5, 3) != 8.\n";
        return EXIT_FAILURE;
    }

    if (ajoute(-5, 3) != -2)
    {
        cout << "ajoute(-5, 3) != -2.\n";
        return EXIT_FAILURE;
    }

    // ...

    return EXIT_SUCCESS;
}
