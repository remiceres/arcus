#include <iostream>
using std::cout;

int main()
{
    cout << "ajoute(5, 3) = "
         << ajoute(5, 3) << "\n";

    cout << "ajoute(-5, 3) = "
         << ajoute(-5, 3) << "\n";

    cout << "ajoute(12, 0) = "
         << ajoute(12, 0) << "\n";

    cout << "ajoute(5, -3) = "
         << ajoute(5, -3) << "\n";

    // ...

    return EXIT_SUCCESS;
}
