#include "catch.hpp"

TEST_CASE("La fonction « ajoute » produit la somme de ses arguments")
{
    REQUIRE(ajoute(5, 3) == 8);
    REQUIRE(ajoute(-5, 3) == -2);
    REQUIRE(ajoute(12, 0) == 12);
    REQUIRE(ajoute(5, -3) == 2);
}
