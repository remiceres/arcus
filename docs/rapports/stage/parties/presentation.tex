\chapter{Présentation du stage}

\section{Laboratoire d’accueil et équipe de recherche}

Le \textsc{LIRMM} est un laboratoire de recherche en informatique, robotique et microélectronique. Fondé en~1992, il dépend conjointement de l'Université de Montpellier et du Centre National de la Recherche Scientifique (\textsc{CNRS}). En~2016, il était composé de 187~chercheurs\ptm{euses}{} et enseignant\ptm{e}{s}-chercheurs\ptm{euses}{} ainsi que de 170~doctorant\ptm{e}{s}~\parencite{lirmm-2016-chiffres}.

Le laboratoire est implanté à Montpellier et à Perpignan. Nous avons réalisé notre stage sur le campus Saint-Priest à Montpellier, dans le bâtiment~4, présenté dans la figure~\ref{fig:intro-lirmm}.

\addtocounter{footnote}{-1}
\begin{figure}[htb]
    \includegraphics[width=.49\textwidth]{figures/presentation/lirmm-photo}
    \hfill
    \includegraphics[width=.49\textwidth]{figures/presentation/lirmm-carte}
    \caption[]{\textbf{Vue extérieure du bâtiment~4 et situation géographique du campus Saint-Priest.} Carte réalisée par les contributeurs\ptm{trices}{} d’OpenStreetMap, sous licence CC-BY-SA 2.0\footnotemark.}
    \label{fig:intro-lirmm}
\end{figure}

\footnotetext{OpenStreetMap~: \url{https://www.openstreetmap.org/}.}

Le \textsc{LIRMM} est composé de trois départements~: informatique, microélectronique et robotique. Chacun de ces départements est divisé en plusieurs équipes~\parencite{lirmm-2016-presentation}.

\textbf{Le département d’informatique} travaille aussi bien sur des thématiques appliquées (bio-informatique, recherche opérationnelle, fouille de données) que des domaines théoriques (algorithmique, cryptographie, complexité, calculabilité).

\textbf{Le département de microélectronique} mène des recherches sur l'architecture, la modélisation, la conception et les tests des systèmes intégrés et des microsystèmes.

Enfin, \textbf{le département de robotique} effectue des recherches sur le pilotage de véhicules autonomes, le traitement d'images ou encore l'interface robot/vivant.

Nous avons réalisé notre stage au sein du département d'informatique, dans l'équipe \emph{Models and Reuse Engineering, Languages}~(\textsc{MAREL}). Cette équipe étudie le génie logiciel, notamment «~l’automatisation des étapes du cycle de vie du logiciel, de la conception à la maintenance en passant par la compilation et l’optimisation du code~»~\parencite{marel-2017-presentation}.

Le génie logiciel est une science qui s'intéresse aux outils, techniques et procédures permettant de s’assurer que les logiciels conçus correspondent aux attentes du client, soient fiables, aient un coût d'entretien réduit et de bonnes performances tout en respectant les délais et les coûts de construction~\parencite{strohmeier-1996-genie}.

\section{Présentation du projet Arcus}

Arcus est un outil de synchronisation multi-services de stockage dans les nuages qui permettra, à terme, à un utilisateur de synchroniser ses données en les répartissant dans plusieurs services de stockage. Ce projet est né dans le cadre des \textsc{TER} du second semestre de \textsc{L2}.

\subsection{Motivations du projet}

Le stockage de données dans les nuages consiste pour un utilisateur à confier ses données à un service qui s'occupe de les stocker pour lui et de les lui restituer dès qu'il en a besoin. Un tel service est généralement opéré par une entreprise qui est responsable de l'intégrité des informations déposées.

Ce mode de stockage offre de nombreux avantages, permettant par exemple à l'utilisateur d'accéder à ses données depuis plusieurs appareils, de conserver une copie de sauvegarde de celles-ci, voire d'en partager certaines avec d'autres personnes. Ce modèle s'est considérablement développé ces dernières années, et diverses entreprises telles que Amazon, Google ou Microsoft proposent de stocker gratuitement les données de leurs utilisateurs.

Malgré les nombreux avantages du stockage dans les nuages, il cause une centralisation des données de l'utilisateur sur un seul service et une dépendance sur celui-ci. L'utilisateur place en effet ses données entre les mains d'une unique entreprise, et par ailleurs sa confiance dans la bonne gestion de ses informations par cette entreprise.

Cette centralisation le fait dépendre du service et de la société qui le gère, qui peut à tout moment décider de modifier son offre ou d'y mettre fin. Les services gratuits n'offrent par ailleurs souvent aucune garantie quant à la confidentialité des données déposées.

\subsection{Objectifs du projet et cahier des charges}

Nous avons baptisé notre projet Arcus, en référence au nuage bas du même nom, qui prend la forme d'un tube et est souvent associé aux \emph{cumulus} et \emph{cumulonimbus}. L'objectif est de permettre à un utilisateur de synchroniser les répertoires de son choix (appelés «~répertoires synchronisés~» dans la suite de ce rapport) entre sa machine et différents services de stockage dans les nuages. En agissant comme un tube qui ferait le lien entre ces différents services et l’utilisateur, il se veut une réponse aux problématiques énoncées précédemment.

Cet objectif est similaire à celui d’autres applications de synchronisation comme celles de Dropbox, Google, Nextcloud ou Apple, mais se distingue notamment par le fait que les données soient réparties sur différents services au contraire des moteurs de synchronisation classiques qui ne les synchronisent qu'avec un service particulier.

Au début des TER, nous avons établi le cahier des charges suivant pour Arcus.

\begin{description}
    \item[Interface commune aux services de stockage.] Habituellement, chaque fournisseur de service propose sa propre application de synchronisation. Dans le cas où l'utilisateur se sert de plusieurs services, il doit installer plusieurs applications ayant le même but. Un objectif pour notre outil est de pouvoir fonctionner avec le plus de services différents possibles.
    \item[Répartition des données de l'utilisateur.] Chaque fichier déposé par l'utilisateur dans un des répertoires choisis sera découpé en plusieurs blocs, qui seront envoyés sur différents services de stockage existants. Dans le cas où un de ces services serait ajouté ou supprimé, les données seraient alors redistribuées afin que toutes les informations restent accessibles. Cette répartition sera homogène et équilibrera au mieux l'espace occupé sur chaque service.
    \item[Réplication des données de l'utilisateur.] Les blocs de données seront stockés de manière redondante sur au moins deux services différents. Cela permettra de se prémunir d'une panne d'un des services dépositaires ou de tout événement entraînant une perte de l'accès aux données.
    \item[Chiffrement des données de l'utilisateur.] Avant de transmettre les blocs de données aux serveurs, ceux-ci seront chiffrés. Ce chiffrement garantira la confidentialité des données de l'utilisateur puisqu'il empêchera les services dépositaires et toute autre personne tierce d'accéder au contenu des blocs.
    \item[Transparence de fonctionnement de l'application.] Les opérations de synchronisation, de répartition et de chiffrement des données s'exécuteront en fond. Les interactions directes avec l'utilisateur se limiteront à la gestion des fichiers par l'intermédiaire du gestionnaire de fichiers du système d’exploitation, à la résolution de conflits éventuels et à la configuration de l'application.
\end{description}

Notre application permettra ainsi de réduire le risque de perte des données de l'utilisateur en les répartissant et en les répliquant à travers plusieurs services de stockage. En combinant ce mécanisme de répartition au chiffrement, elle accroîtra la confidentialité de ces données. Enfin, elle permettra de fédérer l'accès à plusieurs services en une seule application et de bénéficier des ressources de stockage de plusieurs fournisseurs combinés.

\subsection{Architecture globale d'Arcus}

Au cours de la phase de modélisation de l’application, nous avons choisi une architecture que nous avons voulue la plus modulaire possible. En isolant au mieux les différentes fonctionnalités de l’application, la collaboration est facilitée, le code se prête mieux aux tests, et il est plus aisé de localiser le code associé à une fonction précise. Cette architecture est résumée dans la vue d'ensemble en figure~\ref{fig:conception-vue-ensemble}.

\begin{figure}[h!]
    \centering
    \includegraphics{../../modelisation/conception/overview}
    \caption{\textbf{Vue d'ensemble de l'architecture de l'application.} Au centre, dans le cadre d’Arcus, sont présentés les différents modules participant à l'architecture de l'application. Le niveau d'abstraction des modules va en croissant vers le haut du schéma. Les flèches représentent les interactions internes à l'application et celles entre les acteurs, les ressources externes et l'application.}
    \label{fig:conception-vue-ensemble}
\end{figure}

Notre application a besoin de communiquer avec les interfaces de programmation des différents services de stockage de fichiers. Ces services fournissent en général une interface de communication utilisant \textit{HyperText Transfer Protocol} (HTTP). C'est un «~protocole applicatif pour les systèmes d'information distribués, collaboratifs, utilisant des documents riches~»\footnote{\emph{“The Hypertext Transfer Protocol (HTTP) is an application-level protocol for distributed, collaborative, hypermedia information systems.”}~\parencite[traduction libre.]{fielding-1999-hypertext}}, donc particulièrement adapté au transfert de documents. Afin de communiquer avec ces services, notre application contient un module de transmission de données sur HTTP, symbolisé par \module{red}{HTTP} dans la vue d'ensemble.

Bien que les fournisseurs de services de stockage connus tels que Nextcloud, OneDrive ou Dropbox utilisent HTTP, ils ont adopté des architectures hétérogènes qui font qu'il est difficile d'écrire un code unique permettant de communiquer avec toutes leurs interfaces. Dans notre application, nous avons donc construit une couche d'abstraction de ces hétérogénéités. Ce module, symbolisé par \module{blue}{Multi-services} dans la vue d'ensemble, s'appuie sur les fonctionnalités du module HTTP.

Comme chaque utilisateur de la machine cliente peut choisir les répertoires qu'il souhaite synchroniser via l'application, la liste de ces répertoires doit être sauvegardée à un endroit de la machine. Ces informations sont regroupées dans une configuration dite locale spécifique à chaque utilisateur. Il existe par ailleurs des informations spécifiques à chaque répertoire synchronisé, comme par exemple la liste des fichiers présents ou la liste des services connectés. Ces informations sont quant à elles regroupées dans une configuration synchronisée sur les différentes machines d'un même utilisateur. Nous avons décidé d'isoler cette fonctionnalité dans un module de configuration, symbolisé par \module{green}{Configurations} dans la vue d'ensemble.

En s'appuyant sur ces différentes abstractions, nous pouvons mettre en place le moteur de synchronisation, cœur de l'application. Ce moteur doit permettre l'envoi des données contenues dans les répertoires synchronisés vers les services de stockage connectés, en respectant les contraintes du cahier des charges. Réciproquement, il doit permettre le rapatriement de celles-ci depuis les services distants vers la machine de l'utilisateur. Ce module est symbolisé par \module{yellow}{Moteur de synchronisation} dans la vue d'ensemble.

Enfin, nous avons décidé qu'il était nécessaire de créer un module de journalisation. Grâce à ce module, les différents composants de l'application sont en mesure d'écrire des informations sur un journal global dans un format unifié. Ce journal global, qui peut être redirigé vers la sortie standard ou un fichier, aide au débogage en cas de défaillance de l'application. Dans la vue d'ensemble de l'architecture, il est symbolisé par \module{gray}{Journal}.

\subsection{Organisation du développement d’Arcus}

Lors du développement d'Arcus, nous avons décidé de travailler un maximum de temps ensemble et de manière très régulière. Pour que nous puissions profiter tous deux des nombreuses connaissances que nous apporte le développement de ce projet, nous avons favorisé la méthode de programmation en binôme. Cette méthode, qui s’inscrit dans le cadre de l’\emph{extreme programming}~\parencite[p.~4]{beck-1998-extreme}, consiste à programmer à deux, en donnant la responsabilité de l'écriture du code à l'une des deux personnes et en faisant relire le code produit en temps réel par la seconde personne. Une étude menée par l'IEEE Computer Society~\parencite{mcdowell-2003-impact} a en effet montré que les étudiants utilisant cette méthode produisaient du code de meilleure qualité.

Afin d'être le plus efficace et d'avancer le plus rapidement possible nous nous sommes réunis quotidiennement. Durant les jours de la semaine, nous nous sommes vus entre 16~h~30 et 19~h~30, afin de faire le point sur l'avancement du projet, de définir les objectifs du jour et de les réaliser. Enfin, chaque week-end, nous avons réalisé les tâches en attente que nous n'avions pas pu faire durant la semaine.

Au cours des TER, nous avons organisé des réunions quinzomadaires avec notre encadrante \Mme~Hinde~Bouziane afin de faire le point sur l'état d'avancement de l'application. Pendant le stage, nous nous sommes également réunis régulièrement avec notre encadrante. Ces réunions nous ont permis de bénéficier de ses conseils et de son aide sur les difficultés que nous avons rencontrées lors du développement.

Nous utilisons le langage C++ pour le développement d’Arcus. Nous avons choisi de versionner son code avec le gestionnaire de versions décentralisé Git. Le versionnage permet d’enregistrer l’intégralité de l’historique des changements faits sur le code dans un répertoire appelé dépôt. L’historique ainsi obtenu se présente sous la forme d’une succession de \emph{commits} qui sont eux-mêmes de petits ensembles de modifications. Ainsi, il est aisé de revenir à des versions antérieures en cas de problème, de comparer les différentes versions voire d’identifier précisément quel changement a introduit un certain bogue~\parencite[chapitre~1, p.~1]{chacon-2009-pro}.

Git peut être utilisé avec un serveur. Les différent\ptm{e}{s} développeurs\ptm{euses}{} conservent alors une copie du dépôt, appelée copie de travail, et envoient régulièrement leur travail sur le serveur, depuis lequel les autres peuvent le récupérer. Il facilite ainsi la collaboration de plusieurs personnes sur un même projet. Nous avions initialement décidé d’utiliser le serveur Git géré par le Service Informatique de la Faculté des sciences (SIF), qui utilise le logiciel GitLab, mais nous avons dû faire face à des indisponibilités récurrentes du service et au retard de version de celui-ci. Nous nous sommes finalement tournés vers le service GitLab.com\footnote{Dépôt GitLab : \url{https://gitlab.com/remiceres/arcus}}, utilisant le même logiciel.

\subsection{Avancement du projet durant les TER}

Au cours des TER, nous avons pu terminer l'implémentation du module de communication via HTTP. Pour tester ce module, nous avons développé un programme de test réalisant des échanges avec le serveur de test \url{example.com} sur différents types de requêtes. Nous nous sommes ainsi assurés du bon fonctionnement de ce module, y compris lorsque plusieurs requêtes sont envoyées en parallèle. Le module est ainsi utilisable pour l'envoi de requêtes avec n'importe quel verbe sur n'importe quelle URL.

Nous avons également fini la création du module des services, et créé une classe permettant d'utiliser les services de stockage Dropbox et OneDrive. Nous avons écrit un programme de test qui récupère le quota, liste un répertoire, envoie des fichiers et en supprime sur un compte de chaque service. Ce test permet également de valider le bon fonctionnement du module HTTP qui est utilisé par la classe.

Enfin, nous avons achevé la réalisation du module de journalisation des actions de l'application, et celui-ci est utilisé à travers les autres modules. Nous avons également développé un programme pour tester la création du journal et la cohérence des messages affichés.
