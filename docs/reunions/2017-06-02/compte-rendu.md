# Journée de stage du 02/06/2017

*LIRMM, bâtiment 4, 8h - 12h*

## À faire sur les slides

![Liste des tâches sur les slides](todo-slides.jpg)

## Réunion avec Hinde

Pour se mettre au point sur la soutenance de mardi et discuter des améliorations possibles.

### Généralités

Mettre des mots-clefs dans le plan.

* Ne pas faire apparaître l'introduction dans le plan.
* Retirer le jargon technique du plan.
* Première partie => communication avec les serveurs.
* Deuxième partie => utilisation des services.

Dans l'annonce orale du plan, dire qu'on découpera les parties services et HTTP en trois sous-parties : objectifs, modélisation puis implémentation.

Faire apparaître l'objectif dans le plan.

### Introduction

* Renommer « Problématiques » en « Inconvénients » (plus clair).
* Dans le cahier des charges, ne pas répéter les inconvénients (pas le temps).
* Faire apparaître une figure pour définir le stockage dans les nuages au lieu d'une définition textuelle.

Rappeler le nom du projet dans les titres de slides.

**Figure de la première slide :** reprendre la figure de l'architecture avec un seul service de stockage (une application par service). Permet de faire vraiment la différence entre notre application et une application de synchronisation traditionnelle.

**Figure de la dernière slide :** remanier la figure pour avoir plusieurs applications dans le nuage.

Utiliser des noms plus explicites et moins techniques pour les différents modules, par exemple Sync => Synchronisation, Services => Multi-services.

Parler de l'état actuel du projet dans l'intro. Faire apparaître explicitement le fait que le projet est en cours lors de la présentation des modules.

### HTTP

* Commencer à parler dès le début des deux objectifs.
* Faire apparaître le fait que c'est un extrait de la modélisation (points de suspension).
* Éventuellement si le temps est court, déplacer toute la modélisation vers le slide de fin.
* Ne pas rappeler le nom du module dans chaque slide mais le concentrer dans le titre principal.

### Services

* Renommer en "Multi-Services".
* Expliquer l'étude des API. Mettre en relation avec l'interface créée.
* Expliquer un exemple de requête pour les deux services sur deux actions particulières.

### Démonstration

On peut présenter les deux services en même temps.

### Conclusion

Se préparer à des questions sur le module de synchronisation. Préparer des transparents inutilisés pendant la présentation pouvant servir à appuyer d'éventuelles questions sur le sujet.
