# Réunion du 28/03/2017

*Bât 16, durée 40 minutes.*

Signature des conventions pour le stage d'avril et démonstration du concurrenciel sur le projet.

## Stage

Le stage se fera en deux périodes :

* 03/04/2017 - 07/04/2017 : 5 jours à 7 heures par jour (total 35h) pendant la première semaine des vacances ;
* 30/05/2017 - 30/06/2017 : 23 jours à 5 heures par jour (total 115h) après les partiels ;

Pour un total de 150 heures. Il est nécessaire de faire une convention par période de stage. Lors de cette séance : signature des conventions pour la première période par la responsable.

Lors de la première semaine :

* démarrage à 9h30, fin maximum à 19h ;
* prévoir à manger (pas de restauration sur place) ;
* boulangerie à proximité.

## Démonstration

Réalisé : passage du module HTTP en concurrentiel permettant d'effectuer plusieurs requêtes en parallèle. Démonstration du test d'envoi de 1000 requêtes GET en parallèle sur example.net.

Question : lenteur lors de l'utilisation systématique de la variable conditionnelle. Elle engendre un traitement séquentiel des requêtes :

1. Thread 1 : verrouillage du mutex.
2. Thread 1 : ajout d'une requête dans la pile.
3. Thread 1 : réveil du thread secondaire.
4. Thread 1 : déverrouillage du mutex.
5. Thread 2 : verrouillage du mutex.
6. Thread 2 : traitement de la requête.
7. Thread 2 : mise en sommeil.

Le verrouillage d'un seul mutex engendre un blocage du premier thread qui empêche d'ajouter toutes les requêtes à envoyer avant qu'elles commencent à être envoyées. Possibilités :

* réveiller le thread secondaire à la demande ;
* utiliser deux mutex : un pour la pile et un pour le réveil.
