# Réunion du 21/02/2017

*Bât 16, durée 1 heure 15.*

Point sur la finalisation de la modélisation et le début de l’implantation.

## Planification

Revue du [diagramme de Gantt](docs/gantt/gantt-ter.pdf) établi pendant les vacances et planifiant la première phase du projet (celle s’étendant de janvier à mai).

* Rendu du rapport doit être fait fin avril au lieu de fin mai : déplacer la tâche et ses relations.
* Garder le diagramme sous forme étendue même s’il est trop large. On pourra retirer des détails pour le présenter au final.
* Ajouter l’organisation de la deuxième partie.
* Détailler les tâches de développement pour inclure.

## Diagramme de classes

Revue du [diagramme de classes](docs/modelisation/classes.pdf) établi pendant les vacances, concernant les classes du paquetage Services.

* Flèche de spécialisation à transformer en flèche de réalisation et à inverser pour les classes réalisant l’interface `Service`.
* Flèche entre `Service` et `Quota` à vérifier.

## API

Pour stocker le jeton obtenu depuis l’API de chaque service de stockage, utiliser les fonctions du système : plus de sécurité pour des données sensibles. (Se renseigner sur l’existence d’un système de trousseau sur Windows.)

Pour mémoriser le quota qui est reçu sous forme d’entier 64 bits tout en restant compatible avec les processeurs 64 bits, effectuer des opérations sur la chaîne de caractère reçue pour perdre en précision (acceptable puisqu’on souhaite juste récupérer le ratio d’utilisation). Il faut utiliser une unité plus grosse que les octets pour que cela rentre dans un 32 bits (2^32 - 1 ~= 4 000 000 000, donc un comptage en Mo pouraît être acceptable).

Se renseigner sur une API de parsage du JSON en C++.

Sécurité : s’assurer que le jeton n’est pas transmis en clair lors de la réponse de Dropbox entre autres (faire un dump des échanges réseau sur Firefox). Éventuellement trouver une méthode renvoyant le jeton autrement.

Prochaine réunion : date à déterminer dans une ou deux semaines.
