# Réunion du 31/01/2017

*Bât 16, durée 1 heure 45.*

Discussion sur les solutions de découpage, de technologies de stockage de la base d'index.

## Base d'index

Système de base de donnée de type Oracle.

* PostgreSQL
* MySQL

## Solutions de découpage

Protocoles d'envoi possibles :

* FTP ?
* pair à pair ?
* construction manuelle au dessus de TCP ? Probablement pas possible car gestion du client mais pas du serveur.

Découpage du fichier en fichiers temporaires et envoi séquentiel des morceaux. Possibilité de paralléliser le découpage avec plusieurs envois sur le réseau (multi-threading).

Si multi-threading, optimiser l'utilisation de la bande passante, soit en dynamique en évaluant la bande passante, soit en proposant à l'utilisateur de personnaliser la valeur.

Voir éventuellement s'il existe des librairies qui font le découpage.

## Étude bibliographique

À voir vendredi 10 février :

* se renseigner sur les dates de rendu de l'étude ;
* se renseigner si on peut travailler en groupe.

Sujets possibles :

* rsync ;
* partage pair à pair ;
* partitionnement des fichiers.

## À faire

* Faire le diagramme de Gantt.
* Convention de stage.
