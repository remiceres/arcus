# Journée de stage du 07/06/2017

*LIRMM, bâtiment 4, 14h - 18h*

## Liste des tâches

### Pour le stage

- Création de l'arborescence d'exceptions et gestion correcte des erreurs dans le module Services.
- Cohérence de l'utilisation des chemins dans le module Services.
- Mise en place initiale du module de synchronisation.
- Implémentation méthodes restantes pour la base de données.
- Intégration du moteur de synchronisation avec les autres modules.
- Tests de la synchronisation.
- Portage sur d'autres systèmes.
- Chiffrement des données.
- Interface graphique de configuration.

### Pour cette semaine

- Création de l'arborescence d'exceptions et gestion correcte des erreurs dans le module Services.
- Cohérence de l'utilisation des chemins dans le module Services.
- Mise en place initiale du module de synchronisation.

## Gestion des erreurs

![Arborescence d'exceptions pour la gestion des erreurs](erreurs.jpg)

## Gestion des flux dans HTTP

![Travail sur la gestion des flux sur HTTP](flux.jpg)
