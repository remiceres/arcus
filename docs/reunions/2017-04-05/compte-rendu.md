# Journée de stage du 05/04/2017

*LIRMM, bâtiment 4, 9h15–18h50.*

Troisième journée de stage au LIRMM.

## Classe d’application

Création d’une classe permettant la gestion des ressources associées à l’application pour décharger le main. Cette classe :

* lit la configuration ;
* relit la configuration dynamiquement lorsqu’elle est modifiée ;
* lance l’observeur du système de fichiers ;
* relance l’observeur dynamiquement.

Rencontre d’un bug de corruption de mémoire et de l’observeur qui ne veut pas s’arrêter.

![Modélisation de la classe application](modelisation-application.jpg)

## Conception de la synchronisation séparée

Retour sur la décision initiale : possibilité pour l’utilisateur de synchroniser plusieurs dossiers indépendamment. Chaque dossier contient sa base de données indépendante.

![Conception synchronisation](separation-synchroniseurs.jpg)

Prévision d’une classe synchroniseur permettent de gérer les ressources nécessaires à la synchronisation comme la connexion à la base de données.
