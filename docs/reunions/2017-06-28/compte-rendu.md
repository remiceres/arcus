# Journée de stage du 28/06/2017

**LIRMM, bâtiment 4, 8h - 18h**

## Conférence robotique

**LIRMM, bâtiment 5, 14h**

_The Butterfly robot: challenges for motion planning and control_<br>
_Conférence de Anton Shiriaetv_

A problem researched for a few decades.

## Intro

How to find and plan a feasible movement for a robot?

Given a movement, how can we represent it? For example, sine wave. A function of time. Establish primitives for movements and then concatenate them.

What is a “Butterfly” robot?

* Plan a rolling of a sphere around a frame.
* Feedback controller to stabilize the motion.
* Called “Butterfly” because of the frame’s shape.

Problem introduced in 2009.

## Steps in motion planning

Each body needs 3 coordinates, so in total we need 6 corrdinates (1 for the frame and 1 for the ball).

* How to detect a contact point between the frame and the disc?
* How to do that with a rolling ball?

Rolling without slipping:

* no offset between the frame and the ball;
* no drift.
