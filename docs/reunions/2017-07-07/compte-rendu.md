# Journée de stage du 07/07/2017

## Réunion robotique

### Choix de sujet

Deux possibilités de sujet :

* **robotique sous-marine :** problème du canal de communication. Wi-Fi à faible portée, satellites inaccessibles ;

* **robotique terrestre et aérienne :** problèmes mieux connus, moins complexe que sous-marin.

Synthèse sur les communications entre drones et sous-marins d’ici fin août, avec soit deux synthèses soit une seule si le choix est fait.

### Ressources

* [L'incroyable pouvoir athlétique des quadricoptères](https://www.ted.com/talks/raffaello_d_andrea_the_astounding_athletic_power_of_quadcopters?language=fr)
