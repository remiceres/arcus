# Journée de stage du 08/06/2017

*LIRMM, bâtiment 4, 8h - 18h*

Démarrage des travaux sur les flux HTTP.

## Réunion avec Hinde

Réunions à venir la semaine prochaine :

* avec Anne-Elisabeth Baert concernant les algorithmes de découpage. Date à confirmer, potentiellement lundi matin.
* avec le chercheur en robotique concernant le projet de L3, jeudi après-midi.
