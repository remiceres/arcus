# Journée de stage du 01/06/2017

*LIRMM, bâtiment 4, 8h - 12h*

Préparation des slides de la soutenance concernant l'introduction et le module HTTP. Entraînement sur ces parties.

![Idées partie HTTP](partie-http.jpg)
