# Journée de stage du 03/04/2017

*LIRMM, bâtiment 4, 9h30–18h15.*

Première journée de la première période de stage au LIRMM.
La première période se déroulera du 3 avril au 7 avril 2017.

## Objectifs du stage

À faire pendant le stage :

* base de données (schéma déjà fait) avec SQLite ;
* système de découpage ;
* observation des changements dans une arborescence de fichiers (inotify) ;
* rapport.

## Base de données et changements du système du fichier

**Algorigrammes d’intégration**

![Algorigramme d’intégration](algorigramme-1.jpg)
![Algorigramme d’intégration](algorigramme-2.jpg)

### Observation des changements

Bibliothèque sélectionnée pour observer les modifications dans une arborescence de fichiers de façon multiplateforme : [fswatch](https://github.com/emcrisostomo/fswatch/). Compatible avec Windows, Linux, FreeBSD, macOS. API C++ haut niveau.

Ce qui est fait :

* Test de la bibliothèque dans le dossier `observation/`.
* Création du fichier de configuration pour CMake pour l’édition des liens.
* Réflexion sur l’intégration du processus d’observation dans Arcus.

Autres candidats pour l’observation :

* inotify : noyau Linux
  https://en.wikipedia.org/wiki/Inotify
* kqueue : noyau FreeBSD/OS X
  https://en.wikipedia.org/wiki/Kqueue
* libkqueue : couche de compatibilité kqueue
  https://github.com/mheily/libkqueue
* efsw : Entropia File System Watcher
  https://bitbucket.org/SpartanJ/efsw
* Qt : QFileSystemWatcher
  http://doc.qt.io/qt-5/qfilesystemwatcher.html

### Création de la base de données

Bibliothèque sélectionnée pour la création des bases de données : [SQLite](https://www.sqlite.org/). Compatible avec Windows, Linux, macOS. API C. Stocke la base dans un seul fichier auto-contenu, facile à synchroniser.

Ce qui est fait :

* Test de la bibliothèque dans le dossier `base-de-donnees/`.
* Création du fichier de configuration pour CMake pour l’édition des liens.
* Réflexion sur le processus de persistence dans la base.

## Configuration et organisation de l’arborescence

* Configuration centralisée dans un fichier `config.json`.
* Dans la configuration : liste des dossiers synchronisés.
* Dans chaque dossier synchronisé, base de données « locale » (en SQLite) stockant les métadonnées des fichiers, les identifications des services et la liste des blocs.

![Organisation de l’arborescence](organisation-arbo.jpg)
