#include <iostream>
#include <sqlite3.h>

static int callback(void *NotUsed, int argc, char **argv, char **azColName)
{
    int i;
    for(i=0; i<argc; i++)
    {
        printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
    }
    printf("\n");
    return 0;
}

void execution(sqlite3* database, const char *evaluated)
{
    char* erreur = nullptr;
    int retour = 0;

    retour = sqlite3_exec(database, evaluated, callback, nullptr, &erreur);

    if( retour!=SQLITE_OK )
    {
        std::cerr << "SQL error: " << erreur << std::endl;
        sqlite3_free(erreur);
    }
}

int main()
{
    sqlite3 *db;
    int retour;

    retour = sqlite3_open("premier_test.db", &db);

    if( retour )
    {
        std::cerr << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
        sqlite3_close(db);
        return 1;
    }

    execution(db,"CREATE TABLE IF NOT EXISTS jouer(idacteur NUMERIC(5), idfilm NUMERIC(5), salaire NUMERIC CHECK (salaire > 0), PRIMARY KEY (idacteur,idfilm))");

    execution(db,"INSERT INTO jouer VALUES (1234,54321,100);");

    execution(db,"SELECT idfilm FROM jouer;");

    sqlite3_close(db);
return 0;
}
