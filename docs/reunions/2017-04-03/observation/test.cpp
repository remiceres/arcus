#include <libfswatch/c++/monitor.hpp>
#include <iostream>

void monitor_on_event(const std::vector<fsw::event>& events, void* unused)
{
    system("clear");

    for (const fsw::event& event : events)
    {
        std::cout << "Événement" << std::endl;
        std::cout << "Chemin : " << event.get_path() << std::endl;
        std::cout << "Actions : ";

        for (const fsw_event_flag& flag : event.get_flags())
        {
            std::cout << fsw::event::get_event_flag_name(flag) << " ";
        }

        std::cout << std::endl << std::endl;
    }
}

int main(int argc, char* argv[])
{
    std::vector<std::string> paths{"/tmp"};

    fsw::monitor* mon = fsw::monitor_factory::create_monitor(
        fsw_monitor_type::system_default_monitor_type,
        paths,
        &monitor_on_event
    );

    mon->set_latency(2);
    mon->set_fire_idle_event(true);
    mon->start();

    return 0;
}
