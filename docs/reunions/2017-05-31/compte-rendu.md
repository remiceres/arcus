# Journée de stage du 31/05/2017

*LIRMM, bâtiment 4, 8h - 18h30*

## Ordre du jour

### Organisationnel

* Faire planning de présence pour cette semaine et la suivante.

### Soutenance

* Fixer le plan de la présentation.
* Choisir les fonctionnalités à présenter.
* Préparer le contenu des différentes parties.
* Commencer à concevoir les slides.
* Prévoir l'organisation de la vidéo (découpe, montage, ...).

### Code

* Gestion d'erreurs sur OneDrive.

## Plannings

Voir les fichiers planning22.pdf et planning23.pdf pour le planning de la semaine actuelle et de la semaine prochaine.

## Soutenance

Plan de la soutenance :

1. Introduction
2. Envoi de requêtes HTTP asynchrones
3. Interface commune aux services de stockage
4. Démonstration
5. Conclusion

### Introduction

L'introduction de la soutenance explique le principe du stockage de données dans les nuages, son intérêt par rapport à un stockage purement local et aussi les problématiques qu'il soulève. Ces problématiques permettent de motiver notre projet.

Nous expliquons ensuite le cahier des charges et les objectifs définis pour le projet. Enfin, nous détaillons les problématiques et fonctionnalités du projet et celles que nous avons choisies de présenter pendant la soutenance : HTTP et Services.

1. Présentation du cloud et du stockage de fichiers dans les nuages.
2. Avantages du stockage dans les nuages par rapport au stockage sur une seule machine.
3. Problématiques soulevées par ce mode de stockage.
4. Motivation du projet Arcus.
5. Cahier des charges et objectifs du projet.
6. Problématiques et fonctionnalités.
7. Plan de la soutenance.

On prévoit 2 minutes pour l'introduction.

### Envoi de requêtes HTTP asynchrones

On définit d'abord les objectifs du module HTTP, puis on détaille la modélisation choisie et enfin ce qui a été mis en oeuvre pour l'implémenter.

1. Objectifs du module
    - pouvoir envoyer n'importe quelle requête HTTP à n'importe quel serveur ;
    - traiter les requêtes et obtenir des réponses ;
    - utilisée par Services pour interagir avec les serveurs des services de stockage ;
    - traitemnt asynchrone des requêtes.
2. Modélisation choisie (intégrer une version allégée de l'UML).
3. Implémentation du module
    - choix de la bibliothèque cURL ;
    - adaptation de la bibliothèque pour une interface haut niveau ;
    - utilisation de la bibliothèque standard et de Boost pour les fils d'exécution et les promesses ;
    - gestion des fils d'exécution dans la classe Sender.

On prévoit 4 minutes 30 pour HTTP.

### Définition d'une interface multi-services

On définit d'abord les objectifs du module Services, puis on détaille la modélisation choisie et enfin ce qui a été mis en oeuvre pour l'implémenter.

1. Objectifs du module
    - interface abstraite permettant de manipuler les ressources de n'importe quel service de la même manière ;
    - facilité pour l'ajout de support d'un nouveau service (pas besoin de modifier le code utilisant les autres services) ;
    - utilisation de l'abstraction construite dans HTTP (notamment l'asynchronisme).
2. Modélisation choisie (intégrer une version allégée de l'UML).
3. Implémentation du module
    - étude des différentes A.P.I. ;
    - formulation des requêtes pour satisfaire l'interface commune ;
    - interprétation des résultats (JSON/XML) ;
    - promesses et gestion d'erreurs.

On prévoit 4 minutes pour Services.

### Démonstration

Pendant la démonstration, nous devons mettre en évidence l'utilité des modules HTTP et Services expliqués précédemment. Il faut notamment montrer la flexibilité du module HTTP et l'asynchronisme ainsi que la capacité du module Service à cibler n'importe quel service supporté avec le même ensemble de fonction.

Vidéo ou démonstration en direct ? Ou les deux ?

#### HTTP

**Suggestion :** création d'un petit programme interactif permettant de créer des requêtes puis de les envoyer. Pendant l'envoi, affichage d'une progression pour mettre en évidence l'asynchronisme.

#### Services

**Suggestion :** création d'un petit programme interactif permettant de connecter un ensemble de services du choix de l'utilisateur. On peut ensuite effectuer un ensemble d'actions de la même manière. Les résultats ou les éventuelles erreurs sont affichés.

On prévoit 3 minutes pour la démonstration.

### Conclusion

1. **Ce qui est fait :** HTTP, Services, Config, Log, observation des fichiers
2. **Ce qu'il reste à faire :**
    - mettre le moteur de synchronisation en place ;
    - interface graphique ;
    - portage sur d'autres systèmes ;
    - chiffrement des données transférées et de la base de données ;
3. **Critiques :**
    - mauvaise évaluation/planification du temps sur le Gantt initial (pas de prise en compte des rythmes de travail différents entre les vacances et les périodes de cours) ;
    - modélisation trop monolithique (en 1 à 2 mois complets), peut-être commencer à implémenter plus tôt.
4. **Ce qu'on a appris pendant le projet :**
    - compréhension et utilisation du protocole HTTP ;
    - API des services OneDrive et Dropbox ;
    - programmation concurrente, fils d'exécution, promesses, futurs ;
    - bibliothèques cURL, fswatch, SQLite, Boost::Thread ;
    - modélisation et gestion d'un projet de taille moyenne.
5. Perspectives du projet avec continuation en stage.
6. Remerciements : Mme Bouziane, jury, autres étudiants et personnes présentes.

On prévoit 1 minute 30 pour la conclusion.

![Plan soutenance](soutenance-plan.jpg)
![Plan de l'introduction](soutenance-p1.jpg)
![Plan de la partie HTTP](soutenance-p2.jpg)
![Plan de la partie Services](soutenance-p3.jpg)
![Propositions pour la démonstration](soutenance-p4.jpg)
![Plan de la conclusion](soutenance-p5.jpg)
