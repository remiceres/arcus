\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{planning}[2017/02/14 Création de plannings.]
\LoadClass{article}
\RequirePackage{tikz}

\pagestyle{empty}

% Style d’événement prenant en argument la hauteur en heures
% dudit événement
\tikzset{event/.style={
    draw,
    fill=white,
    rectangle,
    anchor=north west,
    line width=0.4pt,
    inner sep=0.3333em,
    text width={\daywidth-0.6666em-0.4pt},
    minimum height=#1*\hourheight,
    align=center
}}

\renewcommand*\title[1]{
    \section*{Planning stage R\'{e}mi et Matt\'{e}o~~~\small{#1}}
}

\newcommand*\event[5]{
    \node[event=#3] at (#1, #2) {#4 -- #5};
}

\newenvironment{planning}
{
% Largeur d’un jour
\newcommand*\daywidth{.2*(\textwidth - 1.5cm)}

% Hauteur d’une heure
\newcommand*\hourheight{3em}

% Première heure du jour
\newcommand*\firsthour{7}

% Dernière heure du jour
\newcommand*\lasthour{17}

\vfill
\begin{tikzpicture}[y=-\hourheight,x=\daywidth]
    % Liste des heures
    \foreach \time in {\firsthour, ..., \lasthour} {
        \draw[loosely dashed, draw=black!20] (1, \time) -- (6, \time);
        \node[anchor=east] at (1, \time) {\time{}h};
    }

    % Noms des jours et séparateurs
    \foreach \day [count=\daynum] in {Lundi, Mardi, Mercredi, Jeudi, Vendredi} {
        \draw (\daynum, \firsthour-1) -- (\daynum, \lasthour+1);
        \node at (.5+\daynum, \firsthour-.5) {\day};
    }

    % Fermeture du cadre
    \draw (1, \firsthour-1) -- (6, \firsthour-1);
    \draw (1, \lasthour+1) -- (6, \lasthour+1);
    \draw (6, \firsthour-1) -- (6, \lasthour+1);
}
{
\end{tikzpicture}
\vfill
}
