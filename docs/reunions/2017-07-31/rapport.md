# Rapport de stage

## Plan

1. Introduction
    1. Présentation du Lirmm
    1. Présentation du projet
    1. Planning « prévisionnel »
1. Contenu
    1. Objectifs & problématiques
    1. Méthodologies & solutions
1. Conclusion
    1. Résultats
    1. Difficultés rencontrées
    1. Apports
    1. Perspectives
