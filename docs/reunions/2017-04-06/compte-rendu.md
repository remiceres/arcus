# Journée de stage du 06/04/2017

*LIRMM, bâtiment 4, 9h–19h.*

Quatrième journée de stage au LIRMM.

## Correction du bug lié à fswatch

Au rechargement de la configuration suite à sa modification, on observait une corruption de la mémoire. Pour modifier la liste des chemins observés, il faut arrêter le surveilleur et le relancer avec la nouvelle liste.

Le code relançait le moniteur depuis la fonction de rappel appelée par le premier moniteur, ce qui l’empêchait de s’arrêter, et causait une lecture d’adresse incorrecte puisqu’on détruisait le moniteur encore en activité.

Utilisation des pointeurs intelligents pour automatiser la gestion de l’instance de `monitor`.

![Schéma des différents fils d’exécution et des pointeurs intelligents](fswatch-thread-bug.jpg)

## Choix du lieu de stockage de la liste des services

Question du choix du lieu de stockage de la liste des services : dans la base indépendante de chaque zone de synchronisation ou bien dans la configuration locale du dossier `.config`.

**Liste des avantages et inconvénients :**

![Avantages et inconvénients des deux choix de lieux de stockage](choix-stockage-services.jpg)

**Décision :** choix n°2 pour sa flexibilité et sa capacité à émuler le choix n°1 avec un petit peu plus de code.

## Gestion de la base de données

### Révision de la structure

Révision de la structure de la base de données :

* les fichiers sont identifiés par UUID au lieu de leur chemin ;
* structure d’entrée au lieu de fichier pour pouvoir inclure les dossiers vides ;
* table de relation père-fils pour la structure ;
* fichier spécial {00000000-0000-0000-0000-000000000000} pour désigner la racine ;
* nouvelle table métadonnées (Info) : numéro de version de la base et nom.

![Nouvelle structure de la base de données](revision-structure-base.jpg)

### Classe de gestion de la base

Création d’une classe de base de données dont la responsabilité est de maintenir la connexion à SQLite, de s’assurer de l’intégrité de la base à l’ouverture et de la corriger si possible, et de permettre la création, mise à jour, suppression de données.

![Modélisation de la classe de gestion de la base](modelisation-base-donnees.jpg)

### Classe moteur de synchronisation

Moteur de synchronisation utilisant une grande partie des blocs définis précédemment.

![Modélisation du moteur de synchronisation](modelisation-syncer-2.jpg)
