# Algorithmes de synchronisation

La base est dotée d’un numéro de version incrémenté à chaque modification (ajout, suppression, déplacement de fichiers).
On conserve une file d’attente de changements locaux, vidée régulièrement pour envoyer au serveur les changements.

## Scénarios

1. Détection de modification locale sans modification sur le serveur
2. Détection de modification distante sans modification locale
3. Détection de modification locale avec modification distante

## Comment détecter à froid (au démarrage) ?

```
comparaison de la base locale avec l’état du système de fichiers
ajout dans la file d’attente des modifications détectées
lancement de la boucle principale de synchronisation
```

## Comment détecter à chaud (pendant l’exécution) ?

```
télécharger le numéro de version distant

si la file de changements est vide alors
	si numéro de version local < numéro de version distant alors
		rapatrier les données
	fin si
sinon
	si numéro de version local < numéro de version distant alors
		régler les confilts
	sinon
		envoyer les données
	fin si
fin si
```

## Question

Comment s’assurer qu’il n’y ait pas d’accès concurrent entre les lignes 12 et 16, 20, 22 ? -> idée : fichier de verrouillage
