# Réunion du 24/01/2017

*Bât 16, durée 45 min.*

Discussion sur le cahier des charges, l’organisation du projet, le découpage des fichiers et le fichier d’index.

(Rédiger et intégrer sur le rapport la réflexion ayant amené aux solutions, pour pas qu'elles ne sortent de nulle part.)

## API

Interface REST et traduction pour les services non-compatibles.

## Découpage, index et efficacité

Découpage en fonction de la taille d'un fichier et du niveau de confidentialité ? Taille de bloc fixe et blocs incomplets pour les fichiers de petite taille ou les derniers blocs des fichiers de taille non multiple. Stockage de chaque morceau comme un fichier dans le service de stockage. Détection de changements par sommes de contrôle.

Base d’index répliquée sur tous les stockages. Utilisation d’une base de données plutôt qu’un fichier manuellement géré permettant une meilleure efficacité lors de la recherche d’un fichier par chemin. Base contentant les infos nécessaires pour reconstituer un fichier à partir des blocs découpés.

![Modélisastion de la base d’index](modele-index.jpg)

*Problème des sommes de contrôle :* tous les blocs doivent être mis à jour si quelque chose est ajouté en début de fichier.
*À tester :* copie des fichiers d’une base de données, est-ce portable ?

Mme Bouziane effectue la demande à une collègue concernant les systèmes de bases de données les plus adaptés à ce genre de manipulations (copie des fichiers).

## Organisation

Diagramme de Gantt à créer, d’abord approximatif car pas de date de rendu explicite pour le moment. Chiffement dans un second temps.
Mise en place d’un dépôt Git pour la gestion des versions.

*À faire :* envoyer un email à M. Ulliana pour obtenir la date limite de rendu.
*À faire :* créer un dépôt GitLab et inviter Mme Bouziane.

Il faudra d’abord modéliser, puis effectuer des choix techniques en conséquence, progressivement.

### Cahier des charges

*Corrections/améliorations à faire.*

application => _application cliente_
préciser le titre "Premier démarrage" => Premier démarrage du programme

Décrire plus précisément comment l'utilisateur interagit avec le démon. En utilisant une comparaison avec Dropbox et son dossier particulier dans le système de fichiers.

Réduire le nombre de termes techniques dans la description fonctionnelle.

Description du programme selon comment sont traités les fichiers.
