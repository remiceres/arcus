# Journée de stage du 20/06/2017

*LIRMM, bâtiment 4, 8h - 12h*

## Synthèse des travaux sur les flux HTTP

Les deux précédentes semaines ont été consacrées à un changement important d’architecture sur le module HTTP. En effet, l’architecture précédente lançait les requêtes et ne transmettait la réponse à l’utilisateur (qui est le module Services) qu’une fois les en-têtes **ET** le contenu reçus.

Ce mode de fonctionnement est inadapté pour la gestion des erreurs car nous avons besoin de connaître le contenu des en-têtes de la réponse afin de juger de ce qu’il faut faire avec son contenu : notamment, faut-il le rediriger dans un fichier ? le mettre en mémoire pour analyse ultérieure ? l’ignorer ? annuler le transfert ?

La nouvelle architecture adopte le fonctionnement suivant :

* la promesse renvoyée par la méthode `Sender::send(Request req)` est résolue dès lors que les en-têtes sont reçus, et n’attend plus la réception complète de celle-ci ;
* l’utilisateur reçoit un objet Response qui permet de définir le flux dans lequel les données doivent être dirigées, puis de demander l’écriture des données de la réponse, via la méthode `Response::finish()` ;
* entre le moment où la promesse est résolue et le moment où la méthode `Response::finish()` est appelée, l’envoi est mis en pause ;
* dès lors que l’instance de Response est détruite, tout envoi est interrompu.

Ainsi, seules les portions de code nécessitant la réception de données sont modifiées pour prendre en compte la réception en deux étapes, les autres restant inchangées.

_Exemple d’envoi de requête simple et synchrone :_

```c++
HTTP::Sender sender;

HTTP::Request req = HTTP::Request()
    .setVerb(HTTP::Verb::POST)
    .setUrl("http://example.com");

sender.send(req).get();
```

Ce code est inchangé entre l’ancienne et la nouvelle architecture car la réponse est ignorée.

_Exemple d’envoi de requête avec analyse des en-têtes de réponse :_

```c++
HTTP::Sender sender;

HTTP::Request req = HTTP::Request()
    .setVerb(HTTP::Verb::POST)
    .setUrl("http://example.com");

HTTP::Response res = sender.send(req).get();

std::cout << res.getCode() << "\n";
std::cout << res.getHeaderSet().getHeaders("Date") << "\n";
```

Ce code est inchangé entre l’ancienne et la nouvelle architecture car seules des données d’en-tête sont utilisées, et aucunement le contenu de la réponse.

_Exemple d’envoi de requête avec redirection conditionnelle du flux de sortie :_

```c++
HTTP::Sender sender;

HTTP::Request req = HTTP::Request()
    .setVerb(HTTP::Verb::GET)
    .setUrl("http://example.com");

HTTP::Response res = sender.send(req).get();

if (res.getCode() == 200)
{
    std::ifstream out("resultat.html");
    res.setOutStream(out);
    res.finish().get();

    std::cout << "Tout s’est bien passé. Résultat dans 'resultat.html'.\n";
}
else
{
    std::cerr << "Code d’erreur du serveur : " << res.getCode() << "\n";
}
```

Ce code est uniquement possible avec la nouvelle architecture. Si le code de retour de la réponse indique 200 OK, le contenu est transféré dans un nouveau fichier créé pour l’occasion. Sinon, aucun fichier n’est créé, l’envoi est abandonné (`Response` détruite) et une erreur est affichée.

La création de cette architecture a nécessité une longue réflexion et plusieurs essais et échecs, avec un nombre important de bogues de synchronisation inter-tâches à résoudre.

Cette nouvelle architecture va permettre l’implémentation d’une gestion d’erreurs efficace et descriptive au niveau du module Services.
