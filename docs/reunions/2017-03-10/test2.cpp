#include <thread>
#include <iostream>
#include <atomic>
#include <mutex>

int main()
{
    std::mutex verrou;

    std::thread t1([&]()
    {
        verrou.lock();

        std::cout << "aei";
        std::cout.flush();
        std::cout << "ouy";
        std::cout.flush();
        std::cout << "aei";
        std::cout.flush();
        std::cout << "ouy";
        std::cout.flush();

        verrou.unlock();
    });

    std::thread t2([&]()
    {
        verrou.lock();

        std::cout << "bcdf";
        std::cout.flush();
        std::cout << "ghjk";
        std::cout.flush();
        std::cout << "lmnp";
        std::cout.flush();
        std::cout << "rstv";
        std::cout.flush();

        verrou.unlock();
    });

    t1.join();
    t2.join();

    return 0;
}
