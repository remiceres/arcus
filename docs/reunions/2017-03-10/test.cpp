#include <future>
#include <array>
#include <iostream>
#include <cstdlib>

void factorielle(std::promise<int> promesse_fact)
{
    std::this_thread::sleep_for(std::chrono::seconds(5));

    if (time(NULL) % 2 == 0)
    {
        promesse_fact.set_value(42);
    }
    else
    {
        promesse_fact.set_exception(
            std::make_exception_ptr(
                std::runtime_error("Ceci est un test")
            )
        );
    }
}

int main(int argc, char** argv)
{
    std::promise<int> promesse_fact;
    std::future<int> valeur_fact = promesse_fact.get_future();
    std::thread thread_fact(factorielle, std::move(promesse_fact));

    std::cout << "Attente du résultat \\";
    char animation[] = "|/-\\";
    std::size_t index = 0;

    do
    {
        std::cout << "\b";
        std::cout << animation[index];
        index++;

        if (index >= 4)
        {
            index = 0;
        }

        std::cout.flush();
    }
    while (
        valeur_fact.wait_for(std::chrono::milliseconds(100)) !=
        std::future_status::ready
    );

    std::cout << "\nEt voilà : " << valeur_fact.get() << std::endl;
    thread_fact.join();

    return EXIT_SUCCESS;
}
