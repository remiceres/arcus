# Journée de stage du 30/05/2017

*LIRMM, bâtiment 4, 8h–16h.*

Premiere journée de la second partie du stage au LIRMM.

## Explication et revue des modification hors stage
- Implémentation de Onedrive
- Factorisation des ensemble d'en-tetes dans `Request` et `Response`.
- explication implémenttion et fonctionement requettes delayé
## Amélioration test HTTP
Modification du test HTTP afin de tester les requettes delayer et la file de priorité d'éxécution des requettes

## Amélioration log
- tranformation implementation des logs en un flux.
    - permet de pourvoir chainner les messages
    - permet les convertion implicite, simplifiant l'API
- Mise en place d'un systemme permetant la coloration du texte seulement lors de l'écriture vers le terminal.

## Recherche et modelisation de la methodes de gestion des erreurs
![Recherches de la fonction de gestion d'erreur](recherche_gestion_erreur.jpg)

Création d'un methode générale (que nous appellerons ici aquarelle) qui prend en entrée :
- une methode permettant de traité les cas d'erreur spécifique
- une methode de traitement du résultat
- requete

1. Envoie la requette.
2. traite les cas d'erreur généreaux.
3. Traite les cas d'erreur spécifique.
4. si erreur alors exeption.
5. sinon traitement du résultat.
