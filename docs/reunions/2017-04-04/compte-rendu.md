# Journée de stage du 04/04/2017

*LIRMM, bâtiment 4, 9h15–18h50.*

Deuxième journée de stage au LIRMM. Avancement sur le projet et réunion pour faire le point sur l’avancement.

## Système de configuration

Conception et mise en place du système de configuration.

* Chaque ordinateur possède son propre fichier de configuration.
* Il contient la liste des répertoires à synchroniser :
    * Chaque répertoire de synchronisation se voit attribuer un UUID.
    * La configuration associe chaque UUID à sa position dans l’arborescence de fichiers.
    * Chaque répertoire de synchronisation contient sa propre base de données indépendante.

![Organisation de la configuration](configuration-organisation.jpg)

Création d’une classe permettant l’écriture et la lecture du fichier de configuration au format JSON. Création d’une classe de génération des UUID. Le diagramme UML commun est mis à jour.

![Modélisation de la configuration](configuration-modelisation.jpg)

Intégration du système de configuration dans l’algorigramme du programme principal.

![Algorigramme principal v2](algorigramme-prog-principal.jpg)

La validation des UUID après leur génération soulève un problème : comment centraliser le format des journaux de messages d’information, avertissement et erreurs ?

## Système de journal

Conception et implantation d’un système de journal permettant d’écrire des message formattés avec information sur la date, l’heure, le niveau de sévérité, la source du message (fichier et ligne) et le message lui-même.

Possibilité d’écrire le journal sur n’importe quel flux (prévision dans le futur de l’écriture du journal dans un fichier de /var/log, notamment quand le programme sera mis en daemon).

Format des messages de journalisation :

![Format du journal](logger-format.jpg)

Modélisation des classes de journaux :

![Modélisation de la gestion du journal](logger-modelisation.jpg)

## Réunion avec l’encadrante

Point sur l’avancement du projet :

* bibliothèque de gestion de la base de données ;
* bibliothèque d’observation de l’arborescence de fichiers ;
* gestion du fichier de configuration ;
* possibilité de synchroniser indépendamment plusieurs répertoires.

Pour la recherche bibliographique, voir avec AEB pour confirmer le choix du sujet et la possibilité ou non de travailler à plusieurs.

Le rapport doit être rendu dans un mois. Plan-type conseillé pour le rapport :

1. Introduction
2. Objectifs
3. Organisation projet & rapport
4. Conception, analyse, réflexion
5. Implémentation
6. Résultats et discussion (poursuite en stage)
7. Bilan, difficultés
8. Conclusion

Les difficultés peuvent inclure les révisions faites au planning d’organisation suite à un blocage, les UE utiles dans la réalisation du projet.

Il faudra d’abord rédiger un plan avec des intitulés aussi détaillés que possible. Le plan doit donner une idée du projet. À rendre en fin de semaine. Prévision d’une **première version du rapport pour le 22 avril**, pour corrections et avis de Mme Bouziane.

Pour la présentation, lors de la démonstration, il faudra prévoir une vidéo pour pallier la possibilité de panne du réseau. La vidéo doit montrer une synchronisation entre plusieurs ordinateurs.
