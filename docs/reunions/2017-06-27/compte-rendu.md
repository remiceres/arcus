# Journée de stage du 27/06/2017

*LIRMM, bâtiment 4, 8h - 18h*

## Soutenance et rapport

* **Rapport :** ensemble ou séparé ? 15 pages ? À voir AEB.
* **Soutenance :** obligatoire ou pas ? Quand ? Devant qui ? À voir avec AEB.

## Projet année prochaine

Pour l’année prochaine, possibilité de proposer un sujet de projet pour les L2/L3 pour poursuivre le développement d’Arcus.

Pour le projet de L3, une personne qui pourrait éventuellement être intéressée :

`alain.grezes@etu.umontpellier.fr`
