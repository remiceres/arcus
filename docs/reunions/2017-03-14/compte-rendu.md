# Réunion du 14/03/2017

*Bât 16, durée 45 minutes.*

Démonstration de la progression du projet, discussion sur la programmation parallèle et le stage.

## Présentation

Présentation du code réalisé pour l’abstraction de cURL, avec notamment les classes `Request`, `Response` et `Sender` de l’espace de noms `HTTP`.

Présentation du code réalisé pour l’implantation de la communication avec Dropbox et démonstration de la communication avec le service.

## Programmation parallèle

Penser à rendre le code gérant la communication réseau parallèle afin de permettre plusieurs requêtes simultanées.

Envoi du cours de L3 sur la programmation parallèle par Mme Bouziane. Référence : C++ Concurrency in Action, Anthony Williams (voir texte intégral sur le web).

## Stage

Répartir le travail sur la partie pré-partiels afin de pouvoir avoir un résultat présentable le jour de l’oral et pour le rapport. Répartir le travail sur la partie post-partiels (stage).

Se renseigner auprès du Lirmm pour :

* la liste des documents à fournir en vue du stage ;
* les horaires d’ouverture ;
* les heures à faire en tant que stagiaire avec le mi-temps (cc. Baert) ;

Rédiger un résumé « contenu scientifique du stage » en environ 2 paragraphes. Réaliser la convention de stage en vue de la signature.
