![Plan premières parties](plan-1.jpg)
![Plan deuxièmes parties](plan-2.jpg)
![Plan dernières parties](plan-3.jpg)

# Introduction

Contexte dans l'introduction
Reprise du contexte dans le cahier des charges (présentation sous forme de problème à réutiliser).

Objectif principal => Problèmes à résoudre pour réaliser l'objectif => Objectifs précis

## Motivation du choix

Cf accroche cahier des charges.

## Objectifs

Mots-clés : cloud, stockage distribué de fichiers
Éviter les mots-clés techniques dans le plan

# Organisation du projet

## Organisation du travail

## Répartition du travail dans le temps

Faire apparaître le diagramme de Gantt.

## Outils

Faire passer les outils après la conception. La conception est indépendante des outils.

# Conception

Qualifier sur ce qui est réalisation.

# Implémentation/Réalisation

==> Choix des outils ici.

Le tableau permet d'introduire la partie et de mettre en contexte les outils utilisés.

+ Citation des noms des outils dans le corps.

Qualifier après le titre ce sur quoi il porte.

Supprimer la répétition au début des titres

Mise en cohérence des sous-titres : sous forme d'action pour les interfaces.

# Bilan et difficultés recontrées

## Bilan

Ce qui a abouti
(éventuellement tests et tests de performance)

# Conclusion et perspectives

Les résultats peuvent être rappelés.

## Conclusion

## Perspectives

Ce qu'il reste à faire (à déplacer ici)

=> Penser à séparer entre corps du texte et annexes pour gagner des pages

I.      2 pages
II.     2 pages
III.    5 pages
IV.     4 pages
V.      1 page
VI.     1 page
