# Journée de stage du 06/07/2017

**LIRMM, bâtiment 4, 8h - 18h**

## Projet robotique

_Springer Handbook of Robotics_<br>
Siciliano Khatib (2008)

Partie E, robots distribués.

### Chapitre 40 — Systèmes multirobots en déplacement

Permettent de répartir une tâche complexe entre plusieurs agents. Adapté aux tâches distribuées. Améliore la robustesse puisqu’en cas de défaillance d’un des robots, un autre peut prendre le relais.

#### Architectures

* **Centralisée :** la coordination se fait via un point central. <br> _Requiert la communication d’une quantité importante d’informations au point de contrôle. Avantageux quand le point de contrôle a une meilleure vision de la situation. Réduit la fiabilité du système._
* **Hiérarchique :** chaque robot coordonne les actions de certains autres, plus bas dans la hiérarchie. Semblable au système militaire. <br> _Plus fiable, mais faible en cas de défaillance d’un robot haut dans la hiérarchie._
* **Décentralisée (la plus commune) :** chaque robot se contrôle lui-même. Seul le programme de contrôle est partagé.<br> _Haute résistance aux défaillance, mais plus difficile d’avoir un comportement cohérent._
* **Hybride :** chaque robot se contrôle lui-même ainsi que certains autres (mélange entre l’architecture hiérarchique et l’architecture décentralisée).

#### Communication

* **Implicite :** communication stigmergique via les traces laissées dans l’environnement par les autres robots. <br> _Simple par son absence de protocole explicite, mais limitée par la qualité de la perception du robot._
* **Passive :** utilisation de capteurs pour observer le comportement des autres robots. <br> _Ne dépend pas d’un système de communication faillible, mais limité également par la perception du robot et sa capacité à interpréter les actions des autres._
* **Explicite :** communication volontaire entre les robots, par exemple via des ondes électromagnétiques ou un réseau, afin de transmettre des informations. <br> _Communication directe et information complète, mais nécessite un système de communication tolérant aux pannes et fiable. Souvent, le moyen de communication ne relie pas en permanence tous les membres._

#### Exemples d’applications

* Ramassage d’objets
* Contrôle de groupes de robots (formations)
* Manipulation d’objets
* Observation de plusieurs cibles
* Contrôle du trafic et planification de chemin
* Football
* [Swarm Robot](https://www.youtube.com/watch?v=G1t4M2XnIhI)

### Chapitre 41 — Robots en réseau

Appareil robotique connecté à un réseau de type LAN ou Internet, avec ou sans fil, avec TCP, UDP ou 802.11 (définition de l’IEEE). On distingue deux types de robots en réseau :

* opérés par des humains qui envoient des commandes sur le réseau ;
* autonomes et transmettant des informations à travers le réseau auquel ils sont connectés.
