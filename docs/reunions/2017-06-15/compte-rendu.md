# Journée de stage du 15/06/2017

*LIRMM, bâtiment 4, 8h - 18h*

Poursuite des travaux sur les flux HTTP.

## Réunion sur le module de synchronisation/découpage

Avec Mmes Hinde Bouziane et Anne-Elisabeth Baert.

Vue d'ensemble des problèmes à résoudre pour la synchronisation :

* taille de blocs ;
* redondance du stockage ;
* découpage ;
* que optimiser ?

### Taille des blocs

Les blocs doivent avoir une taille fixe pour simplifier l'implémentation.

On veut minimiser la quantité de données envoyées. Donc pas une taille fixe mais une taille maximale permettant d'insérer de façon incrémentielle quand une modification est effectuée au milieu du fichier.

Il faut donc stocker la taille du bloc dans la base de données.

Faut-il adapter la taille maximale aux types de fichiers ?

### Redondance

On stocke le même bloc à plusieurs endroits, et non pas en chevauchement entre plusieurs blocs.

Combien de copies faut-il stocker pour réduire le risque de perte ?

### Découpage

S'inspirer des recherches sur rsync.

Faut-il chiffrer avant ou après avoir découpé ? Est-il plus performant de chiffrer plusieurs petits fichiers ou un gros fichier.

### (Re)-distribution

Il faut redistribuer lors de l'ajout d'un nouveau service et de la suppression du service.

Comment réaliser cette redistribution ? Ne pas tout redistribuer mais seulement certaines parties pour réduire le temps de redistribution.

Que faire s'il manque un bloc, un bloc est introuvable ? Avertir l'utilisateur.

Choisir quel ordre de remplissage ? Algorithme glouton. Politiques de placement.

* Placement initial.
* Ajout de données imprévues par l'utilisateur dans le service.
* Ajout de fichier complet.
* Modification du fichier.

Ordonnancement ? Commencer par le plus d'espace libre.

Faut-il prioriser la redondance de certains blocs ? Lesquels ? Les plus utilisés ?

Que faire quand la taille des services est non-homogène ? P. exemple un service à 1 téra et un service de 5 gigaoctets. Faut-il limiter le stockage à 5 Go ? Sinon on perd la redondance !


## Réunion sur le projet de L3

Possibilité de faire un projet en transdisciplinaire avec des étudiants en électronique.

### Dates et créneaux

En L3 premier semestre, lundi après-midi libre.

### Possibilités de sujets de projet

- Programmation parallèle pour le contrôle (temps réel).
- Coopération entre plusieurs robots (éventuellement entre plusieurs types de robots).
- Apporter une contribution sur le projet v-rep.
- Sur le système d'exploitation ROS.

Mobilisation des connaissances en physique.

### Technologies

V-Rep, ROS et Matlab.
Éventuellement microcontrôleurs.

### Idées

- Cartographie via robot ? Construction plan en mémoire.
