# Document de recherches

## API services

Upload sur OneDrive : `curl -X PUT https://api.onedrive.com/v1.0/drive/root:/Test.txt:/content --header "Authorization: bearer <<TOKEN>>" --header "Content-Type: text/plain" --upload-file Documents/liens.txt`

### Dropbox

Référence de l’API HTTP : https://www.dropbox.com/developers/documentation/http/documentation

Token : B-oQg9Pb3FQAAAAAAAADm8a_OTS7zcar7A1cor0Et4V0A3G7v8QsIYdsvGUxXsp0

#### Upload

Upload : `curl -X POST https://content.dropboxapi.com/2/files/upload --header "Authorization: Bearer <<TOKEN>>" --header "Dropbox-API-Arg: {\"path\": \"/Test.txt\",\"mode\": \"add\",\"autorename\": true,\"mute\": false}" --header "Content-Type: application/octet-stream" --data-binary @/home/matteo/Documents/liens.txt`

#### Liste des contenus dans un dossier

Point d’entrée : https://api.dropboxapi.com/2/files/list_folder

https://www.dropbox.com/developers/documentation/http/documentation#files-list_folder

#### Quota

Point d’entrée : https://api.dropboxapi.com/2/users/get_space_usage

https://www.dropbox.com/developers/documentation/http/documentation#users-get_space_usage

## API JSON

* https://github.com/nlohmann/json

## Entiers 64 bits sur des machines 32 bits

* http://stackoverflow.com/questions/16521066/is-u-int64-t-available-on-32-bit-machine

## Chiffrement de l’URL dans HTTPS

* http://stackoverflow.com/questions/187655/are-https-headers-encrypted
* http://stackoverflow.com/questions/499591/are-https-urls-encrypted

## Détection des changements dans le système de fichiers

Niveau noyau :

* inotify : noyau Linux
  https://en.wikipedia.org/wiki/Inotify
* kqueue : noyau FreeBSD/OS X
  https://en.wikipedia.org/wiki/Kqueue

Nivau utilisateur :

* libkqueue : couche de compatibilité kqueue
  https://github.com/mheily/libkqueue
* fswatch : observateur multiplateformes avec librairie et exécutable
  https://github.com/emcrisostomo/fswatch/
  http://emcrisostomo.github.io/fswatch/doc/1.9.3/fswatch.pdf
* efsw : Entropia File System Watcher
  https://bitbucket.org/SpartanJ/efsw
* Qt : QFileSystemWatcher
  http://doc.qt.io/qt-5/qfilesystemwatcher.html
