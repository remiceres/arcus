/** @file */

#ifndef ARCUS_SYNC_ENGINE
#define ARCUS_SYNC_ENGINE

#include <libfswatch/c++/monitor.hpp>
#include <boost/filesystem.hpp>
#include <sqlite3.h>

#include "Services/Service.hpp"

namespace Sync
{
    class Engine
    {
    public:
        Engine(boost::filesystem::path path);
        ~Engine();

        bool start();

        bool handleEvent(fsw::event);

        // void addServices(Services::Service services);
        // void removeServices(Services::Service services);

    private:
        boost::filesystem::path _path;
        sqlite3 *_db = nullptr;
        std::vector<Services::Service> _services;

        void _checkDatabase();

        void _addFile(boost::filesystem::path path);
        void _moveFile(boost::filesystem::path path);
        void _deleteFile(boost::filesystem::path path);

        static const std::string DB_FILE_NAME;
    };
}

#endif // ARCUS_SYNC_ENGINE
