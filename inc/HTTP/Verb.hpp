/** @file */

#ifndef ARCUS_HTTP_VERB
#define ARCUS_HTTP_VERB

#include <string>

namespace HTTP
{
    /**
     * Liste des verbes possibles pour une requête HTTP.
     */
    enum class Verb
    {
        GET,
        HEAD,
        POST,
        PUT,
        DELETE
    };

    /**
     * Convertit un verbe HTTP en sa représentation textuelle.
     *
     * @param verb Verbe HTTP dont il faut obtenir une représentation.
     * @return Représentation textuelle du verbe.
     */
    std::string verbToString(Verb verb);
}

#endif // ARCUS_HTTP_VERB
