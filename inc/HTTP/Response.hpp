/** @file */

#ifndef ARCUS_HTTP_RESPONSE
#define ARCUS_HTTP_RESPONSE

#include "Header.hpp"
#include "config.hpp"

#include <cstdlib>
#include <string>
#include <sstream>
#include <vector>
#include <curl/curl.h>
#include <boost/thread/future.hpp>

namespace HTTP
{
    class Sender;

    /**
     * Réponse HTTP reçue d’un serveur.
     *
     * @see Request Pour construire des requêtes HTTP à envoyer.
     * @see Sender::send Pour envoyer une requête en vue d'obtenir une réponse.
     */
    class Response
    {
    public:
        /**
         * Crée une nouvelle réponse en déplaçant une autre.
         *
         * @param src Réponse à déplacer.
         */
        Response(Response&& src);

        /**
         * Assigne à cette réponse une autre réponse déplacée.
         *
         * @param src Réponse à déplacer.
         */
        Response& operator=(Response&& src);

        /**
         * Supprime cette réponse, arrête tout transfert relatif et libère
         * implicitement les ressources associées.
         */
        ~Response();

        /**
         * Récupère le code de retour HTTP.
         *
         * @return Code de retour HTTP.
         */
        long getCode() const;

        /**
         * Accède à l'ensemble des en-têtes de la réponse .
         *
         * @return Ensemble des en-têtes de la réponse.
         */
        const HeaderSet& getHeaderSet() const;

        /**
         * Définit ou modifie le flux vers lequel sera redirigé le contenu de
         * la réponse du serveur.
         *
         * @param out_stream Flux de sortie vers lequel rediriger le contenu.
         * @return Instance actuelle pour le chaînage.
         */
        Response& setOutStream(std::ostream& out_stream);

        /**
         * Relance les transferts associés à la réponse et les termine.
         *
         * @return Valeur future renseignée dès que tous les transferts sont
         * terminés.
         */
        boost::future<void> finish() const;

        // Classe Sender autorisée à accéder aux attributs privés
        friend Sender;

        // Interdiction de la copie des réponses
        Response(const Response& src) = delete;
        Response& operator=(const Response& src) = delete;

    private:
        /**
         * Construit une nouvelle réponse.
         *
         * @param sender Envoyeur de requêtes gérant cette réponse.
         * @param simple Instance de cURL gérant l'envoi relatif.
         */
        Response(Sender* sender, CURL* simple);

        Sender* _sender = nullptr;
        CURL* _simple = nullptr;

        long _code = 0;
        HeaderSet _headers;
    };
}

#endif // ARCUS_HTTP_RESPONSE
