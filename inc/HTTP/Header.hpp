/** @file */

#ifndef ARCUS_HTTP_HEADER
#define ARCUS_HTTP_HEADER

#include <vector>
#include <map>
#include <string>

namespace HTTP
{
    /**
     * En-tête HTTP.
     */
    class Header
    {
    public:
        using Name = std::string;
        using Value = std::string;

        /**
         * Construit un nouvel en-tête HTTP.
         *
         * @param name Nom de l'en-tête.
         * @param value Valeur de l'en-tête.
         */
        Header(Name name, Value value);

        /**
         * Convertit l'en-tête en une chaîne de caractère formattée au
         * standard HTTP.
         *
         * @return Chaîne représentant l'en-tête.
         */
        operator std::string() const;

        /**
         * Récupère le nom de l'en-tête.
         *
         * @return HeaderName Nom de l'en-tête.
         */
        const Name& getName() const;

        /**
         * Récupère la valeur de l'en-tête.
         *
         * @return HeaderValue Valeur de l'en-tête.
         */
        const Value& getValue() const;

    private:
        Name _name;
        Value _value;
    };

    /**
     * Ensemble d'en-têtes HTTP tel qu'échangé dans une requête ou
     * une réponse HTTP.
     *
     * @see Request Pour effectuer des requêtes HTTP.
     * @see Response Pour lire des réponses HTTP.
     */
    class HeaderSet
    {
    public:
        /**
         * Récupère une liste des en-têtes.
         *
         * @param [name=""] Nom des en-têtes à récupérer, ou vide pour tout.
         * @return Liste des en-têtes correspondants.
         */
        std::vector<Header> getHeaders(Header::Name name = "") const;

        /**
         * Récupère une valeur entière dans l'en-tête de nom donné.
         *
         * Ignore les en-têtes malformés. Si aucun en-tête portant le nom donné
         * n'a de valeur entière correcte, renvoie la valeur maximale pouvant
         * être portée par un entier 64 bits.
         *
         * @param name Nom de l'en-tête à récupérer.
         * @return Valeur entière.
         */
        int64_t getHeaderAsInt(Header::Name name) const;

        /**
         * Récupère le type MIME de la réponse spécifiée dans l’en-tête
         * Content-Type. S’il n’est pas spécifié par le serveur, renvoie
         * le type « application/octet-stream ».
         *
         * @return Chaîne de caractères identifiant le type MIME.
         */
        std::string getMIMEType() const;

        /**
         * Ajoute un nouvel en-tête.
         *
         * Si un en-tête avec le même nom existe déjà, en ajoute un autre.
         *
         * @param name Nom de l’en-tête à ajouter.
         * @param value Valeur de l’en-tête à ajouter.
         * @return L’instance actuelle pour le chaînage.
         */
        HeaderSet& addHeader(Header::Name name, Header::Value value);

        /**
         * Supprime des en-têtes.
         *
         * @param [name=""] Nom des en-têtes à supprimer, ou vide pour tout.
         * @return L'instance actuelle pour le chaînage.
         */
        HeaderSet& removeHeaders(Header::Name name = "");

    private:
        std::multimap<Header::Name, Header::Value> _set;
    };
}

#endif // ARCUS_HTTP_HEADER
