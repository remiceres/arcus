#ifndef ARCUS_HTTP_ERROR
#define ARCUS_HTTP_ERROR

#include <stdexcept>
#include <string>

namespace HTTP
{
    /**
     * Erreur générique provoquée par l’envoi d’une requête HTTP.
     */
    class Error : public std::runtime_error
    {
    public:
        /**
         * Construit une nouvelle erreur du module HTTP.
         *
         * @param code Code d’erreur tel que rapporté par cURL, si applicable.
         * @param description Description en toutes lettres de l’erreur cURL.
         */
        Error(int code, std::string description);

        /**
         * Récupère le code d’erreur cURL associé.
         *
         * @return Valeur du code d’erreur cURL associé.
         */
        int getCode() const;

        /**
         * Récupère la description de l’erreur cURL en toutes lettres.
         *
         * @return Description de l’erreur cURL.
         */
        std::string getDescription() const;

    private:
        int _code;
        std::string _description;
    };

    namespace Errors
    {
        /**
         * Erreur déclenchée lorsque la formulation de la requête par le
         * client est invalide si bien qu’elle ne peut être transmise au
         * serveur ou que celui-ci a refusé de la satisfaire.
         */
        class Request : public Error
        {
        public:
            using Error::Error;
        };

        /**
         * Erreur déclenchée lorsque la réponse du serveur n’est pas acceptable
         * et/ou ne peut pas être interprétée par le module.
         */
        class Response : public Error
        {
        public:
            using Error::Error;
        };

        /**
         * Erreur déclenchée lorsqu’une connexion chiffrée par SSL a été
         * demandée mais qu’il est impossible de se connecter de façon sécurisée
         * au serveur.
         */
        class SSL : public Error
        {
        public:
            using Error::Error;
        };

        /**
         * Erreur déclenchée lorsque il est impossible de se connecter au
         * serveur en raison d’un problème de réseau.
         */
        class Network : public Error
        {
        public:
            using Error::Error;
        };

        /**
         * Erreur déclenchée lorsque la lecture ou l’écriture dans un flux
         * associé à la requête ou la réponse échoue prématurément.
         */
        class IO : public Error
        {
        public:
            using Error::Error;
        };
    }
}

#endif // ARCUS_HTTP_ERROR
