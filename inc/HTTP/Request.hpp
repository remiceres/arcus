/** @file */

#ifndef ARCUS_HTTP_REQUEST
#define ARCUS_HTTP_REQUEST

#include <chrono>
#include <string>

#include "Verb.hpp"
#include "Header.hpp"

namespace HTTP
{
    class Response;

    /**
     * Requête HTTP envoyable à un serveur.
     *
     * Représente une requête HTTP à envoyer sur le réseau.
     *
     * @see Sender::send Pour envoyer des requêtes préparées.
     */
    class Request
    {
    public:
        using TimeClock = std::chrono::steady_clock;

        /**
         * Construit une nouvelle requête.
         */
        Request();

        /**
         * Récupère l’URL cible de la requête.
         *
         * @return URL cible de la requête.
         */
        const std::string& getUrl() const;

        /**
         * Définit ou modifie l’URL cible de la requête.
         *
         * Il doit s’agir d’une URL complète spécifiant le protocole,
         * le serveur et le chemin.
         *
         * @param url URL cible.
         * @return L’instance actuelle pour le chaînage.
         */
        Request& setUrl(std::string url);

        /**
         * Accède à l'ensemble des en-têtes de la requête en lecture seule.
         *
         * @return Ensemble des en-têtes de la requête.
         */
        const HeaderSet& getHeaderSet() const;

        /**
         * Accède à l'ensemble des en-têtes de la requête en lecture/écriture.
         *
         * Il est garanti que tous les en-têtes spécifiés dans cet ensemble
         * seront transmis au serveur. Toutefois, des en-têtes supplémentaires
         * peuvent être ajoutés si besoin.
         *
         * @return Ensemble des en-têtes de la requête.
         */
        HeaderSet& getHeaderSet();

        /**
         * Récupère la méthode/le verbe HTTP de la requête.
         *
         * @return Verbe à utiliser pour la requête.
         */
        const Verb& getVerb() const;

        /**
         * Définit ou modifie la méthode/le verbe HTTP à utiliser pour
         * envoyer la requête.
         *
         * Par défaut, le verbe GET est utilisé.
         *
         * @param verb Verbe à utiliser.
         * @return L’instance actuelle pour le chaînage.
         */
        Request& setVerb(Verb verb);

        /**
         * Récupère l'adresse du flux en lecture depuis lequel lire des
         * données à adjoindre à la requête.
         *
         * @return Flux en lecture contenant les données.
         */
        std::istream* getInStream() const;

        /**
         * Définit ou modifie le flux en lecture depuis lequel lire des
         * données à adjoindre à la requête.
         *
         * Si la méthode utilisée n'est pas PUT ou POST, ces données sont
         * ignorées.
         *
         * @param in_stream Flux de lecture depuis lequel lire les données.
         * @return L’instance actuelle pour le chaînage.
         */
        Request& setInStream(std::istream &in_stream);

        /**
         * Récupère les données à envoyer avec la requête POST.
         *
         * @return Données à envoyer avec la requête POST.
         */
        const std::string& getData() const;

        /**
         * Définit ou modifie les données à envoyer avec la requête POST.
         *
         * Si la méthode utilisée n'est pas POST, ces données sont ignorées.
         *
         * @param data Données à adjoindre au POST.
         * @return L’instance actuelle pour le chaînage.
         */
        Request& setData(std::string data);

        /**
         * Vérifie si le suivi des redirections est activé ou non.
         *
         * @return Booléen témoin de l’activation du suivi des redirections.
         */
        bool getFollowRedirects();

        /**
         * Active ou désactive le suivi des redirections HTTP. Si activé et
         * que le serveur renvoie une redirection, alors celle-ci est suivie
         * de manière transparente et la réponse est résolue dès que le
         * serveur cesse de rediriger. Sinon, la réponse est résolue avec
         * le code de redirection. Par défaut, ce suivi est désactivé.
         *
         * @param bool Vrai pour activer le suivi des redirections.
         * @return L’instance actuelle pour le chaînage.
         */
        Request& setFollowRedirects(bool follow_redirects);

        /**
         * Récupère l'heure d'envoi prévue pour la requête.
         *
         * @return Heure prévue pour l'envoi de la requête.
         */
        const std::chrono::time_point<TimeClock>& getTime() const;

        /**
         * Définit l'heure absolue d'envoi prévue pour la requête.
         *
         * Par défaut, les requêtes sont envoyées immédiatement après leur
         * envoi dans un envoyeur de requête. Si un point temporel dans le futur
         * est défini dans cette méthode, alors la requête ne sera envoyée
         * qu'au plus tôt lorsque ce point temporel sera atteint. Si un point
         * temporel dans le passé est défini, alors la requête sera envoyée
         * immédiatement.
         *
         * @see setDelay Pour spécifier un délai relatif avant l'envoi.
         *
         * @param time Heure prévue pour l'envoi de la requête.
         * @return L'instance actuelle pour le chaînage.
         */
        Request& setTime(std::chrono::time_point<TimeClock> time);

        /**
         * Définit le délai qui doit être respecté avant de tenter d'envoyer
         * la requête.
         *
         * Par défaut, il n'y a pas de délai. Si cette valeur est définie,
         * la requête sera envoyée au plus tôt lorsque ce délai aura été
         * dépassé depuis le moment où il est défini dans la requête.
         *
         * @param delay Délai d'envoi de la requête.
         * @return L'instance actuelle pour le chaînage.
         */
        template<typename Rep, typename Period>
        Request& setDelay(std::chrono::duration<Rep, Period> delay)
        {
            setTime(Request::TimeClock::now() + delay);
            return *this;
        }

    private:
        std::string _url;
        HeaderSet _headers;
        Verb _verb = Verb::GET;
        std::string _data;
        bool _follow_redirects = false;
        
        std::istream* _in_stream = nullptr;
        std::chrono::time_point<TimeClock> _time;
    };
}

#endif // ARCUS_HTTP_REQUEST
