/** @file */

#ifndef ARCUS_HTTP_URL
#define ARCUS_HTTP_URL

#include <string>

namespace HTTP
{
    namespace URL
    {
        /**
         * Encode une chaîne de caractères au format d’encodage pourcentage,
         * en vue de son inclusion dans une URL.
         *
         * @param input Chaîne d’entrée à encoder.
         * @return Chaîne encodée.
         */
        std::string encode(const std::string& input);

        /**
         * Décode une chaîne de caractères encodée au format pourcentage.
         *
         * @param input Chaîne d’entrée à décoder.
         * @return Chaîne décodée.
         */
        std::string decode(const std::string& input);
    }
}

#endif // ARCUS_HTTP_URL
