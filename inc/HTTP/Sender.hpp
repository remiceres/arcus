/** @file */

#ifndef ARCUS_HTTP_SENDER
#define ARCUS_HTTP_SENDER

#include "config.hpp"
#include "CurlPool.hpp"
#include "Request.hpp"
#include "Response.hpp"

#include <curl/curl.h>
#include <boost/thread/future.hpp>
#include <fstream>
#include <thread>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include <map>
#include <queue>
#include <vector>

namespace HTTP
{
    /**
     * Permet l'envoi de requêtes HTTP à un serveur.
     *
     * @see Request Pour construire les requêtes à envoyer.
     * @see Response Pour analyser les réponses du serveur.
     */
    class Sender
    {
    public:
        /**
         * Crée un nouvel objet envoyeur de requêtes.
         */
        Sender();

        /**
         * Détruit un objet envoyeur de requêtes.
         */
        ~Sender();

        // Interdiction de la copie des envoyeurs
        Sender(const Sender&) = delete;
        Sender& operator=(const Sender&) = delete;

        /**
         * Envoie une requête HTTP de façon asynchrone.
         *
         * @param req Requête HTTP à envoyer.
         * @return Réponse HTTP renvoyée par le serveur suite à la requête.
         * Cette réponse ne contient que l'en-tête. Pour récupérer les données
         * renvoyées, utiliser receiveContent.
         */
        boost::future<Response> send(Request& req);

        /**
         * Vérifie si l'envoyeur est en mode verbeux ou non. Si oui, des
         * informations détaillées seront affichées sur la sortie standard pour
         * toutes les requêtes HTTP suivantes.
         *
         * @return Vrai si et seulement si l'envoyeur est en mode verbeux.
         */
        bool getVerbose() const;

        /**
         * Active ou désactive le mode verbeux pour les requêtes à venir.
         *
         * @param verbose Vrai pour activer le mode verbeux.
         */
        void setVerbose(bool verbose);

        /**
         * Récupère le nombre maximal d'envois pouvant être réalisés en
         * parallèle. Si plus d'envois sont mis en attente, ils seront réalisés
         * par groupe d'envois avec la cardinalité paramétrée ici.
         *
         * @return Le nombre maximum d'envois réalisés en parallèle.
         */
        unsigned int getConcurrency() const;

        /**
         * Modifie le nombre maximal d'envois pouvant être réalisés en
         * parallèle.
         *
         * @param concurrency Nombre maximum d'envois réalisés en parallèle.
         */
        void setConcurrency(unsigned int concurrency);

        // Classe Response autorisée à accéder aux attributs privés
        friend Response;

    private:
        /**
         * S'assure que le thread d’envoi est démarré.
         *
         * Les envois sont mis dans la file d'attente par la méthode @see send()
         * et sont traités en parallèle par le thread d'envoi. Ce thread utilise
         * cURL pour communiquer avec le serveur afin de récupérer la réponse
         * aux requêtes envoyées.
         *
         * Si le thread a été démarré mais s'est arrêté en raison d'une
         * erreur, relance le thread.
         */
        void _start();

        /**
         * Prépare un nouvel envoi correspondant à une requête soumise.
         * Crée une instance cURL ou en réutilise une et la configure pour
         * correspondre à la requête, renseigne les informations sur l'envoi
         * et l'ajoute en file d'attente.
         *
         * @param req Requête devant être satisfaite.
         * @return Instance de cURL pour l'envoi.
         */
        CURL* _prepare(Request& req);

        /**
         * Nettoie les ressources associées à l'instance de cURL donnée si
         * elle ne l'est pas déjà.
         *
         * @param simple Instance de cURL à nettoyer.
         */
        void _clean(CURL* simple);

        /**
         * Signale la fin de l’envoi associé à l’instance de cURL donnée.
         * Résoud la promesse de fin avec une exception seulement si le code
         * de retour indique une erreur.
         *
         * @param simple Instance de cURL associé à l’envoi terminé.
         * @param code Code de retour de l’envoi.
         */
        void _terminate(CURL* simple, CURLcode code);

        /**
         * Fonction du thread d’envoi.
         *
         * Le thread d’envoi s’arrête lorsque le drapeau `_send_running`
         * passe à faux. Le thread peut être réveillé par la variable
         * conditionnelle `_send_wakeup`.
         */
        void _sendWorker();

        /**
         * Écrit dans le tampon de cURL les données à envoyer au serveur,
         * lues depuis le flux de la requête.
         *
         * @param buffer Tampon dans lequel écrire les données.
         * @param size Taille de chaque élément du buffer.
         * @param nmemb Nombre d’éléments dans le buffer.
         * @param userdata Pointeur sur le flux de données de la requête à lire.
         * @return Nombre d'octets écrits dans le tampon.
         */
        static std::size_t _sendRequestData(
            char* buffer, std::size_t size,
            std::size_t nmemb, void* userdata
        );

        /**
         * Ajoute un en-tête envoyé par le serveur dans la liste
         * en-têtes reçus. Si l'en-tête HTTP est terminé, satisfait
         * la promesse de réponse et met en pause le transfert avant
         * de déterminer si le client souhaite recevoir le contenu de
         * la réponse.
         *
         * @param buffer Tampon contenant le texte de l’en-tête.
         * @param size Taille de chaque élément du buffer.
         * @param nmemb Nombre d’éléments dans le buffer.
         * @param userdata Pointeur sur l'envoi en question.
         * @return Nombre d’octets interprétés avec succès.
         */
        static std::size_t _receiveResponseHeader(
            char* buffer, std::size_t size,
            std::size_t nmemb, void* userdata
        );

        /**
         * Écrit des données envoyées par le serveur dans un flux.
         *
         * @param buffer Tampon contenant les données reçues.
         * @param size Taille de chaque élément du buffer.
         * @param nmemb Nombre d’éléments dans le buffer.
         * @param userdata Pointeur sur la flux à écrire.
         * @return Nombre d’octets interprétés avec succès.
         */
        static std::size_t _receiveResponseData(
            char* buffer, std::size_t size,
            std::size_t nmemb, void* userdata
        );

        // Indique si des informations détaillées doivent être affichées sur
        // la sortie standard à propos des envois à venir.
        bool _verbose = false;

        // Nombre maximal d'envois pouvant être réalisés en parallèle.
        unsigned int _concurrency = 5;

        // Thread d’envoi.
        std::thread _send_worker;

        // Signal d'arrêt pour le thread d’envoi.
        bool _send_running;

        // Signal permettant de réveiller le thread d’envoi lorsqu’il est
        // en sommeil car il n’a plus de travail en attente.
        std::mutex _wakeup_mutex;
        std::condition_variable _send_wakeup;

        // Mutex permettant de verrouiller l'accès aux données d'envoi
        // (informations sur les requêtes, réponses, promesses) partagées
        // entre le thread principal et le thread d'envoi
        std::recursive_mutex _sendings_mutex;

        // Instance cURL principale.
        CURLM* _multi;

        // Piscine d’instances cURL secondaires.
        CurlPool _pool;

        /**
         * Données associées à un envoi.
         */
        struct _Sending
        {
            /**
             * Envoyeur de requêtes responsable de cet envoi.
             */
            Sender* parent;

            /**
             * Instance de cURL relative à cet envoi.
             */
            CURL* simple;

            /**
             * Liste chaînée d’en-têtes de la requête à libérer à la fin
             * de l’envoi.
             */
            struct curl_slist* headers;

            /**
             * Vrai si et seulement si l'envoi a été relancé.
             */
            bool resumed;

            /**
             * Flux de sortie dans lequel sont écrites les données renvoyées
             * par le serveur.
             */
            std::ostream* out_stream;

            /**
             * Informations sur la requête.
             *
             * @see Request
             */
            Request req;

            /**
             * Informations sur la réponse.
             *
             * @see Response
             */
            Response res;

            /**
             * Promesse d'en-têtes.
             *
             * Cette promesse est satisfaite avec l'objet de réponse dès lors
             * que les en-têtes HTTP ont été intégralement reçus du serveur.
             */
            boost::promise<Response> headers_promise;

            /**
             * Promesse de fin d'envoi.
             *
             * Cette promesse est satisfaite dès lors que tous les transferts
             * relatifs à l'envoi sont terminés.
             */
            boost::promise<void> finish_promise;
        };

        /**
         * Compare deux envois et renvoie le plus prioritaire.
         *
         * L'envoi le plus prioritaire est toujours celui dont la requête
         * a une date la plus antérieure. Ainsi, les envois les plus en retard
         * sont priorisés.
         */
        struct _SendingComparator
        {
            Sender& parent_sender;
            bool operator()(const CURL* lhs, const CURL* rhs);
        };

        _SendingComparator _sending_comparator;

        // Liste des envois en traitement, en attente, ou en pause, indicés par
        // leur adresse  d'instance cURL.
        std::map<const CURL*, _Sending> _sendings;

        // Liste des envois en cours de traitement.
        std::set<const CURL*> _current_sendings;

        // File d'attente des envois en attente de traitement
        std::priority_queue<
            CURL*,
            std::vector<CURL*>,
            _SendingComparator
        > _pending_sendings;

        // Liste des envois pouvant être remis en route après avoir été
        // mis en pause
        std::queue<CURL*> _resumable_sendings;
    };
}

#endif // ARCUS_HTTP_SENDER
