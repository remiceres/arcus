/** @file */

#ifndef ARCUS_HTTP_CURL_POOL
#define ARCUS_HTTP_CURL_POOL

#include <curl/curl.h>
#include <stack>

namespace HTTP
{
    /**
     * Piscine permettant de réutiliser des instances de cURL.
     */
    class CurlPool
    {
    public:
        /**
         * Construit une nouvelle piscine vide.
         */
        CurlPool() = default;

        /**
         * Détruit toutes les instances de requêtes cURL stockés
         * dans la piscine, puis la piscine elle-même.
         */
        ~CurlPool();

        // Interdiction de la copie des piscines
        CurlPool(const CurlPool& src) = delete;
        CurlPool& operator=(const CurlPool& src) = delete;

        /**
         * Retire une instance de cURL de la piscine. Réutilise
         * une instance existante si possible, en crée une nouvelle
         * si possible.
         *
         * @return Instance de requête cURL.
         */
        CURL* pop();

        /**
         * Remet une instance de cURL dans la piscine pour être
         * réutilisé plus tard. Après la remise dans la piscine,
         * l'instance ne doit plus être utilisée.
         *
         * @param inst Instance à remettre.
         */
        void push(CURL* inst);

    private:
        // Maximum d'instances pouvant être conservées par la piscine.
        static const std::size_t MAX_INSTANCES = 10;

        // Pile des instances réutilisables.
        std::stack<CURL*> _available_instances;
    };
}

#endif // ARCUS_HTTP_CURL_POOL
