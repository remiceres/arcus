/** @file */

#ifndef ARCUS_SERVICES_QUOTA
#define ARCUS_SERVICES_QUOTA

#include <cstdint>

namespace Services
{
    /**
     * Quota de stockage disponible sur un compte d’un fournisseur
     * de services de stockage.
     *
     * @see Service::getQuota()
     */
    class Quota
    {
    public:
        /**
         * Construit une nouvelle instance de quota.
         *
         * @param used Quantité de stockage utilisée en octets.
         * @param allocation Quantité totale de stockage allouée en octets.
         */
        Quota(uint64_t used, uint64_t allocation);

        /**
         * Récupère l’utilisation actuelle du stockage.
         *
         * @return Utilisation actuelle en octets.
         */
        uint64_t getUsed() const;

        /**
         * Récupère l’allocation totale d'espace pour l'utilisateur sur le
         * service, incluant l'espace utilisé et l'espace restant.
         *
         * @return Allocation actuelle en octets.
         */
        uint64_t getAllocation() const;

    private:
        uint64_t _used = 0;
        uint64_t _allocation = 0;
    };
}

#endif // ARCUS_SERVICES_QUOTA
