/** @file */

#ifndef ARCUS_SERVICES_DROPBOX_SERVICE
#define ARCUS_SERVICES_DROPBOX_SERVICE

#include "Service.hpp"
#include <string>

namespace Services
{
    /**
     * Connexion avec le fournisseur de services de stockage Dropbox.
     */
    class DropboxService : public Service
    {
    public:
        /**
         * Construit une nouvelle instance de service Dropbox permettant
         * d'exécuter des actions sur un compte Dropbox.
         *
         * @param sender Envoyeur de requêtes HTTP à utiliser.
         * @param [token=""] Jeton d'authentification à utiliser pour accéder
         * à Dropbox. Si ce jeton est laissé vide, il faudra d'abord connecter
         * l'utilisateur pour utiliser le service.
         */
        DropboxService(HTTP::Sender& sender, std::string token = "");

        boost::future<void> login();

        boost::future<void> putFile(
            boost::filesystem::path source,
            boost::filesystem::path target,
            unsigned int retries = 5
        );

        boost::future<void> fetchFile(
            boost::filesystem::path source,
            boost::filesystem::path target,
            unsigned int retries = 5
        );

        boost::future<void> removeFile(
            boost::filesystem::path path,
            unsigned int retries = 5
        );

        boost::future<std::vector<Entry>> listDir(
            boost::filesystem::path path,
            unsigned int retries = 5
        );

        boost::future<Quota> getQuota(unsigned int retries = 5);

    private:
        static const std::string APP_ID;
        static const std::string REDIRECT_URL;
        static const std::string API_BASE_URL;
        static const std::string CONTENT_BASE_URL;

        std::string _token;
    };
}

#endif // ARCUS_SERVICES_DROPBOX_SERVICE
