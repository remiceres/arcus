/** @file */

#ifndef ARCUS_SERVICES_SERVICE
#define ARCUS_SERVICES_SERVICE

#include <vector>
#include <string>
#include <boost/filesystem/path.hpp>

#include "HTTP/Sender.hpp"

namespace Services
{
    class Quota;
    class Entry;

    /**
     * Fournisseur de services de stockage distant.
     *
     * Cette interface décrit les actions communes à tous les fournisseurs de
     * services de stockage.
     */
    class Service
    {
    public:
        /**
         * Construit une nouvelle instance du service.
         *
         * @param sender Envoyeur de requêtes HTTP à utiliser.
         */
        explicit Service(HTTP::Sender& sender);

        /**
         * Détruit une instance de service.
         */
        virtual ~Service();

        /**
         * Connecte un utilisateur au service.
         *
         * @return Valeur future remplie dès que la connexion est terminée.
         * Une exception est placée dans la valeur future si la connexion
         * échoue.
         */
        virtual boost::future<void> login() = 0;

        /**
         * Téléverse un fichier sur le service.
         *
         * @param source Flux en lecture contenant les données à téléverser.
         * @param target Chemin où stocker le fichier sur le service.
         * @param [retries=5] Nombre maximal d'essais à effectuer.
         * @return Valeur future remplie dès que le téléversement est terminé.
         * Une exception est placée dans la valeur future si le téléversement
         * échoue.
         */
        virtual boost::future<void> putFile(
            boost::filesystem::path source,
            boost::filesystem::path target,
            unsigned int retries = 5
        ) = 0;

        /**
         * Télécharge un fichier depuis le service.
         *
         * @param source Chemin depuis lequel lire les données sur le service.
         * @param target Flux en écriture dans lequel stocker les données.
         * @param [retries=5] Nombre maximal d'essais à effectuer.
         * @return Valeur future remplie dès que le téléchargement est terminé.
         * Une exception est placée dans la valeur future si le téléchargement
         * échoue.
         */
        virtual boost::future<void> fetchFile(
            boost::filesystem::path source,
            boost::filesystem::path target,
            unsigned int retries = 5
        ) = 0;

        /**
         * Supprime un fichier sur le service.
         *
         * @param path Chemin du fichier à supprimer.
         * @param [retries=5] Nombre maximal d'essais à effectuer.
         * @return Valeur future remplie dès que la suppression est effectuée.
         * Une exception est placée dans la valeur future si la suppression
         * échoue.
         */
        virtual boost::future<void> removeFile(
            boost::filesystem::path path,
            unsigned int retries = 5
        ) = 0;

        /**
         * Récupère la liste des enfants d’un répertoire sur le service.
         *
         * @param path Chemin du dossier sur le service dont il faut lister
         * le contenu.
         * @param [retries=5] Nombre maximal d'essais à effectuer.
         * @return Valeur future remplie dès que la liste est récupérée avec
         * une liste d'@see Entry correspondant à la liste des enfants du
         * répertoire donné. Une exception est placée dans la valeur future si
         * la suppression échoue.
         */
        virtual boost::future<std::vector<Entry>> listDir(
            boost::filesystem::path path,
            unsigned int retries = 5
        ) = 0;

        /**
         * Récupère le quota actuel de l’utilisateur sur le service.
         *
         * @param [retries=5] Nombre maximal d'essais à effectuer.
         * @return Valeur future remplie dès que le quota est récupéré avec une
         * structure contenant le nombre d'octets alloués à l'utilisateur au
         * total et le nombre d'octets utilisés actuellement. Une exception est
         * placée dans la valeur future si la suppression échoue.
         */
        virtual boost::future<Quota> getQuota(unsigned int retries = 5) = 0;

    protected:
        HTTP::Sender& _sender;

        /**
         * Permet de réessayer l’envoi de la requête plusieurs fois dans
         * le cas où le code de réponse est 429, 500 ou 503. Respecte
         * l’en-tête « Retry-After » dans la mesure du possible et réessaye
         * avec un intervalle exponentiel autrement.
         *
         * @param req Requête HTTP à envoyer.
         * @param retries Nombre maximum d'essais à faire.
         * @param delay Délai maximal avant de faire l’essai suivant.
         * @return Fonction permettant de réessayer l’envoi.
         */
        boost::future<HTTP::Response> retry(
            HTTP::Request req,
            unsigned int retries,
            uint64_t delay = 1
        );
    };
}

#endif // ARCUS_SERVICES_SERVICE
