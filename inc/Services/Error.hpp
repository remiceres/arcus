#ifndef ARCUS_SERVICES_ERROR
#define ARCUS_SERVICES_ERROR

#include <stdexcept>
#include <string>

namespace Services
{
    /**
     * Erreur générique provoquée par un appel à une méthode d’un objet
     * permettant de manipuler un service de stockage dans les nuages.
     */
    class Error : public std::runtime_error
    {
    public:
        /**
         * Construit une nouvelle erreur du module Services.
         *
         * @param message Message d’erreur.
         */
        explicit Error(std::string message);
    };

    namespace Errors
    {
        /**
         * Erreur déclenchée lorsque le serveur a pu être contacté mais refuse
         * de répondre aux requêtes.
         *
         * @see HTTP::NetworkError Pour lorsque le serveur ne peut pas être
         * contacté car le réseau n’est pas disponible.
         */
        class ServerUnavailable : public Error
        {
        public:
            using Error::Error;
        };

        /**
         * Erreur déclenchée lorsque les moyens d’authentification fournis
         * ne permettent pas de se connecter au compte de l’utilisateur.
         */
        class AuthenticationFailure : public Error
        {
        public:
            using Error::Error;
        };

        /**
         * Erreur déclenchée lorsque la ressource distante n’est pas disponible,
         * par exemple si le chemin est invalide ou ne pointe pas vers une
         * ressource utilisable.
         */
        class ResourceUnavailable : public Error
        {
        public:
            using Error::Error;
        };

        /**
         * Erreur déclenchée lorsque le quota de l’utilisateur ne lui permet
         * plus d’ajouter de nouvelles données dans le service.
         */
        class QuotaExceeded : public Error
        {
        public:
            using Error::Error;
        };

        /**
         * Erreur déclenchée lorsque l’utilisateur tente de manipuler une
         * ressource à laquelle il n’a pas accès.
         */
        class PermissionDenied : public Error
        {
        public:
            using Error::Error;
        };
    }
}

#endif // ARCUS_SERVICES_ERROR
