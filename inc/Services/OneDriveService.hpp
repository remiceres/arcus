/** @file */

#ifndef ARCUS_SERVICES_ONEDRIVE_SERVICE
#define ARCUS_SERVICES_ONEDRIVE_SERVICE

#include <string>

#include "Service.hpp"

namespace Services
{
    /**
     * Connexion avec le fournisseur de services de stockage OneDrive.
     */
    class OneDriveService : public Service
    {
    public:
        /**
         * Construit une nouvelle instance de service OneDrive permettant
         * d'exécuter des actions sur un compte OneDrive.
         *
         * @param sender Envoyeur de requêtes HTTP à utiliser.
         * @param [token=""] Jeton d'authentification à utiliser pour accéder
         * à OneDrive. Si ce jeton est laissé vide, il faudra d'abord connecter
         * l'utilisateur pour pouvoir utiliser le service.
         */
        OneDriveService(HTTP::Sender& sender, std::string token = "");

        boost::future<void> login();

        boost::future<void> putFile(
            boost::filesystem::path source,
            boost::filesystem::path target,
            unsigned int retries = 5
        );

        boost::future<void> fetchFile(
            boost::filesystem::path source,
            boost::filesystem::path target,
            unsigned int retries = 5
        );

        boost::future<void> removeFile(
            boost::filesystem::path path,
            unsigned int retries = 5
        );

        boost::future<std::vector<Entry>> listDir(
            boost::filesystem::path path,
            unsigned int retries = 5
        );

        boost::future<Quota> getQuota(unsigned int retries = 5);

    private:
        static const std::string APP_ID;
        static const std::string APP_PERMISSIONS;
        static const std::string APP_SECRET;
        static const std::string REDIRECT_URL;
        static const std::string API_BASE_URL;
        static const std::string AUTH_BASE_URL;

        /**
         * Transforme la réponse du serveur en gérant les éventuelles erreurs
         * signalées. Si la réponse indique un succès, elle est renvoyée
         * immédiatement. Si une erreur d’authentification se produit, tente
         * de réauthentifier l’utilisateur. Sinon, une exception normalisant
         * l’erreur qui s’est produite est générée et placée dans une valeur
         * future.
         *
         * @param [allow_reauth=true] Si vrai, alors en cas d’erreur
         * d’authentification, retente d’authentifier l’utilisateur.
         * @return Fonction à chaîner à une valeur future contenant une réponse
         * dans laquelle est placée soit une réponse en cas de succès soit une
         * exception avec l’erreur normalisée dans le cas inverse.
         */
        boost::future<HTTP::Response> _sendToOneDrive(
            HTTP::Request req, int retries,
            bool allow_reauth = true
        );

        /**
         * Obtient un jeton d'accès à partir d'un code d'autorisation ou
         * d'un jeton de rafraîchissement.
         *
         * @param [code=""] Code d'autorisation à utiliser. Si vide, utilise le
         * jeton de rafraîchissement.
         * @return Valeur future remplie lorsque le jeton est obtenu.
         * Une exception est placée dans la future si le rafraîchissement
         * échoue.
         */
        boost::future<void> _renewToken(std::string code = "");

        std::string _access_token;
        std::string _refresh_token;
    };
}

#endif // ARCUS_SERVICES_ONEDRIVE_SERVICE
