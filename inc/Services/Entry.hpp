/** @file */

#ifndef ARCUS_SERVICES_ENTRY
#define ARCUS_SERVICES_ENTRY

#include <string>
#include <boost/filesystem/path.hpp>

namespace Services
{
    class Entry
    {
    public:
        enum class Type
        {
            FILE,
            FOLDER
        };

        Entry(
            Type type,
            boost::filesystem::path path,
            std::string checksum = "",
            uint64_t size = 0
        );

        Type getType() const;
        boost::filesystem::path getPath() const;
        std::string getChecksum() const;
        uint64_t getSize() const;

    private:
        Type _type;
        boost::filesystem::path _path;
        std::string _checksum;
        uint64_t _size;
    };
}

#endif // ARCUS_SERVICES_ENTRY
