/** @file */

#ifndef ARCUS_CONFIG_LOCAL
#define ARCUS_CONFIG_LOCAL

#include <map>
#include <vector>
#include <string>
#include <boost/filesystem.hpp>

#include "UID.hpp"

namespace Config
{
    /**
     * Gestionnaire de la configuration locale du programme.
     *
     * La configuration locale est différente sur chaque ordinateur et n’est
     * pas synchronisée dans les nuages. Elle est stockée au format JSON dans
     * le dossier de configuration de l’utilisateur.
     */
    class Local
    {
    public:
        /**
         * Construit un nouveau gestionnaire de configuration locale
         * gérant le fichier de configuration par défaut.
         */
        Local();

        /**
         * Construit un nouveau gestionnaire de configuration locale.
         *
         * @param path Fichier de configuration à gérer.
         */
        Local(boost::filesystem::path path);

        // Interdiction de la copie des configurations locales
        Local(const Local& src) = delete;
        Local& operator=(const Local& src) = delete;

        /**
         * Lit les données depuis le fichier de configuration locale. La
         * configuration doit être lue avant toute opération de modification.
         *
         * @return Vrai si et seulement si l’ouverture a réussi.
         */
        bool open();

        /**
         * Ajoute un nouveau répertoire à surveiller dans la configuration
         * locale, en connaissant déjà son identifiant unique.
         *
         * @param id Identifiant unique du nouveau répertoire à surveiller.
         * @param path Chemin absolu vers le répertoire à surveiller.
         */
        void addMonitoredDirectory(UID id, boost::filesystem::path path);

        /**
         * Ajoute un nouveau répertoire à surveiller dans la configuration
         * locale.
         *
         * @param path Chemin absolu vers le répertoire à surveiller.
         * @return Identifant unique du nouveau répertoire à surveiller.
         */
        UID addMonitoredDirectory(boost::filesystem::path path);

        /**
         * Supprime un répertoire surveillé de la configuration locale.
         *
         * @param id Identifiant unique du répertoire surveillé.
         */
        void removeMonitoredDirectory(UID id);

        /**
         * Récupère la liste des répertoires surveillés actuellement.
         *
         * @return Liste des identifiants uniques des répertoires surveillés.
         */
        std::vector<UID> getMonitoredDirectories() const;

        /**
         * Récupère le chemin d'un répertoire surveillé à partir de son
         * identifiant unique.
         *
         * @param id Identifiant unique du répertoire surveillé.
         * @return Chemin absolu vers répertoire surveillé..
         */
        const boost::filesystem::path& getMonitoredDirectoryPath(UID id) const;

        /**
         * Récupère le chemin dans lequel est enregistrée cette configuration
         * locale.
         *
         * @return Chemin d’enregistrement de la configuration.
         */
        const boost::filesystem::path& getPath() const;

        /**
         * Modifie le chemin dans lequel est enregistrée cette configuration
         * locale.
         *
         * @param path Chemin dans lequel enregistrer la configuration.
         */
        void setPath(const boost::filesystem::path& path);

        /**
         * Récupère le chemin par défaut pour le stockage du fichier de
         * configuration locale sur le système actuel.
         *
         * @return Chemin du fichier de configuration.
         */
        static boost::filesystem::path getDefaultPath();

    private:
        boost::filesystem::path _path;
        std::map<UID, boost::filesystem::path> _monitored_directories;

        /**
         * Écrit la configuration vers le fichier.
         *
         * @return Vrai si et seulement si l’écriture du fichier a réussi.
         */
        bool _write() const;
    };
}

#endif // ARCUS_CONFIG_LOCAL
