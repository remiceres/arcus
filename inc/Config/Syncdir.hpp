/** @file */

#ifndef ARCUS_CONFIG_SYNCDIR
#define ARCUS_CONFIG_SYNCDIR

#include <boost/filesystem.hpp>
#include <sqlite3.h>
#include <vector>
#include <string>
#include <map>

namespace Config
{
    /**
     * Ligne de résultat dans la base de données de configuration.
     */
    typedef std::map<std::string, std::string> Row;

    /**
     * Gestionnaire de la configuration d’un répertoire synchronisé.
     *
     * Cette configuration est une base de données synchronisée avec les
     * différents fournisseurs  de stockage dans les nuages. Elle est stockée
     * au format SQLite.
     */
    class Syncdir
    {
    public:
        /**
         * Construit un nouveau gestionnaire de configuration de répertoire
         * synchronisé.
         *
         * @param path Fichier de configuration à gérer.
         */
        Syncdir(boost::filesystem::path path);
        ~Syncdir();

        // Interdiction de la copie des configurations locales
        Syncdir(const Syncdir& src) = delete;
        Syncdir& operator=(const Syncdir& src) = delete;

        /**
         * Ouvre la base de données stockant la configuration. La base
         * doit être ouverte avant tout appel aux méthodes de modification.
         *
         * @return Vrai si et seulement si l’ouverture a réussi.
         */
        bool open();

    private:
        /**
         * Exécute une requête sur la base de données ouverte.
         *
         * @param req Requête SQL à exécuter.
         * @return Liste des lignes de résultat de la requête.
         */
        std::vector<Row> _exec(std::string req);

        /**
         * Échappe les guillemets doubles dans une chaîne de caractères
         * en les remplaçant par des doubles guillemets doubles.
         *
         * @example _escapeQuotes("test\"abc") = "test\"\"abc"
         * @return Chaîne de caractères échappée.
         */
        static std::string _escapeQuotes(std::string name);

        /**
         * S’assure que la structure de la base de données est suffisamment
         * bien formée pour pouvoir y stocker la configuration d’un dossier
         * synchronisé.
         *
         * Dans le cas où la structure est incorrecte, tente de la corriger
         * en créant ou supprimant des tables.
         *
         * @return Vrai si et seulement si la structure est prête à être
         * utilisée pour stocker la configuration.
         */
        bool _normalize();

        // Liste des tables utilisées dans la base de données de configuration
        static const std::vector<std::string> TABLES_LIST;

        // Liste des requêtes permettant de créer chaque table de configuration
        static const std::vector<std::string> TABLES_REQUESTS;

        boost::filesystem::path _path;
        sqlite3* _handle = nullptr;
    };
}

#endif // ARCUS_CONFIG_SYNCDIR
