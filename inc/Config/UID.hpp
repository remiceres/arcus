/** @file */

#ifndef ARCUS_CONFIG_UID
#define ARCUS_CONFIG_UID

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/string_generator.hpp>
#include <boost/uuid/random_generator.hpp>
#include <string>

namespace Config
{
    /**
     * Identifiant universellement unique.
     */
    class UID
    {
    public:
        /**
         * Génère un nouvel identifiant unique en utilisant
         * les librairies disponibles sur le système.
         */
        UID();

        /**
         * Initialise un identifiant unique à partir d'une
         * chaîne de caractères.
         *
         * @param source Chaîne de caractères source.
         */
        UID(const std::string& source);

        // Comparaison lexicographique d'identifiants uniques
        bool operator==(const UID& right) const;
        bool operator!=(const UID& right) const;
        bool operator<(const UID& right) const;
        bool operator<=(const UID& right) const;
        bool operator>(const UID& right) const;
        bool operator>=(const UID& right) const;

        /**
         * Convertit un identifiant unique en chaîne de
         * caractères.
         *
         * @return Chaîne de caractères représentant l'identifiant.
         */
        operator std::string() const;

        /**
         * Vérifie si un UID est l’UID nul.
         *
         * @return Vrai si et seulement si l’UID est nul.
         */
        bool isNull() const;

    private:
        static boost::uuids::string_generator string_generator;
        static boost::uuids::random_generator random_generator;
        boost::uuids::uuid _value;
    };
}

#endif // ARCUS_CONFIG_UID
