/** @file */

#ifndef ARCUS_LOG_PRINTER
#define ARCUS_LOG_PRINTER

#include <ostream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <chrono>

#include "Level.hpp"

/**
 * Macro permettant d’écrire un message informatif dans le journal global.
 */
#define LOG_INFO \
Log::global_printer.print(Log::Level::INFO, __FILE__, __LINE__)

/**
 * Macro permettant d’écrire un message d’avertissement dans le journal global.
 */
#define LOG_WARNING \
Log::global_printer.print(Log::Level::WARNING, __FILE__, __LINE__)

/**
 * Macro permettant d’écrire un message d’erreur dans le journal global.
 */
#define LOG_ERROR \
Log::global_printer.print(Log::Level::ERROR, __FILE__, __LINE__)

namespace Log
{
    /**
     * Gestionnaire de journalisation. Écrit les données du journal, priorisées
     * par niveau de sévérité.
     *
     * @example Écrire un message informatif dans le journal global.
     * LOG_INFO << "Ceci est un message d’information";
     *
     * @example Écrire un message d’erreur en signalant un autre emplacement
     * que celui actuel.
     * Log::global_printer(Log::Level::ERROR, "exemple.cpp", 31) << "Erreur !";
     */
    class Printer
    {
    public:
        /**
         * Crée un nouveau gestionnaire de journalisation.
         *
         * @param [outstream=stderr] Flux de sortie dans lequel écrire les
         * messages du journal.
         * @param [threshold=INFO] Seuil de sévérité au deçà duquel les messages
         * sont ignorés et ne sont pas écrits dans le journal.
         */
        Printer(
            std::ostream* outstream = &std::cerr,
            Level threshold = Level::INFO
        );

        /**
         * Formatte un point temporel en chaîne de caractères.
         *
         * @param timepoint Point temporel à formatter.
         * @return Chaîne représentant un point temporel.
         */
        template<typename Clock>
        static std::string timeToString(std::chrono::time_point<Clock> time)
        {
            std::time_t now = std::chrono::system_clock::to_time_t(
                std::chrono::system_clock::now()
                + (time - Clock::now())
            );

            std::tm* tm = std::localtime(&now);
            std::ostringstream out;

            out << std::put_time(tm, "%Y-%m-%dT%H:%M:%S");
            return out.str();
        }

        /**
         * Paramètre le gestionnaire de journalisation pour les prochains
         * messages.
         *
         * @param level Niveau de sévérité à utiliser.
         * @param file Nom du fichier à afficher.
         * @param line Numéro de ligne à utiliser.
         * @return L’instance actuelle pour le chaînage.
         */
        std::ostream& print(Level level, std::string file, int line);

        /**
         * Récupère le flux dans lequel le journal est écrit.
         *
         * @return Flux d’écriture du journal.
         */
        std::ostream* getOutStream() const;

        /**
         * Modifie le flux dans lequel écrire le journal.
         *
         * @param outstream Flux dans lequel écrire le journal.
         */
        void setOutStream(std::ostream* out_stream);

        /**
         * Récupère le seuil de filtrage des messages de journalisation.
         *
         * Les messages de journalisation de sévérité inférieure à ce seuil
         * seront ignorés.
         *
         * @return Niveau de sévérité minimal pour qu’un message soit
         * journalisé.
         */
        Level getThreshold() const;

        /**
         * Modifie le seuil de filtrage des messages de journalisation.
         *
         * @param threshold Nouveau seuil de filtrage.
         */
        void setThreshold(Level threshold);

    private:
        std::ostream* _out_stream;
        std::ofstream _null_stream;

        Level _threshold;
    };

    /**
     * Journal global de l’application.
     */
    extern Printer global_printer;
}

#endif // ARCUS_LOG_PRINTER
