/** @file */

#ifndef ARCUS_LOG_LEVEL
#define ARCUS_LOG_LEVEL

#include <string>

namespace Log
{
    /**
     * Liste des niveaux de sévérité des messages de journalisation.
     */
    enum class Level
    {
        INFO,
        WARNING,
        ERROR
    };

    /**
     * Convertit un niveau de sévérité en sa représentation textuelle.
     *
     * @param level Niveau dont il faut obtenir une représentation.
     * @param [colored=false] Vrai pour colorer le niveau.
     * @return Représentation textuelle du niveau.
     */
    std::string levelToString(Level level, bool colored = false);
}

#endif // ARCUS_LOG_LEVEL
