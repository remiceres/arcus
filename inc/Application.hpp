/** @file */

#ifndef ARCUS_APPLICATION
#define ARCUS_APPLICATION

#include <libfswatch/c++/monitor.hpp>
#include <memory>

#include "Config/Local.hpp"

/**
 * Point d’entrée de l’application permettant d’initialiser les
 * ressources.
 */
class Application
{
public:
    /**
     * Construit une nouvelle instance de l’application.
     */
    Application();

    /**
     * Démarre l’application.
     */
    void start();

    /**
     * Stoppe l’application.
     */
    void stop();

private:
    /**
     * Fonction de rappel appelée par fswatch pour traiter la liste
     * des événements survenus dans les répertoires surveillés.
     *
     * @param events Liste des événements survenus.
     * @param userdata Pointeur sur l’instance actuelle de l’application.
     */
    static void _onEvents(
        const std::vector<fsw::event>& events,
        void* userdata
    );

    /**
     * Traite un événement survenu dans un des répertoires surveillés.
     *
     * @param event Détails de l’événement.
     */
    void _onEvent(const fsw::event& event);

    /**
     * Configure le surveilleur avec la liste des répertoires à surveiller
     * puis démarre le surveilleur.
     */
    void _startMonitor();

    std::atomic<bool> _is_running;
    Config::Local _config;
    std::unique_ptr<fsw::monitor> _monitor = nullptr;
};

#endif // ARCUS_APPLICATION
