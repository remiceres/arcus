#include "HTTP/CurlPool.hpp"

#include <iostream>

using namespace HTTP;

CurlPool::~CurlPool()
{
    while (!_available_instances.empty())
    {
        curl_easy_cleanup(_available_instances.top());
        _available_instances.pop();
    }
}

CURL* CurlPool::pop()
{
    if (_available_instances.empty())
    {
        return curl_easy_init();
    }
    else
    {
        CURL* inst = _available_instances.top();
        _available_instances.pop();
        return inst;
    }
}

void CurlPool::push(CURL* inst)
{
    if (_available_instances.size() < MAX_INSTANCES)
    {
        _available_instances.push(inst);
    }
    else
    {
        curl_easy_cleanup(inst);
    }
}
