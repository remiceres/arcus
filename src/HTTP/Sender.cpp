#include "HTTP/Sender.hpp"
#include "HTTP/Request.hpp"
#include "HTTP/Response.hpp"
#include "HTTP/Verb.hpp"
#include "HTTP/Error.hpp"
#include "Log/Printer.hpp"

#include <curl/curl.h>
#include <cctype>
#include <istream>
#include <iostream>

using namespace HTTP;

Sender::Sender()
: _send_running(false), _multi(curl_multi_init()),
  _sending_comparator{*this}, _pending_sendings(_sending_comparator)
{}

Sender::~Sender()
{
    // Si le thread d’envoi a été démarré, on veut désormais le stopper
    if (_send_worker.joinable())
    {
        // Demande d'arrêt du thread
        {
            std::unique_lock<std::mutex> wakeup_lock(_wakeup_mutex);
            _send_running = false;
            _send_wakeup.notify_one();
        }

        // Jonction des deux fils d’exécution
        _send_worker.join();
    }

    // Libération de la file d’attente cURL
    curl_multi_cleanup(_multi);
}

boost::future<Response> Sender::send(Request& req)
{
    _start();
    boost::future<Response> result;

    {
        // Mise en attente d'une instance cURL pour la nouvelle requête
        std::lock_guard<std::recursive_mutex> sendings_lock(_sendings_mutex);
        CURL* simple = _prepare(req);
        result = _sendings.find(simple)->second.headers_promise.get_future();
    }

    {
        // Réveil du thread d’envoi pour la prise en charge de la requête
        std::unique_lock<std::mutex> wakeup_lock(_wakeup_mutex);
        _send_wakeup.notify_one();
    }

    return result;
}

bool Sender::getVerbose() const
{
    return _verbose;
}

void Sender::setVerbose(bool verbose)
{
    _verbose = verbose;
}

unsigned int Sender::getConcurrency() const
{
    return _concurrency;
}

void Sender::setConcurrency(unsigned int concurrency)
{
    _concurrency = concurrency;
}

void Sender::_start()
{
    if (!_send_running)
    {
        // Si le thread d'envoi a rencontré une erreur, on l'arrête
        if (_send_worker.joinable())
        {
            _send_worker.join();
        }

        LOG_INFO << "Démarrage du thread d'envoi des requêtes.\n";

        // Lancement du thread d'envoi
        _send_running = true;
        _send_worker = std::thread(&Sender::_sendWorker, this);
    }
}

bool Sender::_SendingComparator::operator()(const CURL* lhs, const CURL* rhs)
{
    return
        parent_sender._sendings.find(lhs)->second.req.getTime() >
        parent_sender._sendings.find(rhs)->second.req.getTime();
}

std::size_t Sender::_sendRequestData(
    char* buffer,
    std::size_t size,
    std::size_t nmemb,
    void* userdata
)
{
    std::istream& in = *static_cast<std::istream*>(userdata);
    std::streamsize count = in.readsome(buffer, size * nmemb);

    if (in.fail())
    {
        // Une erreur de lecture s’est produite dans le flux
        return CURL_READFUNC_ABORT;
    }

    if (in.eof())
    {
        // Fin de fichier atteinte, fin de l’envoi de la requête
        return 0;
    }

    // Signale à cURL le nombre d’octets écrits dans le tampon
    return count;
}

std::size_t Sender::_receiveResponseHeader(
    char* buffer,
    std::size_t size,
    std::size_t nmemb,
    void* userdata
)
{
    _Sending& sending = *static_cast<_Sending*>(userdata);
    HeaderSet& headers = sending.res._headers;

    std::size_t total = nmemb * size;
    std::string data(buffer, total);

    // Si la ligne est vide, tous les en-têtes ont été reçus et on peut
    // les transmettre au client
    if (data == "\n" || data == "\r\n" || data == "")
    {
        // Récupération du code de réponse HTTP actuel
        long response_code = 0;
        curl_easy_getinfo(
            sending.simple,
            CURLINFO_RESPONSE_CODE,
            &response_code
        );

        // Si le serveur a répondu « 100 Continue », on ne satisfait pas
        // immédiatement la promesse car une vraie réponse est à venir
        if (response_code >= 200
            && (!sending.req.getFollowRedirects()
                || response_code < 300
                || response_code >= 400))
        {
            sending.res._code = response_code;

            LOG_INFO << verbToString(sending.req.getVerb()) << " "
            << sending.req.getUrl() << " - En-têtes reçus avec le code "
            << sending.res.getCode() << "\n";

            // Satisfaction de la promesse des en-têtes
            sending.headers_promise.set_value(std::move(sending.res));
        }
    }

    // Si la ligne contient un deux-points, alors c'est un en-tête et on
    // l'ajoute dans la réponse.
    std::string::const_iterator colon
        = std::find(std::cbegin(data), std::cend(data), ':');

    if (colon != std::end(data))
    {
        std::string::const_iterator start_value = std::find_if_not(
            colon + 1, std::cend(data),
            std::ptr_fun<int, int>(std::isspace)
        );

        std::string::const_iterator end_value = std::find_if(
            start_value, std::cend(data),
            std::ptr_fun<int, int>(std::isspace)
        );

        headers.addHeader(
            std::string(std::cbegin(data), colon),
            std::string(start_value, end_value)
        );
    }

    return total;
}

std::size_t Sender::_receiveResponseData(
    char* buffer,
    std::size_t size,
    std::size_t nmemb,
    void* userdata
)
{
    _Sending& sending = *static_cast<_Sending*>(userdata);
    Sender& sender = *sending.parent;

    // Si l'envoi n'a pas été relancé, on le met en pause et on laisse la
    // place pour un autre envoi
    if (!sending.resumed)
    {
        sender._current_sendings.erase(sending.simple);
        return CURL_WRITEFUNC_PAUSE;
    }

    // Si le flux est nul, on ignore les données
    if (sending.out_stream == nullptr)
    {
        return size * nmemb;
    }

    // Sinon, on écrit dans le flux
    if (sending.out_stream->write(buffer, size * nmemb))
    {
        return size * nmemb;
    }

    return 0;
}

CURL* Sender::_prepare(Request& base_req)
{
    // Récupération d'une instance de cURL depuis la piscine
    CURL* simple = _pool.pop();

    // Création d'un objet d'informations sur l'envoi. Les informations sur
    // la requête sont copiées afin de rester accessibles tout au long de
    // l'envoi.
    _sendings.emplace(simple, _Sending{
        this,                       // < parent
        simple,                     // < simple
        nullptr,                    // < headers
        false,                      // < resumed
        nullptr,                    // < out_stream
        base_req,                   // < req
        Response(this, simple),     // < res
        boost::promise<Response>(), // < headers_promise
        boost::promise<void>()      // < finish_promise
    });

    _Sending& sending = _sendings.find(simple)->second;
    struct curl_slist*& headers = sending.headers;
    Request& req = sending.req;

    // Si le mode verbeux est activé, signalement auprès de cURL
    curl_easy_setopt(simple, CURLOPT_VERBOSE, _verbose);

    // Configure le suivi des redirections
    curl_easy_setopt(simple, CURLOPT_FOLLOWLOCATION, req.getFollowRedirects());

    // Identification du programme auprès du serveur
    curl_easy_setopt(simple, CURLOPT_USERAGENT, ARCUS_USER_AGENT);

    // Configuration de l’URL dans cURL
    curl_easy_setopt(simple, CURLOPT_URL, req.getUrl().c_str());

    // Configuration spécifique à chaque verbe HTTP
    switch (req.getVerb())
    {
    case Verb::GET:
        curl_easy_setopt(simple, CURLOPT_CUSTOMREQUEST, "GET");
        curl_easy_setopt(simple, CURLOPT_HTTPGET, 1L);
        curl_easy_setopt(simple, CURLOPT_UPLOAD, 0L);
        break;

    case Verb::POST:
        curl_easy_setopt(simple, CURLOPT_CUSTOMREQUEST, "POST");
        curl_easy_setopt(simple, CURLOPT_HTTPPOST, nullptr);
        curl_easy_setopt(simple, CURLOPT_UPLOAD, 0L);

        // Adjonction des données du POST si nécessaire.
        if (!req.getData().empty())
        {
            curl_easy_setopt(simple, CURLOPT_POSTFIELDS, req.getData().c_str());
        }
        break;

    case Verb::PUT:
        curl_easy_setopt(simple, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_easy_setopt(simple, CURLOPT_UPLOAD, 1L);

        if (req.getInStream() == nullptr)
        {
            throw std::runtime_error(
                "Impossible d'effectuer une requête PUT sans données. "
                "Utilisez req.setInStream(stream) pour spécifier le flux de "
                "données à envoyer au serveur."
            );
        }
        break;

    case Verb::HEAD:
        curl_easy_setopt(simple, CURLOPT_CUSTOMREQUEST, "HEAD");
        curl_easy_setopt(simple, CURLOPT_NOBODY, 1L);
        break;

    case Verb::DELETE:
        curl_easy_setopt(simple, CURLOPT_CUSTOMREQUEST, "DELETE");
        break;
    }

    // Envoi du fichier si demandé, pour les requêtes POST et PUT
    if (
        req.getInStream() != nullptr &&
        (req.getVerb() == Verb::POST || req.getVerb() == Verb::PUT)
    )
    {
        curl_easy_setopt(simple, CURLOPT_UPLOAD, 1L);
        curl_easy_setopt(simple, CURLOPT_READFUNCTION, _sendRequestData);
        curl_easy_setopt(simple, CURLOPT_READDATA, req.getInStream());

        headers = curl_slist_append(
            headers,
            "Content-Type: application/octet-stream"
        );
    }

    // Configuration de la liste d’en-têtes dans cURL
    for (const Header &header : req.getHeaderSet().getHeaders())
    {
        headers = curl_slist_append(
            headers,
            static_cast<std::string>(header).c_str()
        );
    }

    curl_easy_setopt(simple, CURLOPT_HTTPHEADER, headers);

    // Demande l'ajout des en-têtes renvoyés par le serveur dans
    // l'objet de réponse
    curl_easy_setopt(simple, CURLOPT_HEADERFUNCTION, _receiveResponseHeader);
    curl_easy_setopt(simple, CURLOPT_HEADERDATA, &sending);

    // Demande l'écriture des données renvoyées par le serveur
    // dans le flux passé en argument
    curl_easy_setopt(simple, CURLOPT_WRITEFUNCTION, _receiveResponseData);
    curl_easy_setopt(simple, CURLOPT_WRITEDATA, &sending);

    // Ajout de la nouvelle instance dans la file d'attente
    _pending_sendings.push(simple);
    return simple;
}

void Sender::_terminate(CURL* simple, CURLcode code)
{
    _Sending& sending = _sendings.find(simple)->second;
    Request& req = sending.req;

    // Si l’envoi s’est terminé correctement, alors on satisfait la promesse
    // et on journalise le résultat.
    if (code == CURLE_OK)
    {
        // Journalise la fin de la réception de la réponse
        LOG_INFO << verbToString(req.getVerb()) << " "
            << req.getUrl() << " - Contenu de la réponse reçu.\n";

        // Notification de la fin de l'envoi
        sending.finish_promise.set_value();
    }

    // Sinon, on convertit le code d’erreur cURL en exception que l’on place
    // dans la promesse et on journalise l’erreur.
    else
    {
        boost::exception_ptr http_error;
        const char* message = curl_easy_strerror(code);

        // Journalise l’erreur de l’envoi
        LOG_ERROR << verbToString(req.getVerb()) << " "
            << req.getUrl() << " - Envoi échoué (" << message << ", code "
            << code << ").\n";

        switch (code)
        {
        case CURLE_UNSUPPORTED_PROTOCOL:
        case CURLE_URL_MALFORMAT:
            http_error = boost::copy_exception(
                Errors::Request(code, std::string(message))
            );
            break;

        case CURLE_GOT_NOTHING:
        case CURLE_BAD_CONTENT_ENCODING:
            http_error = boost::copy_exception(
                Errors::Response(code, std::string(message))
            );
            break;

        case CURLE_SSL_CONNECT_ERROR:
        case CURLE_SSL_CERTPROBLEM:
        case CURLE_SSL_CIPHER:
        case CURLE_SSL_ENGINE_INITFAILED:
        case CURLE_SSL_ENGINE_NOTFOUND:
        case CURLE_SSL_ENGINE_SETFAILED:
        case CURLE_SSL_CACERT:
        case CURLE_SSL_CACERT_BADFILE:
        case CURLE_SSL_SHUTDOWN_FAILED:
        case CURLE_SSL_CRL_BADFILE:
        case CURLE_SSL_ISSUER_ERROR:
        case CURLE_SSL_PINNEDPUBKEYNOTMATCH:
        case CURLE_SSL_INVALIDCERTSTATUS:
            http_error = boost::copy_exception(
                Errors::SSL(code, std::string(message))
            );
            break;

        case CURLE_COULDNT_RESOLVE_PROXY:
        case CURLE_COULDNT_RESOLVE_HOST:
        case CURLE_COULDNT_CONNECT:
        case CURLE_OPERATION_TIMEDOUT:
            http_error = boost::copy_exception(
                Errors::Network(code, std::string(message))
            );
            break;

        case CURLE_ABORTED_BY_CALLBACK:
        case CURLE_WRITE_ERROR:
        case CURLE_READ_ERROR:
            http_error = boost::copy_exception(
                Errors::IO(code, std::string(message))
            );
            break;

        default:
            http_error = boost::copy_exception(
                Error(code, std::string(message))
            );
            break;
        }

        try
        {
            sending.headers_promise.set_exception(http_error);
        }
        catch(boost::promise_already_satisfied& err)
        {
            // Dans le cas où la promesse des en-têtes a déjà été
            // satisfaite, on ne modifie pas sa valeur et on se
            // contente de satisfaire la promesse de fin.
        }

        sending.finish_promise.set_exception(http_error);
    }

    _clean(simple);
}

void Sender::_clean(CURL* simple)
{
    auto search = _sendings.find(simple);

    if (search != std::cend(_sendings))
    {
        // Retrait de l'instance cURL du multi
        curl_multi_remove_handle(_multi, simple);

        // Libération de la liste des en-têtes
        curl_slist_free_all(search->second.headers);

        // Suppression des informations pour cet envoi
        _sendings.erase(search);

        // Retrait des envois en cours de traitement
        _current_sendings.erase(simple);
    }
}

void Sender::_sendWorker()
{
    CURLMcode code;
    int unused = 0;
    int file_descriptors = 0;

    while (true)
    {
        std::unique_lock<std::recursive_mutex> sendings_lock{_sendings_mutex};

        // Demande de réalisation des envois associés aux requêtes cURL
        // en attente (non-bloquant)
        code = curl_multi_perform(_multi, &unused);

        if (code != CURLM_OK)
        {
            _send_running = false;
            LOG_ERROR << "Arrêt du thread d'envoi de Sender : "
                << "curl_multi_perform() a échoué ("
                << curl_multi_strerror(code) << ")\n";
            return;
        }

        // Satisfaction des promesses des envois terminés
        CURLMsg* message = curl_multi_info_read(_multi, &unused);

        while (message != nullptr)
        {
            if (message->msg == CURLMSG_DONE)
            {
                // Envoi terminé normalement, signal et nettoyage
                _terminate(message->easy_handle, message->data.result);
            }

            message = curl_multi_info_read(_multi, &unused);
        }

        // S'il reste de la place pour faire des envois supplémentaires,
        // on relance en priorité les envois mis en pause, et s'il n'y en
        // a plus en lance de nouveaux envois
        while (
            _current_sendings.size() < _concurrency &&
            (!_resumable_sendings.empty() || !_pending_sendings.empty())
        )
        {
            if (!_resumable_sendings.empty())
            {
                // Remise en route d'un envoi en pause
                CURL* simple = _resumable_sendings.front();
                CURLcode result = curl_easy_pause(simple, CURLPAUSE_CONT);

                if (result != CURLE_OK)
                {
                    // Erreur prématurée lors de la remise en route,
                    // interruption immédiate
                    _terminate(simple, result);
                }
                else
                {
                    _current_sendings.insert(simple);
                }

                _resumable_sendings.pop();
            }
            else
            {
                CURL* simple = _pending_sendings.top();
                Request& req = _sendings.find(simple)->second.req;

                // Si l'envoi le plus prioritaire est dans le futur,
                // il faut attendre
                if (req.getTime() > Request::TimeClock::now())
                {
                    break;
                }

                _pending_sendings.pop();

                LOG_INFO << verbToString(req.getVerb()) << " " << req.getUrl()
                    << " - Envoi de la requête (prévue pour "
                    << Log::Printer::timeToString(req.getTime()) << ")\n";

                _current_sendings.insert(simple);
                curl_multi_add_handle(_multi, simple);
            }
        }

        if (_current_sendings.size() == 0)
        {
            std::unique_lock<std::mutex> wakeup_lock(_wakeup_mutex);

            if (!_send_running)
            {
                // Arrêt du thread
                return;
            }
            else if (_pending_sendings.empty())
            {
                // File d'attente des envois vide, il faut donc attendre
                // jusqu'au prochain ajout d'une requête
                sendings_lock.unlock();
                _send_wakeup.wait(wakeup_lock);
            }
            else
            {
                // File d'attente des envois dans le futur, il faut donc
                // attendre soit jusqu'au point temporel de la plus proche
                // requête ou jusqu'à l'ajout d'une autre
                _Sending& sending
                    = _sendings.find(_pending_sendings.top())->second;
                std::chrono::time_point<Request::TimeClock> point
                    = sending.req.getTime();

                sendings_lock.unlock();
                _send_wakeup.wait_until(wakeup_lock, point);
            }
        }
        else
        {
            // Reste des envois en cours, attente du temps préconisé par
            // la bibliothèque cURL, ou au plus une seconde
            curl_multi_wait(_multi, NULL, 0, 1000, &file_descriptors);
            sendings_lock.unlock();

            if (file_descriptors == 0)
            {
                // Si on ne peut pas savoir quand se termineront les travaux
                // en cours, on attend 200 ms
                std::this_thread::sleep_for(std::chrono::milliseconds(200));
            }
        }
    }
}
