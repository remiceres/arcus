#include "HTTP/Header.hpp"

#include <limits>

using namespace HTTP;

Header::Header(Header::Name name, Header::Value value)
: _name(name), _value(value)
{}

Header::operator std::string() const
{
    return _name + ": " + _value;
}

const Header::Name& Header::getName() const
{
    return _name;
}

const Header::Value& Header::getValue() const
{
    return _value;
}

std::vector<Header> HeaderSet::getHeaders(Header::Name name) const
{
    std::multimap<Header::Name, Header::Value>::const_iterator begin, end;
    std::vector<Header> result;

    if (name.empty())
    {
        begin = std::begin(_set);
        end = std::end(_set);
    }
    else
    {
        begin = _set.lower_bound(name);
        end = _set.upper_bound(name);
    }

    for (auto it = begin; it != end; ++it)
    {
        result.emplace_back(it->first, it->second);
    }

    return result;
}

int64_t HeaderSet::getHeaderAsInt(Header::Name name) const
{
    std::vector<Header> list = getHeaders(name);

    int64_t max = std::numeric_limits<int64_t>::max();
    int64_t result = max;

    auto it = std::cbegin(list);

    while (result == max && it != std::cend(list))
    {
        try
        {
            result = std::stoi(it->getValue());
        }
        catch (std::invalid_argument& err)
        {
            // Ignore les valeurs qui ne sont pas formattées correctement.
        }
        catch (std::out_of_range& err)
        {
            // Ignore les valeurs qui sont trop grandes ou trop petites.
        }

        ++it;
    }

    return result;
}

std::string HeaderSet::getMIMEType() const
{
    std::vector<Header> list = getHeaders("Content-Type");

    if (list.empty())
    {
        return "application/octet-stream";
    }
    else
    {
        std::string type = list[0].getValue();
        auto type_start = type.find_first_not_of(" \t");
        auto type_end = type.find_first_of(" \t;", type_start);
        return type.substr(type_start, type_end - type_start);
    }
}

HeaderSet& HeaderSet::addHeader(Header::Name name, Header::Value value)
{
    _set.emplace(name, value);
    return *this;
}

HeaderSet& HeaderSet::removeHeaders(Header::Name name)
{
    if (name.empty())
    {
        _set.clear();
    }
    else
    {
        _set.erase(_set.lower_bound(name), _set.upper_bound(name));
    }

    return *this;
}
