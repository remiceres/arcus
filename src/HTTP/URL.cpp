#include "HTTP/URL.hpp"

#include <sstream>
#include <iomanip>

using namespace HTTP;

std::string URL::encode(const std::string& input)
{
    // Implémentation tirée de
    // https://stackoverflow.com/a/17708801/3452708
    std::ostringstream output;
    output.fill('0');
    output << std::hex;

    for (const unsigned char& c : input)
    {
        if (isalnum(c) || c == '-' || c == '_' || c == '.' || c == '~')
        {
            // Conserve les caractères alphanumériques intacts
            output << c;
        }
        else
        {
            // Encodage des autres caractères
            output << std::uppercase;
            output << '%' << std::setw(2) << static_cast<int>(c);
            output << std::nouppercase;
        }
    }

    return output.str();
}

std::string URL::decode(const std::string& input)
{
    std::string output;
    output.reserve(input.size());

    for (auto it = std::cbegin(input); it < std::cend(input); ++it)
    {
        const unsigned char& c = *it;

        if (c == '%')
        {
            std::string str_value(it + 1, it + 3);
            unsigned int value = std::strtoul(str_value.c_str(), nullptr, 16);

            output.push_back(value);
            it += 2;
        }
        else
        {
            output.push_back(c);
        }
    }

    return output;
}
