#include "HTTP/Verb.hpp"

using namespace HTTP;

std::string HTTP::verbToString(Verb verb)
{
    switch (verb)
    {
    case Verb::GET:
        return "GET";

    case Verb::HEAD:
        return "HEAD";

    case Verb::POST:
        return "POST";

    case Verb::PUT:
        return "PUT";

    case Verb::DELETE:
        return "DELETE";
    }
}
