#include "HTTP/Error.hpp"

using namespace HTTP;

Error::Error(int code, std::string description)
: std::runtime_error("Erreur lors de l'envoi (" + description  + ", code "
    + std::to_string(code) + ")"), _code(code), _description(description)
{}

int Error::getCode() const
{
    return _code;
}

std::string Error::getDescription() const
{
    return _description;
}
