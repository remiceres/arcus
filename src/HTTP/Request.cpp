#include "HTTP/Request.hpp"

using namespace HTTP;

Request::Request()
: _time(Request::TimeClock::now())
{}

const std::string& Request::getUrl() const
{
    return _url;
}

Request& Request::setUrl(std::string url)
{
    _url = url;
    return *this;
}

const HeaderSet& Request::getHeaderSet() const
{
    return _headers;
}

HeaderSet& Request::getHeaderSet()
{
    return _headers;
}

const Verb& Request::getVerb() const
{
    return _verb;
}

Request& Request::setVerb(Verb verb)
{
    _verb = verb;
    return *this;
}

std::istream* Request::getInStream() const
{
    return _in_stream;
}

Request& Request::setInStream(std::istream& in_stream)
{
    _in_stream = &in_stream;
    return *this;
}

const std::string& Request::getData() const
{
    return _data;
}

Request& Request::setData(std::string data)
{
    _data = data;
    return *this;
}

const std::chrono::time_point<Request::TimeClock>& Request::getTime() const
{
    return _time;
}

Request& Request::setTime(std::chrono::time_point<Request::TimeClock> time)
{
    _time = time;
    return *this;
}

Request& Request::setFollowRedirects(bool follow_redirects)
{
    _follow_redirects = follow_redirects;
    return *this;
}

bool Request::getFollowRedirects()
{
    return _follow_redirects;
}
