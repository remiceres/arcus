#include "HTTP/Response.hpp"
#include "HTTP/Sender.hpp"

using namespace HTTP;

Response::Response(Sender* sender, CURL* simple)
: _sender(sender), _simple(simple)
{}

Response::Response(Response&& src)
: _sender(src._sender), _simple(src._simple),
  _code(src._code), _headers(std::move(src._headers))
{
    src._sender = nullptr;
    src._simple = nullptr;
}

Response& Response::operator=(Response&& src)
{
    _sender = src._sender;
    src._sender = nullptr;

    _simple = src._simple;
    src._simple = nullptr;

    _code = src._code;
    _headers = std::move(src._headers);

    return *this;
}

Response::~Response()
{
    if (_sender != nullptr && _simple != nullptr)
    {
        std::lock_guard<std::recursive_mutex>
            sendings_lock(_sender->_sendings_mutex);

        _sender->_clean(_simple);
        _sender->_pool.push(_simple);
    }
}

long Response::getCode() const
{
    return _code;
}

const HeaderSet& Response::getHeaderSet() const
{
    return _headers;
}

Response& Response::setOutStream(std::ostream& out_stream)
{
    std::lock_guard<std::recursive_mutex>
        sendings_lock(_sender->_sendings_mutex);

    _sender->_sendings.find(_simple)->second.out_stream = &out_stream;
    return *this;
}

boost::future<void> Response::finish() const
{
    boost::future<void> result;

    std::unique_lock<std::recursive_mutex>
        sendings_lock(_sender->_sendings_mutex);

    Sender::_Sending& sending = _sender->_sendings.find(_simple)->second;
    sending.resumed = true;
    result = sending.finish_promise.get_future();
    _sender->_resumable_sendings.push(_simple);

    std::unique_lock<std::mutex> wakeup_lock(_sender->_wakeup_mutex);
    sendings_lock.unlock();
    _sender->_send_wakeup.notify_one();

    return result;
}
