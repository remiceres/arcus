#include "lib/json.hpp"
#include "Services/Error.hpp"
#include "Services/DropboxService.hpp"
#include "Services/Quota.hpp"
#include "Services/Entry.hpp"
#include "HTTP/Request.hpp"
#include "HTTP/Response.hpp"

#include <chrono>
#include <regex>
#include <functional>

using namespace Services;
using json = nlohmann::json;

const std::string DropboxService::APP_ID = "xzzw6yrtfx5irnx";
const std::string DropboxService::REDIRECT_URL = "https://example.net";
const std::string DropboxService::API_BASE_URL =
    "https://api.dropboxapi.com/2/";
const std::string DropboxService::CONTENT_BASE_URL =
    "https://content.dropboxapi.com/2/";

DropboxService::DropboxService(HTTP::Sender& sender, std::string token)
: Service(sender), _token(token)
{}

boost::future<void> DropboxService::login()
{
    std::string url =
        std::string("https://www.dropbox.com/oauth2/authorize?") +
        "client_id=" + DropboxService::APP_ID + "&" +
        "redirect_uri=" + DropboxService::REDIRECT_URL + "&" +
        "response_type=token";

    std::string final_url;

    std::cout << "Connectez-vous sur l’URL suivante :" << std::endl;
    std::cout << url << std::endl << std::endl;

    std::cout << "Une fois connecté, collez l’URL sur laquelle vous êtes ";
    std::cout << "redirigé :" << std::endl;

    std::cin >> final_url;

    std::size_t left_token_pos = final_url.find_first_of('=') + 1;
    _token = final_url.substr(
        left_token_pos,
        final_url.find_first_of('&') - left_token_pos
    );

    return boost::make_ready_future();
}

namespace
{
    /**
     * Expression régulière détectant les erreurs de chemin malformé.
     */
    const std::regex invalid_path_regex("path: .* did not match pattern");

    /**
     * Gère les erreurs d’API en levant une exception si nécessaire.
     *
     * @param error_type Type d’erreur à gérer.
     * @param error Données associées à l’erreur à gérer.
     */
    void handleAPIError(
        const std::string& error_type,
        const json& error
    )
    {
        if (error_type == "UploadError")
        {
            return handleAPIError("WriteError", error["path"]["reason"]);
        }

        if (error_type == "DownloadError")
        {
            return handleAPIError("LookupError", error["path"]);
        }

        if (error_type == "DeleteError")
        {
            std::string tag = error[".tag"];

            if (tag == "path_lookup")
            {
                return handleAPIError("LookupError", error["path_lookup"]);
            }

            if (tag == "path_write")
            {
                return handleAPIError("WriteError", error["path_write"]);
            }
        }

        if (error_type == "DownloadError")
        {
            return handleAPIError("LookupError", error["path"]);
        }

        if (error_type == "WriteError")
        {
            std::string tag = error[".tag"];

            if (tag == "no_write_permission")
            {
                throw boost::enable_current_exception(Errors::PermissionDenied(
                    "Vous n’avez pas le droit d’écrire dans ce répertoire."
                ));
            }

            if (tag == "insufficient_space")
            {
                throw boost::enable_current_exception(Errors::QuotaExceeded(
                    "Le quota d’espace de stockage a été dépassé."
                ));
            }

            if (tag == "disallowed_name")
            {
                throw boost::enable_current_exception(Error(
                    "Le nom de cet élément n’est pas autorisé."
                ));
            }

            if (tag == "team_folder")
            {
                throw boost::enable_current_exception(Error(
                    "Impossible de modifier un dossier d’équipe."
                ));
            }
        }

        if (error_type == "LookupError")
        {
            std::string tag = error[".tag"];

            if (tag == "malformed_path")
            {
                throw boost::enable_current_exception(
                    Errors::ResourceUnavailable(
                        "Le chemin est invalide."
                    )
                );
            }

            if (tag == "not_found")
            {
                throw boost::enable_current_exception(
                    Errors::ResourceUnavailable(
                        "La ressource n’existe pas sur le serveur."
                    )
                );
            }

            if (tag == "not_file")
            {
                throw boost::enable_current_exception(
                    Errors::ResourceUnavailable(
                        "La ressource n’est pas un fichier."
                    )
                );
            }

            if (tag == "not_folder")
            {
                throw boost::enable_current_exception(
                    Errors::ResourceUnavailable(
                        "La ressource n’est pas un répertoire."
                    )
                );
            }

            if (tag == "restricted_content")
            {
                throw boost::enable_current_exception(
                    Errors::ResourceUnavailable(
                        "Le contenu de la ressource n’est pas accessible "
                        "pour des raisons légales."
                    )
                );
            }
        }
    }

    /**
     * Transforme la réponse du serveur en gérant les éventuelles erreurs
     * signalées. Si la réponse indique un succès, elle est renvoyée
     * immédiatement. Sinon, une exception normalisant l’erreur qui s’est
     * produite est générée et placée dans une valeur future.
     *
     * @param error_type Type d’erreur d’API renvoyé. Par exemple, DeleteError.
     * Si vide, renvoie une erreur générique en cas d’erreur d’API.
     * @param on_res Valeur future contenant la réponse à transformer.
     * @return Fonction à chaîner à une valeur future contenant une réponse
     * dans laquelle est placée soit une réponse en cas de succès soit une
     * exception avec l’erreur normalisée dans le cas inverse.
     */
    std::function<
        boost::future<HTTP::Response>(boost::future<HTTP::Response>)
    > handleDropboxError(const std::string& error_type = "")
    {
        return [error_type](boost::future<HTTP::Response> on_res)
        {
            HTTP::Response res = on_res.get();
            long code = res.getCode();

            if (code >= 200 && code < 300)
            {
                return boost::make_ready_future(std::move(res));
            }

            if (code == 401)
            {
                throw boost::enable_current_exception(
                    Errors::AuthenticationFailure(
                        "Le jeton d’authentification n’est pas ou plus valide "
                        "et ne permet pas à Arcus de transmettre ses requêtes "
                        "au serveur Dropbox. Cela peut résulter d’une "
                        "révocation du jeton ou de son expiration."
                    )
                );
            }

            if (code == 429)
            {
                throw boost::enable_current_exception(Error(
                    "L’envoi de requêtes vers le serveur Dropbox a été "
                    "temporairement interdit pour l’application Arcus. Cela "
                    "peut résulter d’un nombre trop important de demandes "
                    "ou d’un accès concurrent. L’envoi a été réessayé "
                    "plusieurs fois sans succès."
                ));
            }

            if (code >= 500 && code < 600)
            {
                throw boost::enable_current_exception(Errors::ServerUnavailable(
                    "Le serveur Dropbox rencontre des problèmes techniques et "
                    "Arcus ne peut pas communiquer avec lui. Le problème vient "
                    "certainement des serveurs Dropbox. L’envoi a été réessayé "
                    "plusieurs fois sans succès. Si le problème persiste, "
                    "consultez la page de statut du service pour plus "
                    "d'informations (https://status.dropbox.com/)."
                ));
            }

            if (code != 400 && code != 409)
            {
                throw boost::enable_current_exception(Error(
                    "Le serveur Dropbox a répondu de façon inattendue à la "
                    "requête d’Arcus. Le code de réponse renvoyé est "
                    + std::to_string(code) + "."
                ));
            }

            auto out = std::make_shared<std::stringstream>();
            auto on_finish = res.setOutStream(*out).finish();

            return on_finish.then(
                [out, code, res{std::move(res)}, error_type]
                (boost::future<void> future)
                -> HTTP::Response
                {
                    future.get();

                    // Cas des requêtes malformées. Dans ce cas, Dropbox répond
                    // avec un message en texte plein que l’on est obligé
                    // d’interpréter à la main
                    if (code == 400)
                    {
                        std::string message = out->str();

                        if (
                            message.find("OAuth 2 access token is malformed")
                            != std::string::npos
                        )
                        {
                            throw boost::enable_current_exception(
                                Errors::AuthenticationFailure(
                                    "Le jeton d’authentification est malformé "
                                    "et ne permet pas à Arcus de transmettre "
                                    "ses requêtes au serveur Dropbox. Vérifiez "
                                    "le jeton."
                                )
                            );
                        }

                        if (std::regex_search(message, invalid_path_regex))
                        {
                            throw boost::enable_current_exception(
                                Errors::ResourceUnavailable(
                                    "Le chemin de la ressource est malformé."
                                )
                            );
                        }

                        throw boost::enable_current_exception(Error(
                            "Le serveur Dropbox a refusé de répondre à une "
                            "requête qu’il a jugée malformée (" + out->str()
                            + "). Ceci est probablement un bogue de Arcus."
                        ));
                    }

                    // Cas des erreurs d’API, pour lesquelles Dropbox renvoie
                    // des informations sous forme de JSON
                    json data;
                    data << *out;

                    handleAPIError(error_type, data["error"]);

                    // Erreur d’API inattendue, dans ce cas on synthétise les
                    // informations fournies par Dropbox dans une erreur
                    // générique
                    std::string message = std::string("Une erreur inattendue ")
                        + "de type « "
                        + data["error_summary"].get<std::string>()
                        + " » a été renvoyée par le serveur Dropbox";

                    if (data.count("user_message") > 0)
                    {
                        message += " ("
                            + data["user_message"].get<std::string>() + ").";
                    }
                    else
                    {
                        message += ".";
                    }

                    throw boost::enable_current_exception(Error(message));
                }
            );
        };
    }
}

boost::future<void> DropboxService::putFile(
    boost::filesystem::path source,
    boost::filesystem::path target,
    unsigned int retries
)
{
    json payload = {
        {"path", target.string()},
        {"mode", "overwrite"},
        {"mute", true},
    };

    auto in = std::make_shared<std::ifstream>(
        source.string(),
        std::ios::in | std::ios::binary
    );

    HTTP::Request req = HTTP::Request()
        .setVerb(HTTP::Verb::POST)
        .setUrl(CONTENT_BASE_URL + "files/upload")
        .setInStream(*in);

    req.getHeaderSet()
        .addHeader("Authorization", "Bearer " + _token)
        .addHeader("Dropbox-API-Arg", payload.dump());

    return retry(req, retries)
        .then(handleDropboxError("UploadError")).unwrap()
        .then([in](boost::future<HTTP::Response> on_res)
        {
            on_res.get();
        });
}

boost::future<void> DropboxService::fetchFile(
    boost::filesystem::path source,
    boost::filesystem::path target,
    unsigned int retries
)
{
    json payload = {{"path", source.string()}};

    HTTP::Request req = HTTP::Request()
        .setVerb(HTTP::Verb::POST)
        .setUrl(CONTENT_BASE_URL + "files/download");

    req.getHeaderSet()
        .addHeader("Authorization", "Bearer " + _token)
        .addHeader("Dropbox-API-Arg", payload.dump());

    return retry(req, retries)
        .then(handleDropboxError("DownloadError")).unwrap()
        .then([target](boost::future<HTTP::Response> on_res)
        {
            HTTP::Response res = on_res.get();
            auto out = std::make_shared<std::ofstream>(
                target.string(),
                std::ios::out | std::ios::binary
            );
            auto on_finish = res.setOutStream(*out).finish();

            return on_finish.then(
                [out, res{std::move(res)}] (boost::future<void> future)
                {
                    future.get();
                }
            );
        }).unwrap();
}

boost::future<void> DropboxService::removeFile(
    boost::filesystem::path path,
    unsigned int retries
)
{
    json payload = {{"path", path.string()}};

    HTTP::Request req = HTTP::Request()
        .setVerb(HTTP::Verb::POST)
        .setUrl(API_BASE_URL + "files/delete")
        .setData(payload.dump());

    req.getHeaderSet()
        .addHeader("Authorization", "Bearer " + _token)
        .addHeader("Content-Type", "application/json");

    return retry(req, retries)
        .then(handleDropboxError("DeleteError")).unwrap()
        .then([](boost::future<HTTP::Response> on_res)
        {
            on_res.get();
        });
}

boost::future<std::vector<Entry>> DropboxService::listDir(
    boost::filesystem::path path,
    unsigned int retries
)
{
    json payload = {{"path", path.string()}};

    HTTP::Request req = HTTP::Request()
        .setVerb(HTTP::Verb::POST)
        .setUrl(API_BASE_URL + "files/list_folder")
        .setData(payload.dump());

    req.getHeaderSet()
        .addHeader("Authorization", "Bearer " + _token)
        .addHeader("Content-Type", "application/json");

    return retry(req, retries)
        .then(handleDropboxError("ListFolderError")).unwrap()
        .then([](boost::future<HTTP::Response> on_res)
        {
            HTTP::Response res = on_res.get();
            auto out = std::make_shared<std::stringstream>();
            auto on_finish = res.setOutStream(*out).finish();

            return on_finish.then(
                [out, res{std::move(res)}] (boost::future<void> future)
                {
                    future.get();

                    json data;
                    data << *out;

                    std::vector<Entry> entries;
                    entries.reserve(data["entries"].size());

                    std::transform(
                        std::begin(data["entries"]),
                        std::end(data["entries"]),
                        std::back_inserter(entries),
                        [](const json &entry)
                        {
                            if (entry[".tag"] == "file")
                            {
                                return Entry(
                                    Entry::Type::FILE,
                                    entry["path_lower"].get<std::string>(),
                                    entry["content_hash"],
                                    entry["size"]
                                );
                            }
                            else
                            {
                                return Entry(
                                    Entry::Type::FOLDER,
                                    entry["path_lower"].get<std::string>()
                                );
                            }
                        }
                    );

                    return entries;
                }
            );
        }).unwrap();
}

boost::future<Quota> DropboxService::getQuota(unsigned int retries)
{
    HTTP::Request req = HTTP::Request()
        .setVerb(HTTP::Verb::POST)
        .setUrl(API_BASE_URL + "users/get_space_usage");

    req.getHeaderSet()
        .addHeader("Authorization", "Bearer " + _token);

    return retry(req, retries)
        .then(handleDropboxError()).unwrap()
        .then([](boost::future<HTTP::Response> on_res)
        {
            HTTP::Response res = on_res.get();
            auto out = std::make_shared<std::stringstream>();
            auto on_finish = res.setOutStream(*out).finish();

            return on_finish.then(
                [out, res{std::move(res)}] (boost::future<void> future)
                {
                    future.get();

                    json data;
                    data << *out;

                    return Quota(
                        data["used"],
                        data["allocation"]["allocated"]
                    );
                }
            );
        }).unwrap();
}
