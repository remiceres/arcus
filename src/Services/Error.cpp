#include "Services/Error.hpp"

using namespace Services;

Error::Error(std::string message)
: std::runtime_error(message)
{}
