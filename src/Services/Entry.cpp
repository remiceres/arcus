#include "Services/Entry.hpp"

using namespace Services;

Entry::Entry(
    Type type,
    boost::filesystem::path path,
    std::string checksum,
    uint64_t size
)
: _type(type), _path(path), _checksum(checksum), _size(size)
{}

Entry::Type Entry::getType() const
{
    return _type;
}

boost::filesystem::path Entry::getPath() const
{
    return _path;
}

std::string Entry::getChecksum() const
{
    return _checksum;
}

uint64_t Entry::getSize() const
{
    return _size;
}
