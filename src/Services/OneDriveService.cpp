#include "lib/json.hpp"
#include "Services/Error.hpp"
#include "Services/OneDriveService.hpp"
#include "Services/Quota.hpp"
#include "Services/Entry.hpp"
#include "HTTP/Sender.hpp"
#include "HTTP/URL.hpp"

#include <sstream>
#include <ctime>
#include <boost/filesystem.hpp>

using namespace Services;
using json = nlohmann::json;

const std::string OneDriveService::APP_ID =
    "639304b8-563a-4e32-b710-d81c24fc3dd1";
const std::string OneDriveService::APP_PERMISSIONS =
    "files.readwrite%20offline_access";
const std::string OneDriveService::APP_SECRET = "msXDjPY6CrKjwTf3stQVpMU";
const std::string OneDriveService::REDIRECT_URL = "https://example.net";
const std::string OneDriveService::API_BASE_URL =
    "https://graph.microsoft.com/v1.0/me/";
const std::string OneDriveService::AUTH_BASE_URL =
    "https://login.microsoftonline.com/common/oauth2/v2.0/";

OneDriveService::OneDriveService(HTTP::Sender& sender, std::string token)
: Service(sender), _refresh_token(token)
{}

boost::future<void> OneDriveService::login()
{
    std::string url =
        OneDriveService::AUTH_BASE_URL + "authorize?" +
        "client_id=" + OneDriveService::APP_ID + "&" +
        "scope=" + OneDriveService::APP_PERMISSIONS + "&" +
        "response_type=code&" +
        "redirect_uri=" + OneDriveService::REDIRECT_URL;

    std::string final_url;

    std::cout << "Connectez-vous sur l’URL suivante :" << std::endl;
    std::cout << url << std::endl << std::endl;

    std::cout << "Une fois connecté, collez l’URL sur laquelle vous êtes ";
    std::cout << "redirigé :" << std::endl;

    std::cin >> final_url;

    std::size_t left_token_pos = final_url.find_first_of('=') + 1;
    std::string code = final_url.substr(
        left_token_pos,
        final_url.find_first_of('&') - left_token_pos
    );

    return _renewToken(code);
}

boost::future<void> OneDriveService::_renewToken(std::string code)
{
    std::string data =
        "client_id=" + OneDriveService::APP_ID + "&" +
        "redirect_uri=" + OneDriveService::REDIRECT_URL + "&" +
        "client_secret=" + OneDriveService::APP_SECRET + "&";

    if (code.size() > 0)
    {
        data += "code=" + code + "&grant_type=authorization_code";
    }
    else
    {
        data += "refresh_token=" + _refresh_token + "&grant_type=refresh_token";
    }

    HTTP::Request token_request = HTTP::Request()
        .setVerb(HTTP::Verb::POST)
        .setUrl(OneDriveService::AUTH_BASE_URL + "token")
        .setData(data);

    return _sender.send(token_request)
        .then([this](boost::future<HTTP::Response> on_res)
        {
            HTTP::Response res = on_res.get();
            auto out = std::make_shared<std::stringstream>();
            auto on_finish = res.setOutStream(*out).finish();

            return on_finish.then(
                [this, out, res{std::move(res)}](boost::future<void> future)
                {
                    future.get();

                    json data;
                    data << *out;

                    if (data.count("error") == 1)
                    {
                        std::string err = data["error"];

                        if (err == "invalid_grant")
                        {
                            throw boost::enable_current_exception(
                                Errors::AuthenticationFailure(
                                    "Les jetons d’authentification ne sont pas "
                                    "ou plus valides et le serveur OneDrive a "
                                    "refusé de les renouveler."
                                )
                            );
                        }

                        throw boost::enable_current_exception(Error(
                            "Une erreur inattendue de type « " + err + "» a "
                            "été renvoyée par le serveur OneDrive."
                        ));
                    }

                    _access_token = data["access_token"];
                    _refresh_token = data["refresh_token"];
                }
            );
        }).unwrap();
}

boost::future<HTTP::Response> OneDriveService::_sendToOneDrive(
    HTTP::Request req, int retries, bool allow_reauth
)
{
    HTTP::Request auth_req = req;
    auth_req.getHeaderSet()
        .addHeader("Authorization", "Bearer " + _access_token);

    return retry(auth_req, retries).then(
        [this, req, retries, allow_reauth](boost::future<HTTP::Response> on_res)
        {
            HTTP::Response res = on_res.get();
            long code = res.getCode();

            if (code >= 200 && code < 300)
            {
                return boost::make_ready_future(std::move(res));
            }

            if (code == 401)
            {
                if (allow_reauth)
                {
                    return _renewToken().then(
                        [this, req, retries](boost::future<void> on_reauth)
                        {
                            on_reauth.get();
                            return _sendToOneDrive(req, retries, false);
                        }
                    ).unwrap();
                }

                throw boost::enable_current_exception(
                    Errors::AuthenticationFailure(
                        "Les jetons d’authentification ne sont pas ou plus "
                        "valides et ne permettent pas à Arcus de transmettre "
                        "ses requêtes au serveur OneDrive. Cela peut résulter "
                        "d’une révocation des jetons ou de leur expiration."
                    )
                );
            }

            if (code == 500 || code == 503)
            {
                throw boost::enable_current_exception(Errors::ServerUnavailable(
                    "Le serveur OneDrive rencontre des problèmes techniques et "
                    "Arcus ne peut pas communiquer avec lui. Le problème vient "
                    "certainement des serveurs OneDrive. L’envoi a été réessayé"
                    " plusieurs fois sans succès. Si le problème persiste, "
                    "consultez la page de statut du service pour plus "
                    "d'informations (https://portal.office.com/servicestatus)."
                ));
            }

            if (code == 507)
            {
                throw boost::enable_current_exception(Errors::QuotaExceeded(
                    "Le quota d’espace de stockage a été dépassé."
                ));
            }

            if (code == 404 || code == 410)
            {
                throw boost::enable_current_exception(
                    Errors::ResourceUnavailable(
                        "La ressource n’existe pas ou plus sur le serveur."
                    )
                );
            }

            if (code == 403)
            {
                throw boost::enable_current_exception(Errors::PermissionDenied(
                    "Vous n’avez pas le droit d'accéder à cette ressource."
                ));
            }

            if (code != 400)
            {
                throw boost::enable_current_exception(Error(
                    "Le serveur OneDrive a répondu de façon inattendue à la "
                    "requête d’Arcus. Le code de réponse renvoyé est "
                    + std::to_string(code) + "."
                ));
            }

            auto out = std::make_shared<std::stringstream>();
            auto on_finish = res.setOutStream(*out).finish();

            return on_finish.then(
                [this, out, res{std::move(res)}]
                (boost::future<void> future) -> HTTP::Response
                {
                    future.get();

                    if (
                        res.getHeaderSet().getMIMEType()
                        == "application/json"
                    )
                    {
                        json data;
                        data << *out;

                        std::string message = data["error"]["message"];

                        if (
                            message.find("Resource not found")
                            != std::string::npos
                        )
                        {
                            throw boost::enable_current_exception(
                                Errors::ResourceUnavailable(
                                    "Le chemin de la ressource est malformé."
                                )
                            );
                        }
                    }

                    throw boost::enable_current_exception(Error(
                        "Le serveur OneDrive a refusé de répondre à une "
                        "requête qu’il a jugée malformée (" + out->str()
                        + "). Ceci est probablement un bogue de Arcus."
                    ));
                }
            );
        }
    ).unwrap();
}

boost::future<void> OneDriveService::putFile(
    boost::filesystem::path source,
    boost::filesystem::path target,
    unsigned int retries
)
{
    auto in = std::make_shared<std::ifstream>(
        source.string(),
        std::ios::in | std::ios::binary
    );

    HTTP::Request req = HTTP::Request()
        .setVerb(HTTP::Verb::PUT)
        .setUrl(
            OneDriveService::API_BASE_URL + "drive/root:"
                + HTTP::URL::encode(target.string()) + ":/content"
        )
        .setInStream(*in);

    return _sendToOneDrive(req, retries)
        .then([in](boost::future<HTTP::Response> on_res)
        {
            HTTP::Response res = on_res.get();
            auto on_finish = res.finish();

            return on_finish.then(
                [in, res{std::move(res)}](boost::future<void> future)
                {
                    future.get();
                }
            );
        }).unwrap();
}

boost::future<void> OneDriveService::fetchFile(
    boost::filesystem::path source,
    boost::filesystem::path target,
    unsigned int retries
)
{
    HTTP::Request req = HTTP::Request()
        .setVerb(HTTP::Verb::GET)
        .setFollowRedirects(true)
        .setUrl(
            OneDriveService::API_BASE_URL + "drive/root:"
                + HTTP::URL::encode(source.string()) + ":/content"
        );

    req.getHeaderSet()
        .addHeader("Authorization", "Bearer " + _access_token);

    return _sendToOneDrive(req, retries)
        .then([target](boost::future<HTTP::Response> on_res)
        {
            HTTP::Response res = on_res.get();
            auto out = std::make_shared<std::ofstream>(
                target.string(),
                std::ios::out | std::ios::binary
            );
            auto on_finish = res.setOutStream(*out).finish();

            return on_finish.then(
                [out, res{std::move(res)}]
                (boost::future<void> future)
                {
                    future.get();
                }
            );
        }).unwrap();
}

boost::future<void> OneDriveService::removeFile(
    boost::filesystem::path path,
    unsigned int retries
)
{
    std::string url = OneDriveService::API_BASE_URL + "drive/root";

    if (!path.empty())
    {
        url += ":" + HTTP::URL::encode(path.string());
    }

    HTTP::Request req = HTTP::Request()
        .setVerb(HTTP::Verb::DELETE)
        .setUrl(url);

    req.getHeaderSet()
        .addHeader("Authorization", "Bearer " + _access_token);

    return _sendToOneDrive(req, retries)
        .then([](boost::future<HTTP::Response> future)
        {
            future.get();
        });
}

boost::future<std::vector<Entry>> OneDriveService::listDir(
    boost::filesystem::path path,
    unsigned int retries
)
{
    std::string url = OneDriveService::API_BASE_URL + "drive/root";

    if (!path.empty())
    {
        url += ":" + HTTP::URL::encode(path.string()) + ":";
    }

    url += "/children";

    HTTP::Request req = HTTP::Request()
        .setVerb(HTTP::Verb::GET)
        .setUrl(url);

    req.getHeaderSet()
        .addHeader("Authorization", "Bearer " + _access_token);

    return _sendToOneDrive(req, retries)
        .then([path](boost::future<HTTP::Response> on_res)
        {
            HTTP::Response res = on_res.get();

            auto out = std::make_shared<std::stringstream>();
            auto on_finish = res.setOutStream(*out).finish();

            return on_finish.then(
                [out, res{std::move(res)}, path](boost::future<void> future)
                {
                    future.get();

                    json data;
                    data << *out;

                    std::vector<Entry> entries;
                    entries.reserve(data["value"].size());

                    std::transform(
                        std::begin(data["value"]),
                        std::end(data["value"]),
                        std::back_inserter(entries),
                        [path](const json &entry)
                        {
                            if (entry.count("file") == 1)
                            {
                                return Entry(
                                    Entry::Type::FILE,
                                    path / entry["name"].get<std::string>(),
                                    entry["file"]["hashes"]["sha1Hash"],
                                    entry["size"]
                                );
                            }
                            else
                            {
                                return Entry(
                                    Entry::Type::FOLDER,
                                    path / entry["name"].get<std::string>()
                                );
                            }
                        }
                    );

                    return entries;
                }
            );
        }).unwrap();
}

boost::future<Quota> OneDriveService::getQuota(unsigned int retries)
{
    HTTP::Request req = HTTP::Request()
        .setVerb(HTTP::Verb::GET)
        .setUrl(OneDriveService::API_BASE_URL + "drive");

    req.getHeaderSet()
        .addHeader("Authorization", "Bearer " + _access_token);

    return _sendToOneDrive(req, retries)
        .then([](boost::future<HTTP::Response> on_res)
        {
            HTTP::Response res = on_res.get();
            auto out = std::make_shared<std::stringstream>();
            auto on_finish = res.setOutStream(*out).finish();

            return on_finish.then(
                [out, res{std::move(res)}](boost::future<void> future)
                {
                    future.get();

                    json data;
                    data << *out;

                    return Quota(
                        data["quota"]["used"],
                        data["quota"]["total"]
                    );
                }
            );
        }).unwrap();
}
