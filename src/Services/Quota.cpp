#include "Services/Quota.hpp"

using namespace Services;

Quota::Quota(uint64_t used, uint64_t allocation)
: _used(used), _allocation(allocation)
{}

uint64_t Quota::getUsed() const
{
    return _used;
}

uint64_t Quota::getAllocation() const
{
    return _allocation;
}
