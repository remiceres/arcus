#include "Log/Printer.hpp"
#include "Services/Service.hpp"

#include <cmath>

using namespace Services;

Service::Service(HTTP::Sender& sender)
: _sender(sender)
{}

Service::~Service()
{}

boost::future<HTTP::Response> Service::retry(
    HTTP::Request req,
    unsigned int retries,
    uint64_t delay
)
{
    return _sender.send(req).then(
        [this, req, retries, delay](boost::future<HTTP::Response> future)
        {
            HTTP::Response res = future.get();
            long code = res.getCode();

            // Types d'erreur pouvant donner lieu à un nouvel essai d'envoi
            // seulement si le nombre d'éssais n'est pas épuisé
            if (
                (code == 429 || code == 500 || code == 503 || code == 509)
                && retries > 0
            )
            {
                // Indication du serveur concernant le temps à attendre avant
                // le prochain essai
                uint64_t retry_after = std::max(
                    0L, res.getHeaderSet().getHeaderAsInt("Retry-After")
                );

                // Le temps réellement attendu est borné par l’argument donné
                uint64_t real_delay = std::min(retry_after, delay);
                uint64_t next_delay = std::floor(1.5 * real_delay);

                LOG_INFO << "Nouvel essai dans " << real_delay
                    << " secondes.\n";

                HTTP::Request next = req;
                next.setDelay(std::chrono::seconds(real_delay));
                return retry(next, retries - 1, next_delay);
            }

            return boost::make_ready_future(std::move(res));
        }
    ).unwrap();
}
