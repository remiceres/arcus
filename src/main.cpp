#include "Application.hpp"
#include "Log/Printer.hpp"

#include <signal.h>

namespace
{
    // Instance de l’application
    Application app;
}

void onStop(int unused)
{
    app.stop();
}

int main(int argc, char* argv[])
{
    signal(SIGINT, onStop);
    app.start();

    LOG_INFO << "Fin du programme.\n";
    return EXIT_SUCCESS;
}
