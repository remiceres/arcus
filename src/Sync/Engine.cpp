#include "Sync/Engine.hpp"
#include "Log/Printer.hpp"

using namespace Sync;
namespace fs = boost::filesystem;

const std::string Engine::DB_FILE_NAME = ".arcus.db";

Engine::Engine(boost::filesystem::path path):_path(path)
{}

bool Engine::start()
{
    fs::path total_path = _path / DB_FILE_NAME;

    if (sqlite3_open(total_path.c_str(), &_db))
    {
        LOG_ERROR << "Impossible d'ouvrir la base : "
            << total_path << " (" <<  sqlite3_errmsg(_db) << ")\n";
        sqlite3_close(_db);
        return false;
    }

    _checkDatabase();

    //TODO : ouverture des sevices

    return true;
}
