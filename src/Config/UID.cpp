#include "Config/UID.hpp"
#include "Log/Printer.hpp"

#include <boost/uuid/string_generator.hpp>
#include <boost/uuid/random_generator.hpp>
#include <boost/uuid/uuid_io.hpp>

using namespace Config;
namespace uuids = boost::uuids;

uuids::string_generator UID::string_generator;
uuids::random_generator UID::random_generator;

UID::UID()
: _value(random_generator())
{}

UID::UID(const std::string& source)
: _value(string_generator(source))
{}

bool UID::operator==(const UID &right) const
{
    return _value == right._value;
}

bool UID::operator!=(const UID &right) const
{
    return _value != right._value;
}

bool UID::operator<(const UID &right) const
{
    return _value < right._value;
}

bool UID::operator<=(const UID &right) const
{
    return _value <= right._value;
}

bool UID::operator>(const UID &right) const
{
    return _value > right._value;
}

bool UID::operator>=(const UID &right) const
{
    return _value >= right._value;
}

UID::operator std::string() const
{
    return boost::uuids::to_string(_value);
}

bool UID::isNull() const
{
    return _value.is_nil();
}
