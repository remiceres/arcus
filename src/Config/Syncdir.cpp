#include "Log/Printer.hpp"
#include "Config/Syncdir.hpp"

using namespace Config;

const std::vector<std::string> Syncdir::TABLES_LIST
    = {"entry", "entry_parent_of", "service", "chunk", "info"};

const std::vector<std::string> Syncdir::TABLES_REQUESTS = {
    // Requête permettant de créer la table entry
    "CREATE TABLE entry ("
        "id CHARACTER(36) PRIMARY KEY,"
        "name VARCHAR(255),"
        "type VARCHAR(4) CHECK(type IN ('file', 'dir'))"
    ")",

    // Requête permettant de créer la table entry_parent_of
    "CREATE TABLE entry_parent_of ("
        "parent CHARACTER(36),"
        "child CHARACTER(36),"
        "FOREIGN KEY(parent) REFERENCES entry(id) ON DELETE CASCADE,"
        "FOREIGN KEY(child) REFERENCES entry(id) ON DELETE CASCADE,"
        "PRIMARY KEY(parent, child)"
    ")",

    // Requête permettant de créer la table service
    "CREATE TABLE service ("
        "id CHARACTER(36) PRIMARY KEY,"
        "provider VARCHAR(255),"
        "auth VARCHAR(255)"
    ")",

    // Requête permettant de créer la table chunk
    "CREATE TABLE chunk ("
        "id CHARACTER(36) PRIMARY KEY,"
        "hash VARCHAR(255),"
        "chunk_order INTEGER,"
        "entry_id CHARACTER(36),"
        "service_id CHARACTER(36),"
        "FOREIGN KEY(entry_id) REFERENCES entry(id) ON DELETE CASCADE,"
        "FOREIGN KEY(service_id) REFERENCES service(id) ON DELETE CASCADE"
    ")",

    // Requête permettant de créer la table info
    "CREATE TABLE info ("
        "name VARCHAR(255),"
        "version INTEGER"
    ")",
};

Syncdir::Syncdir(boost::filesystem::path path)
: _path(path)
{}

Syncdir::~Syncdir()
{
    sqlite3_close(_handle);
}

bool Syncdir::open()
{
    if (sqlite3_open(_path.c_str(), &_handle))
    {
        LOG_ERROR << "Impossible d'ouvrir la base : " << _path.string()
            << " (" << sqlite3_errmsg(_handle) << ")\n";

        sqlite3_close(_handle);
        return false;
    }

    try
    {
        // Activation de la vérification de l’intégrité des clés étrangères
        _exec("PRAGMA foreign_keys = ON");
    }
    catch (std::runtime_error& err)
    {
        LOG_WARNING << "Impossible d'activer les clés étrangères sur la base : "
            << _path.string() << " (" << err.what() << ")\n";
    }

    return _normalize();
}

std::vector<Row> Syncdir::_exec(std::string req)
{
    std::vector<Row> results;
    char* message = nullptr;

    int error_code = sqlite3_exec(
        _handle,
        req.c_str(),
        [](void* userdata, int length, char** values, char** names)
        {
            auto results = static_cast<std::vector<Row>*>(userdata);
            Row current;

            for( int i = 0 ; i < length ; i++)
            {
                current[names[i]] = values[i];
            }

            results->push_back(current);
            return 0;
        },
        &results,
        &message
    );

    if (error_code)
    {
        std::string message_str = message;
        sqlite3_free(message);

        throw std::runtime_error(
            "Erreur d'exécution de la requête '"
            + req + "' (" + message_str + ")"
        );
    }

    return results;
}

std::string Syncdir::_escapeQuotes(std::string name)
{
    std::string result;
    std::string::iterator start = name.begin();
    std::string::iterator next;

    while ((next = std::find(start, name.end(), '"')) != name.end())
    {
        result.append(start, next);
        result += "\"\"";
        start = next + 1;
    }

    result.append(start, next);
    return result;
}

bool Syncdir::_normalize()
{
    // Récupération de la liste des tables dans la base et accumulation
    // des tables invalides
    auto result = _exec("SELECT name FROM sqlite_master WHERE type = 'table'");
    std::vector<std::string> current_tables;

    std::transform(
        result.begin(), result.end(),
        std::back_inserter(current_tables),
        [](Row& row)
        {
            return row["name"];
        }
    );

    // Suppression des tables qui ne font pas partie de la liste des
    // tables utilisées par le programme
    std::string drop_request = "";

    for (int i = 0; i < current_tables.size(); i++)
    {
        const std::string& name = current_tables[i];

        if (
            std::find(
                Syncdir::TABLES_LIST.begin(),
                Syncdir::TABLES_LIST.end(),
                name
            ) == Syncdir::TABLES_LIST.end()
        )
        {
            drop_request += "DROP TABLE \"" + _escapeQuotes(name) + "\"; ";
            LOG_INFO << "Suppression de la table superflue : " << name << "\n";
        }
    }

    if (drop_request.size() != 0)
    {
        try
        {
            _exec(drop_request);
        }
        catch (std::runtime_error& err)
        {
            LOG_WARNING << "Impossible de supprimer les tables superflues ("
                << err.what() << ")\n";
        }
    }

    // Ajout des tables manquantes dans la base de données
    for (int i = 0; i < Syncdir::TABLES_LIST.size(); i++)
    {
        const std::string& name = Syncdir::TABLES_LIST[i];

        if (
            std::find(
                current_tables.begin(),
                current_tables.end(),
                name
            ) == current_tables.end()
        )
        {
            try
            {
                _exec(Syncdir::TABLES_REQUESTS[i]);
            }
            catch (std::runtime_error& err)
            {
                LOG_ERROR << "Impossible d’ajouter la table manquante : "
                    << name << " (" << err.what() << ")\n";
                return false;
            }

            LOG_INFO << "Création de la table : " << name << "\n";
        }
    }

    return true;
}
