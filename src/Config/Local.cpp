#include "Config/Local.hpp"
#include "Config/UID.hpp"
#include "lib/json.hpp"
#include "Log/Printer.hpp"

#include <fstream>

using namespace Config;
namespace fs = boost::filesystem;
using json = nlohmann::json;

Local::Local(fs::path path)
: _path(path)
{}

Local::Local()
: Local(Local::getDefaultPath())
{}

void Local::addMonitoredDirectory(UID id, fs::path path)
{
    _monitored_directories[id] = path;
    _write();
}

UID Local::addMonitoredDirectory(fs::path path)
{
    UID nextID;
    _monitored_directories[nextID] = path;
    _write();

    return nextID;
}

void Local::removeMonitoredDirectory(UID id)
{
    _monitored_directories.erase(id);
    _write();
}

std::vector<UID> Local::getMonitoredDirectories() const
{
    std::vector<UID> result;
    result.reserve(_monitored_directories.size());

    std::transform(
        _monitored_directories.begin(),
        _monitored_directories.end(),
        std::back_inserter(result),
        [](const std::pair<UID, fs::path>& directory)
        {
            return directory.first;
        }
    );

    return result;
}

const fs::path& Local::getMonitoredDirectoryPath(UID id) const
{
    return _monitored_directories.at(id);
}

const fs::path& Local::getPath() const
{
    return _path;
}

void Local::setPath(const fs::path& path)
{
    _path = path;
}

fs::path Local::getDefaultPath()
{
#ifdef _WIN32
    return fs::path(getenv("APPDATA")) /
        "arcus\\config.json";
#elif __APPLE__
    return fs::path(getenv("HOME")) /
        "Library/Preferences/Arcus/config.json";
#elif defined(__linux__) || defined(__unix__)
    return fs::path(getenv("HOME")) /
        ".config/arcus/config.json";
#else
    #error Impossible de déterminer le répertoire des configurations pour \
    ce système.
#endif
}

bool Local::open()
{
    std::ifstream file(_path.c_str(), std::ios::in);

    if (!file.is_open())
    {
        LOG_INFO << "Impossible de lire le fichier de configuration, "
                    "tentative d'écriture d'un fichier vide.\n";
        return _write();
    }

    // Tentative d’interprétation du contenu du fichier de configuration
    // en tant que fichier JSON
    json data;

    try
    {
        file >> data;
    }
    catch (std::invalid_argument& err)
    {
        LOG_ERROR << "Format invalide du fichier de configuration : "
            << _path.string() << " (" << err.what() << ")\n";
        return false;
    }

    // Lecture de la liste des répertoires à surveiller en une
    // table de hachage associant les UID aux chemins
    _monitored_directories.empty();

    for (
        json::iterator directory = data["monitored_directories"].begin();
        directory != data["monitored_directories"].end();
        ++directory
    )
    {
        std::string value = directory.value();

        try
        {
            _monitored_directories[directory.key()] = fs::path(value);
        }
        catch (std::runtime_error& err)
        {
            LOG_WARNING << "Identifiant unique invalide pour le répertoire : "
                << value << " (identifiant : " << directory.key() << "). Le "
                << "répertoire a été ignoré.\n";
        }
    }

    return true;
}

bool Local::_write() const
{
    fs::create_directories(_path.parent_path());
    std::ofstream file(_path.c_str(), std::ios::out);

    if (!file)
    {
        LOG_ERROR << "Impossible d'écrire le fichier de configuration."
            << " Vérifier les permissions du fichier : " << _path.string()
            << "\n";
        return false;
    }

    json data;

    // Conversion de la table de hachage contenant les répertoires
    // surveillés en JSON
    json monitored_dirs;

    for (auto& pair : _monitored_directories)
    {
        monitored_dirs[pair.first] = pair.second.string();
    }

    data["monitored_directories"] = monitored_dirs;

    // Écriture du JSON dans le fichier
    file << data.dump(4) << '\n';
    return true;
}
