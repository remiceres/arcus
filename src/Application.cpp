#include "Application.hpp"
#include "Log/Printer.hpp"

#include <thread>

Application::Application()
: _is_running(true)
{}

void Application::start()
{
    if (!_config.open())
    {
        return;
    }

    // Redémarre le surveilleur jusqu’à la fin de l’application
    while (_is_running)
    {
        _startMonitor();
    }
}

void Application::stop()
{
    _is_running = false;

    if (_monitor)
    {
        _monitor->stop();
    }
}

void Application::_onEvents(
    const std::vector<fsw::event>& events,
    void *userdata
)
{
    Application* app = static_cast<Application*>(userdata);

    for (const fsw::event& event : events)
    {
        app->_onEvent(event);
    }
}

void Application::_onEvent(const fsw::event& event)
{
    const std::vector<fsw_event_flag>& flags = event.get_flags();
    bool is_noop = std::find(flags.begin(), flags.end(), fsw_event_flag::NoOp)
        != flags.end();

    // Écriture des événements reçus dans le journal si différent des
    // événements inutiles
    if (!is_noop)
    {
        std::string event_string;

        for (const fsw_event_flag& flag : flags)
        {
            event_string += fsw::event::get_event_flag_name(flag) + " ";
        }

        LOG_INFO << "Événement reçu : " << event_string
            << " sur " << event.get_path() << "\n";
    }

    // Modification du fichier de configuration : relecture du fichier
    // et réinitialisation du surveilleur
    if (
        std::find(flags.begin(), flags.end(), fsw_event_flag::Updated) !=
        flags.end() &&
        event.get_path() == _config.getPath()
    )
    {
        LOG_INFO << "Fichier de configuration modifié : rechargement de "
                    "la configuration.\n";

        _config.open();
        _monitor->stop();
    }
}

void Application::_startMonitor()
{
    // Récupération de la liste des répertoires à surveiller depuis
    // le fichier de configuration
    std::vector<Config::UID> directories = _config.getMonitoredDirectories();
    std::vector<std::string> monitored_paths{_config.getPath().string()};
    monitored_paths.reserve(directories.size() + 1);

    std::transform(
        directories.begin(),
        directories.end(),
        std::back_inserter(monitored_paths),
        [this](Config::UID id)
        {
            return _config.getMonitoredDirectoryPath(id).string();
        }
    );

    // Lancement du surveilleur sur les répertoires et la configuration
    _monitor.reset(fsw::monitor_factory::create_monitor(
        fsw_monitor_type::system_default_monitor_type,
        monitored_paths,
        &Application::_onEvents,
        this
    ));

    std::vector<fsw_event_type_filter> allowed_events{
        {fsw_event_flag::NoOp},
        {fsw_event_flag::Created},
        {fsw_event_flag::Updated},
        {fsw_event_flag::Removed},
        {fsw_event_flag::Renamed},
        {fsw_event_flag::MovedFrom},
        {fsw_event_flag::MovedTo},
        {fsw_event_flag::Overflow},
    };

    _monitor->set_recursive(true);
    _monitor->set_event_type_filters(allowed_events);
    _monitor->set_fire_idle_event(true);

    LOG_INFO << "Démarrage de la surveillance de :\n";

    for (const std::string& directory : monitored_paths)
    {
        LOG_INFO << " - " << directory << "\n";
    }

    _monitor->start();
}
