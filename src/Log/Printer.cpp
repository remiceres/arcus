#include "config.hpp"
#include "Log/Printer.hpp"

#include <sstream>
#include <boost/filesystem.hpp>

using namespace Log;
namespace fs = boost::filesystem;

Printer Log::global_printer = Printer();

Printer::Printer(std::ostream* out_stream, Level threshold)
: _out_stream(out_stream), _threshold(threshold)
{}

std::ostream& Printer::print(Level level, std::string file, int line)
{
    // Création d’un chemin relatif à la racine du projet
    std::string path = fs::relative(file, ARCUS_SOURCE_DIR).string();

    if (level >= _threshold)
    {
        // On ne colore que si on écrit vers le terminal
        bool colored = _out_stream == &std::cout || _out_stream == &std::cerr;

        return *_out_stream
            << Printer::timeToString(std::chrono::system_clock::now())
            << " [" << levelToString(level, colored) << "] "
            << path << ":" << std::to_string(line) << " - ";
    }

    return _null_stream;
}

std::ostream* Printer::getOutStream() const
{
    return _out_stream;
}

void Printer::setOutStream(std::ostream *out_stream)
{
    _out_stream = out_stream;
}

Level Printer::getThreshold() const
{
    return _threshold;
}

void Printer::setThreshold(Level threshold)
{
    _threshold = threshold;
}
