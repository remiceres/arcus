#include "Log/Level.hpp"

using namespace Log;

std::string Log::levelToString(Level level, bool colored)
{
    switch (level)
    {
    case Level::INFO:
        return colored ? "\033[32minfo\033[0m" : "info";

    case Level::WARNING:
        return colored ? "\033[33mwarn\033[0m" : "warn";

    case Level::ERROR:
        return colored ? "\033[31merr!\033[0m" : "err!";
    }
}
