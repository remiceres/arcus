cmake_minimum_required(VERSION 3.1)

project(arcus)
set(PROJECT_VERSION_MAJOR 0)
set(PROJECT_VERSION_MINOR 1)

# Compilation en utilisant C++14
set(CMAKE_CXX_STANDARD 14)

# Définitions locales de modules
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/Modules/")

# Recherche des librairies nécessaires
find_package(CURL REQUIRED)
find_package(Threads REQUIRED)
find_package(SQLite3 REQUIRED)
find_package(fswatch REQUIRED)
find_package(Boost 1.58.0 COMPONENTS system filesystem thread REQUIRED)

# Inclusion des en-têtes
include_directories(inc)
include_directories(${CURL_INCLUDE_DIRS})
include_directories(${SQLite3_INCLUDE_DIR})
include_directories(${fswatch_INCLUDE_DIR})
include_directories(${Boost_INCLUDE_DIRS})

# Fichier de configuration permettant de transmettre des informations
# entre le système de construction et le code lui-même
include_directories(${PROJECT_BINARY_DIR}/inc)
configure_file(
    inc/config.hpp.in
    ${PROJECT_BINARY_DIR}/inc/config.hpp
    ESCAPE_QUOTES
)

# Librairie common : fichiers sources communs
set(SOURCES
    src/Services/Quota.cpp
    src/Services/Entry.cpp
    src/Services/DropboxService.cpp
    src/Services/OneDriveService.cpp
    src/Services/Service.cpp
    src/Services/Error.cpp
    src/HTTP/URL.cpp
    src/HTTP/CurlPool.cpp
    src/HTTP/Header.cpp
    src/HTTP/Sender.cpp
    src/HTTP/Request.cpp
    src/HTTP/Response.cpp
    src/HTTP/Verb.cpp
    src/HTTP/Error.cpp
    src/Config/UID.cpp
    src/Config/Local.cpp
    src/Config/Syncdir.cpp
    src/Log/Level.cpp
    src/Log/Printer.cpp
    src/Sync/Engine.cpp
    src/Application.cpp
)

add_library(common ${SOURCES})

# Liste des librairies à lier aux exécutables
set(LIBS
    common
    ${CURL_LIBRARIES}
    ${CMAKE_THREAD_LIBS_INIT}
    ${SQLite3_LIBRARIES}
    ${fswatch_LIBRARIES}
    ${Boost_LIBRARIES}
)

# Exécutables de test
add_executable(test-HTTP tests/HTTP.cpp)
target_link_libraries(test-HTTP ${LIBS})

add_executable(test-HTTP-Error tests/HTTP-Error.cpp)
target_link_libraries(test-HTTP-Error ${LIBS})

add_executable(test-Services tests/Services.cpp)
target_link_libraries(test-Services ${LIBS})

add_executable(test-Services-error tests/Services-error.cpp)
target_link_libraries(test-Services-error ${LIBS})

add_executable(test-UID tests/UID.cpp)
target_link_libraries(test-UID ${LIBS})

add_executable(test-Log tests/Log.cpp)
target_link_libraries(test-Log ${LIBS})

add_executable(test-LocalConfig tests/LocalConfig.cpp)
target_link_libraries(test-LocalConfig ${LIBS})

add_executable(test-SyncdirConfig tests/SyncdirConfig.cpp)
target_link_libraries(test-SyncdirConfig ${LIBS})

add_executable(test-Demo tests/Demo.cpp)
target_link_libraries(test-Demo ${LIBS})

# Exécutable principal
add_executable(${CMAKE_PROJECT_NAME} src/main.cpp)
target_link_libraries(${CMAKE_PROJECT_NAME} ${LIBS})
