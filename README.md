# Arcus

## Informations techniques

### Architecture

![Vue d’ensemble de l’architecture](./docs/modelisation/conception/overview.svg)

### Structure du dépôt

<table>
<tr>
<th colspan="2">Répertoires</th>
</tr>
<tr>
<td><code>inc</code></td>
<td>En-têtes du projet (chaque sous-répertoire correspond à un module)</td>
</tr>
<tr>
<td><code>src</code></td>
<td>Code source (chaque sous-répertoire correspond à un module)</td>
</tr>
<tr>
<td><code>tests</code></td>
<td>Tests unitaires</td>
</tr>
<tr>
<td><code>lib</code></td>
<td>Bibliothèques externes</td>
</tr>
<tr>
<td><code>docs</code></td>
<td>Documents de travail et documentation</td>
</tr>
<tr>
<td><code>image</code></td>
<td>Paramètres de l’image Docker utilisée pour l’intégration continue</td>
</tr>
<tr>
<td><code>cmake</code></td>
<td>Recherche de bibliothèques pour CMake</td>
</tr>
<tr>
<th colspan="2">Fichiers de configuration</th>
</tr>
<tr>
<td><code>CMakeLists.txt</code></td>
<td>Configuration de <a href="https://cmake.org/">CMake</a> pour la construction du projet</td>
</tr>
<tr>
<td><code>Doxyfile</code></td>
<td>Configuration de <a href="http://doxygen.org/">Doxygen</a> pour la <a href="docs/doxygen/html/index.html">documentation générée</a></td>
</tr>
<tr>
<td><code>.gitlab-ci.yml</code></td>
<td>Configuration de <a href="https://docs.gitlab.com/ce/ci/">l’intégration continue sur GitLab</a></td>
</tr>
</table>
