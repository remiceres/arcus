#include "config.hpp"
#include "Log/Printer.hpp"

#include <chrono>
#include <fstream>
#include <thread>
#include <boost/filesystem/path.hpp>

namespace fs = boost::filesystem;

int main(int argc, char** argv)
{
    LOG_INFO << "Démarrage du test de journalisation" << " (concaténation)\n";
    LOG_ERROR << "Une erreur ne s’est pas produite\n";
    LOG_WARNING << "Attention : ce message n’apporte aucune information\n";

    LOG_INFO << "Attente de 2 secondes…\n";
    std::this_thread::sleep_for(std::chrono::seconds(2));

    // Modification du seuil de sévérité
    Log::global_printer.setThreshold(Log::Level::WARNING);
    LOG_INFO << "Ce message ne devrait pas s'afficher !\n";
    LOG_WARNING << "Ce message devrait bien s'afficher ici maintenant.\n";
    LOG_ERROR << "Nous journalisons maintenant dans un fichier\n";

    // Redirection du journal vers un fichier
    fs::path path = ARCUS_SOURCE_DIR / "tests/files/test.log";
    std::ofstream out(path.string());
    Log::global_printer.setThreshold(Log::Level::INFO);
    LOG_INFO << "Le fichier de log est : " << path << "\n";
    Log::global_printer.setOutStream(&out);

    LOG_INFO << "Message d'information dans le fichier.\n";
    LOG_ERROR << "Message d'erreur dans le fichier.\n";
    LOG_WARNING << "Message d'avertissement dans le fichier.\n";
    LOG_INFO << "TERMINÉ\n";

    return EXIT_SUCCESS;
}
