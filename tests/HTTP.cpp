#include "HTTP/Response.hpp"
#include "HTTP/Request.hpp"
#include "HTTP/Sender.hpp"
#include "HTTP/URL.hpp"

#include <iostream>
#include <fstream>
#include <cstring>
#include <chrono>

std::vector<boost::future<HTTP::Response>> send(
    HTTP::Sender& sender,
    const HTTP::Request& req_tpl,
    std::size_t amount = 50
)
{
    // Envoi de 100 requêtes à example.net
    std::vector<HTTP::Request> reqs(amount, req_tpl);
    std::vector<boost::future<HTTP::Response>> futures;

    futures.reserve(amount);

    for (HTTP::Request& req : reqs)
    {
        futures.push_back(sender.send(req));
    }

    return futures;
}

int main(int argc, char* argv[])
{
    // Création de l'envoyeur de requêtes
    HTTP::Sender sender;

    // Activation du mode verbeux si demandé dans les options
    if (argc == 2 && strcmp(argv[1], "-v") == 0)
    {
        std::cout << "Mode verbeux activé.\n\n";
        sender.setVerbose(true);
    }

    // Tests d’encodage et décodage
    {
        std::string str = HTTP::URL::encode("test!?test??");

        if (str != "test%21%3Ftest%3F%3F")
        {
            std::cerr << "Encodage erroné : " << str << std::endl;
            return EXIT_FAILURE;
        }
    }

    {
        std::string str = HTTP::URL::decode("%20AF!");

        if (str != " AF!")
        {
            std::cerr << "Décodage erroné : " << str << std::endl;
            return EXIT_FAILURE;
        }
    }

    // Modèles de requêtes à envoyer
    HTTP::Request tpl_google;
    tpl_google.setVerb(HTTP::Verb::POST);
    tpl_google.setUrl("http://www.traildusud.com");

    HTTP::Request tpl_example;
    tpl_example.setUrl("https://google.com");

    HTTP::Request tpl_delayed;
    tpl_delayed.setUrl("https://www.impots.gouv.fr/portail/");
    tpl_delayed.setDelay(std::chrono::seconds(15));

    auto futures_1 = send(sender, tpl_delayed);
    auto futures_2 = send(sender, tpl_example);
    std::this_thread::sleep_for(std::chrono::seconds(3));
    auto futures_3 = send(sender, tpl_google);

    boost::wait_for_all(std::begin(futures_1), std::end(futures_1));
    boost::wait_for_all(std::begin(futures_2), std::end(futures_2));
    boost::wait_for_all(std::begin(futures_3), std::end(futures_3));

    for (auto& fut : futures_1)
    {
        fut.get();
    }

    for (auto& fut : futures_2)
    {
        fut.get();
    }

    for (auto& fut : futures_3)
    {
        fut.get();
    }

    return EXIT_SUCCESS;
}
