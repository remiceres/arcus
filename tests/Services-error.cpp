#include "HTTP/Sender.hpp"
#include "HTTP/Error.hpp"
#include "Services/Service.hpp"
#include "Services/DropboxService.hpp"
#include "Services/OneDriveService.hpp"
#include "Services/Error.hpp"
#include "tokens.hpp"

#include <boost/filesystem.hpp>

namespace fs = boost::filesystem;

namespace
{
    const fs::path root_file = ARCUS_SOURCE_DIR / "tests/files/Services-error";
    const fs::path normal_file = root_file / "normal.txt";
    const fs::path unreadable_file = root_file / "unreadable.txt";
}

void createFile(fs::path path)
{
    std::ofstream out(path.string());
    out << "Ce fichier de test est généré automatiquement par le test\n";
    out << "test-Services-error.\n";
}

int commonTests(Services::Service& service)
{
    std::cout << "◉ Test d’envoi d’un fichier illisible.\n\n";

    try
    {
        service.putFile(unreadable_file, "/unreadable.txt").get();
        std::cerr << "\n✗ Aucune exception produite.\n\n";
        return EXIT_FAILURE;
    }
    catch (const HTTP::Errors::IO& err)
    {
        std::cout << std::string("\n✓ Exception attrapée (")
            + err.what() + ").\n\n";
    }

    std::cout << "◉ Test d’envoi vers un chemin invalide.\n\n";

    try
    {
        service.putFile(normal_file, "invalid path!?").get();
        std::cerr << "\n✗ Aucune exception produite.\n\n";
        return EXIT_FAILURE;
    }
    catch (const Services::Errors::ResourceUnavailable& err)
    {
        std::cout << std::string("\n✓ Exception attrapée (")
            + err.what() + ").\n\n";
    }

    std::cout << "◉ Test de téléchargement d’un fichier inexistant.\n\n";
    fs::path dest = root_file / "should-not-be-created.txt";

    try
    {
        service.fetchFile("/does-not-exist.txt", dest).get();
        std::cerr << "\n✗ Aucune exception produite.\n\n";
        return EXIT_FAILURE;
    }
    catch (const Services::Errors::ResourceUnavailable& err)
    {
        std::cout << std::string("\n✓ Exception attrapée (")
            + err.what() + ").\n";
    }

    if (fs::exists(dest))
    {
        std::cerr << "✗ Fichier créé lors du téléchargement.\n\n";
        return EXIT_FAILURE;
    }
    else
    {
        std::cout << "✓ Aucun fichier créé.\n\n";
    }

    std::cout << "◉ Test de suppression d’un fichier inexistant.\n\n";

    try
    {
        service.removeFile("/notavalidfile.txt").get();
        std::cerr << "\n✗ Aucune exception produite.\n\n";
        return EXIT_FAILURE;
    }
    catch (const Services::Errors::ResourceUnavailable& err)
    {
        std::cout << std::string("\n✓ Exception attrapée (")
            + err.what() + ").\n";
    }

    return EXIT_SUCCESS;
}

int main(int argc, char* argv[])
{
    HTTP::Sender sender;
    std::cout << "TEST D’ERREURS DU MODULE SERVICES\n";

    // Activation du mode verbeux si demandé dans les options
    if (std::find(argv + 1, argv + argc, std::string("-v")) != argv + argc)
    {
        std::cout << "(Mode verbeux activé.)\n";
        sender.setVerbose(true);
    }

    std::cout << "\n";

    // Nettoyage et création des fichiers de test
    fs::remove_all(root_file);
    fs::create_directories(root_file);
    createFile(normal_file);
    createFile(unreadable_file);

    fs::permissions(unreadable_file, fs::perms::no_perms);
    std::cout << "✓ Fichiers du test créés.\n\n";
    std::cout << "───────────────────────────\n\n";
    std::cout << "TEST DE DROPBOX\n\n";

    {
        std::cout << "◉ Test de jeton expiré.\n\n";
        Services::DropboxService dropbox(
            sender,
            "xzuoZrBG8CAAAAAAAAAAeGYVV5mHc6nGf2De18CiVOhx-KmDnC3oPQfDLrHEiP6b"
        );

        try
        {
            dropbox.putFile(normal_file, "/normal.txt").get();
            std::cerr << "\n✗ Aucune exception produite.\n\n";
            return EXIT_FAILURE;
        }
        catch (const Services::Errors::AuthenticationFailure& err)
        {
            std::cout << std::string("\n✓ Exception attrapée (")
                + err.what() + ").\n\n";
        }
    }

    {
        std::cout << "◉ Test de jeton malformé.\n\n";
        Services::DropboxService dropbox(sender, "invalid-token");

        try
        {
            dropbox.putFile(normal_file, "/normal.txt").get();
            std::cerr << "\n✗ Aucune exception produite.\n\n";
            return EXIT_FAILURE;
        }
        catch (const Services::Errors::AuthenticationFailure& err)
        {
            std::cout << std::string("\n✓ Exception attrapée (")
                + err.what() + ").\n\n";
        }
    }

    {
        Services::DropboxService dropbox(sender, Tests::DROPBOX_TOKEN);

        if (commonTests(dropbox) != EXIT_SUCCESS)
        {
            return EXIT_FAILURE;
        }
    }

    std::cout << "\n───────────────────────────\n\n";
    std::cout << "TEST DE ONEDRIVE\n\n";

    {
        std::cout << "◉ Test de jeton expiré.\n\n";
        Services::OneDriveService onedrive(sender, "MCbbLGu5*uISrEoZGOjcpupjr5tirWyFpQB1YIlgpK8cIySjHqtmGXNO2PVH60PqkLAMTuqBzoebdFVafqihGtg3wDoAcS8Qw32MuHqBQd*zRIEDAfIYGIkeElLuC7rgvj0Z23V*TRWhbHFJ2DP4yF*h4RXSPoYH4qecVoEYSplz7lDb6LMWGGFQ!ieysCSG9Y0t*1mPFDp2Q1AGjOJ1N*3TUMVLkEx6RqJfCGYIQ*3krERvls6Efkds9cgy2oqmAmU1zLiPx7qQ7y*DP5rzDQcu!WRy*BoWJJsVuTWaTErx7mle9Kh2dESVPStTl3zjpVXw8PLMpEa8aEFzKqOCBVedFuyJ!DP4AfJaLMOqvug4qwQS*La08zN7dGU47ZXmd2g$$");

        try
        {
            onedrive.putFile(normal_file, "/normal.txt").get();
            std::cerr << "\n✗ Aucune exception produite.\n\n";
            return EXIT_FAILURE;
        }
        catch (const Services::Errors::AuthenticationFailure& err)
        {
            std::cout << std::string("\n✓ Exception attrapée (")
                + err.what() + ").\n\n";
        }
    }

    {
        std::cout << "◉ Test de jeton malformé.\n\n";
        Services::OneDriveService onedrive(sender, "invalid-token");

        try
        {
            onedrive.putFile(normal_file, "/normal.txt").get();
            std::cerr << "\n✗ Aucune exception produite.\n\n";
            return EXIT_FAILURE;
        }
        catch (const Services::Errors::AuthenticationFailure& err)
        {
            std::cout << std::string("\n✓ Exception attrapée (")
                + err.what() + ").\n\n";
        }
    }

    {
        Services::OneDriveService onedrive(sender, Tests::ONEDRIVE_TOKEN);

        if (commonTests(onedrive) != EXIT_SUCCESS)
        {
            return EXIT_FAILURE;
        }
    }

    return EXIT_SUCCESS;
}
