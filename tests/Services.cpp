#include "Services/DropboxService.hpp"
#include "Services/OneDriveService.hpp"
#include "Services/Quota.hpp"
#include "Services/Entry.hpp"
#include "HTTP/Sender.hpp"

#include <iostream>
#include <fstream>
#include <boost/filesystem/path.hpp>
#include <boost/thread.hpp>

namespace fs = boost::filesystem;

// Crée un fichier à l'emplacement donné avec un contenu de test
// de taille donnée (en kilo-octets)
void createFile(const fs::path& path, std::size_t size)
{
    std::vector<char> data(1024, 1);
    std::ofstream out(path.string(), std::ios::binary | std::ios::out);

    for (std::size_t written = 0; written < size; written++)
    {
        if (!out.write(&data[0], data.size()))
        {
            std::cerr << "Impossible de créer le fichier de test." << std::endl;
            exit(1);
        }
    }
}

void testService(const std::string& name, Services::Service& service)
{
    std::cout << "=== Test du service " << name << " ===\n" << std::endl;

    // Test de récupération des métadonnées
    auto fut_quota = service.getQuota();
    auto fut_dirs = service.listDir("");

    std::cout << "Récupération du quota et du contenu de la racine...";
    std::cout << std::endl;

    Services::Quota quota = fut_quota.get();

    std::cout << "Quota : " << quota.getUsed() / 1e6;
    std::cout << " Mo / " << quota.getAllocation() / 1e6 << " Mo" << std::endl;

    std::vector<Services::Entry> dirs = fut_dirs.get();
    std::cout << "Contenu du dossier racine :" << std::endl;

    for (Services::Entry& entry : dirs)
    {
        std::cout << " - " << entry.getPath() << std::endl;
    }

    // Test d'envoi parallèle de plusieurs fichiers
    std::cout << "Création du fichier de test de 1 Mio..." << std::endl;

    fs::path test_file = ARCUS_SOURCE_DIR / "tests/files"
        / (name + "-test.dat");

    createFile(test_file, 1024);

    static const int COUNT_UPLOADS = 1000;

    std::vector<boost::future<void>> fut_putfile;
    std::vector<std::ifstream> streams_file;

    fut_putfile.reserve(COUNT_UPLOADS);
    streams_file.reserve(COUNT_UPLOADS);

    fs::path target("/");

    service.putFile(test_file,target);

    std::cout << "Envoi de " << COUNT_UPLOADS << " fichiers..." << std::endl;
    boost::wait_for_all(fut_putfile.begin(), fut_putfile.end());

    for (boost::future<void>& future : fut_putfile)
    {
        future.get();
    }

    std::cout << "Envoi terminé ! " << std::endl;
    std::cout << "Appuyez sur une touche pour lancer la suppression...";
    std::cin.get();

    fut_putfile.clear();

    for (int i = 0; i < COUNT_UPLOADS; i++)
    {
        fut_putfile.push_back(service.removeFile("/test-" + std::to_string(i)));
    }

    std::cout << "Suppression des fichiers..." << std::endl;
    boost::wait_for_all(fut_putfile.begin(), fut_putfile.end());

    for (boost::future<void>& future : fut_putfile)
    {
        future.get();
    }

    std::cout << "Suppression terminée !" << std::endl;
    std::cout << "Téléchargement du fichier rapport.pdf..." << std::endl;

    service.fetchFile("/rapport.pdf", ARCUS_SOURCE_DIR / "tests/files/"
        / (name + "-rapport.pdf")).get();

    std::cout << "Téléchargement terminé !" << std::endl;
}

int main(int argc, char* argv[])
{
    HTTP::Sender sender;

    // Activation du mode verbeux si demandé dans les options
    if (argc == 2 && strcmp(argv[1], "-v") == 0)
    {
        std::cout << "Mode verbeux activé.\n\n";
        sender.setVerbose(true);
    }

    Services::DropboxService dropbox(sender, "B-oQg9Pb3FQAAAAAAAADrbsHXrgJGnvz5OJQpxVMgzZiYHV9xgFpML0lrPgQBHd9");
    testService("Dropbox", dropbox);

    // Services::OneDriveService onedrive(sender, "MCbbLGu5*uISrEoZGOjcpupjr5tirWyFpQB1YIlgpK8cIySjHqtmGXNO2PVH60PqkLAMTuqBzoebdFVafqihGtg3wDoAcS8Qw32MuHqBQd*zRIEDAfIYGIkeElLuC7rgvj0Z23V*TRWhbHFJ2DP4yF*h4RXSPoYH4qecVoEYSplz7lDb6LMWGGFQ!ieysCSG9Y0t*1mPFDp2Q1AGjOJ1N*3TUMVLkEx6RqJfCGYIQ*3krERvls6Efkds9cgy2oqmAmU1zLiPx7qQ7y*DP5rzDQcu!WRy*BoWJJsVuTWaTErx7mle9Kh2dESVPStTl3zjpVXw8PLMpEa8aEFzKqOCBVedFuyJ!DP4AfJaLMOqvug4qwQS*La08zN7dGU47ZXmd2g$$");
    // onedrive._renewToken().get();
    // testService("OneDrive", onedrive);

    return 0;
}
