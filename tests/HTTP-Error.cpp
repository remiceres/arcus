#include "HTTP/Error.hpp"
#include "HTTP/Request.hpp"
#include "HTTP/Sender.hpp"
#include "HTTP/Response.hpp"
#include "Log/Printer.hpp"

#include <iostream>
#include <fstream>
#include <boost/filesystem.hpp>

namespace fs = boost::filesystem;

int main(int argc, char** argv)
{

    //création de l'envoyeur de requêtes
    HTTP::Sender sender;

    
    // Activation du mode verbeux si demandé dans les options
    if (argc == 2 && strcmp(argv[1], "-v") == 0)
    {
        std::cout << "Mode verbeux activé.\n\n";
        sender.setVerbose(true);
    }
    // Activation du mode verbeux si demandé dans les options
    if (argc == 2 && strcmp(argv[1], "-v") == 0)
    {
        std::cout << "Mode verbeux activé.\n\n";
        sender.setVerbose(true);
    }

    std::cout << "==============================" << '\n';
    std::cout << "=== Erreur d'entrée sortie ===" << '\n';
    std::cout << "==============================" << "\n\n";


    std::cout << "WRITE_ERROR" << '\n';
    // Création du flux de sortie
    std::ofstream out;

    // Création de la requête
    HTTP::Request req_write = HTTP::Request()
        .setUrl("http://www.httpbin.org/");

    try
    {
        // Envoi de la requête
        HTTP::Response res_write = sender.send(req_write).get();
        res_write.setOutStream(out).finish().get();
    }
    catch (const HTTP::Errors::IO& err)
    {
        std::cout << "IOError : " << err.what() << "\n";
    }
    std::cout << '\n';


    std::cout << "READ_ERROR" << '\n';
    fs::path path = fs::path(ARCUS_SOURCE_DIR) / "tests/files/readerror.txt";
    std::ofstream fichiertest(path.string(), std::ios::out);
    fs::permissions(path, fs::no_perms);


    std::ifstream in(path.string(), std::ios::in);
    HTTP::Request req_read = HTTP::Request()
        .setVerb(HTTP::Verb::PUT)
        .setUrl("http://www.httpbin.org/put")
        .setInStream(in);


    try
    {
        HTTP::Response res_read = sender.send(req_read).get();
        res_read.finish().get();
        std::cout << "Échec\n";
        return EXIT_FAILURE;
    }
    catch(HTTP::Errors::IO& err)
    {
        std::cout << "IOError : " << err.what() << "\n";
    }

    std::cout<< '\n';
    std::cout << "==============================" << '\n';
    std::cout << "====== Erreur de requête =====" << '\n';
    std::cout << "==============================" << "\n\n";

    std::cout << "UNSUPPORTED_PROTOCOL" << '\n';
    HTTP::Request req_TMR = HTTP::Request()
        .setUrl("existepas://test.com");

    try
    {
        sender.send(req_TMR).get();
        std::cout << "Échec\n";
        return EXIT_FAILURE;
    }
    catch(HTTP::Errors::Request& err)
    {
        std::cout << "RequestError : " << err.what() << "\n";
    }
    std::cout << '\n';


    std::cout << "URL_MALFORMAT" << '\n';
    HTTP::Request req_UMF = HTTP::Request()
        .setUrl("http://example.com:-666/");

    try
    {
        sender.send(req_UMF).get();
        std::cout << "Échec\n";
        return EXIT_FAILURE;
    }
    catch(HTTP::Errors::Request& err)
    {
        std::cout << "RequestError : " << err.what() << "\n";
    }



    std::cout<< '\n';
    std::cout << "==============================" << '\n';
    std::cout << "====== Erreur de réseau ======" << '\n';
    std::cout << "==============================" << "\n\n";

    std::cout << "COULDNT_RESOLVE_HOST" << '\n';
    HTTP::Request req_CRH = HTTP::Request()
        .setUrl("http://jenexistepas.com");

    try
    {
        sender.send(req_CRH).get();
        std::cout << "Échec\n";
        return EXIT_FAILURE;
    }
    catch(HTTP::Errors::Network& err)
    {
        std::cout << "NetworkError : " << err.what() << "\n";
    }
    std::cout << '\n';


    LOG_INFO << "Arrêt du wifi";
    system("nmcli radio wifi off");

    std::cout << "COULDNT_CONNECT" << '\n';
    HTTP::Request req_CC = HTTP::Request()
        .setUrl("216.58.210.238");

    try
    {
        sender.send(req_CC).get();
        std::cout << "Échec\n";
        return EXIT_FAILURE;
    }
    catch(HTTP::Errors::Network& err)
    {
        std::cout << "NetworkError : " << err.what() << "\n";
    }

    LOG_INFO << "Mise en route du wifi\n";
    system("nmcli radio wifi on");
    std::cout << '\n';
    std::this_thread::sleep_for(std::chrono::seconds(3));


    std::cout << "OPERATION_TIMEOUT" << '\n';
    HTTP::Request req_OP = HTTP::Request()
        .setUrl("10.255.255.4");

    try
    {
        sender.send(req_OP).get();
        std::cout << "Échec\n";
        return EXIT_FAILURE;
    }
    catch(HTTP::Errors::Network& err)
    {
        std::cout << "NetworkError : " << err.what() << "\n";
    }
    std::cout << '\n';

    std::cout << "==============================" << '\n';
    std::cout << "========= Erreur SSL =========" << '\n';
    std::cout << "==============================" << "\n\n";

    std::cout << "SSL_CONNECT_ERROR" << '\n';
    HTTP::Request req_SCE = HTTP::Request()
        .setUrl("https://traildusud.com");

    try
    {
        sender.send(req_SCE).get();
        std::cout << "Échec\n";
        return EXIT_FAILURE;
    }
    catch(HTTP::Errors::SSL& err)
    {
        std::cout << "SSLError : " << err.what() << "\n";
    }
    std::cout << '\n';

    return EXIT_SUCCESS;
}
