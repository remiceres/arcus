#include "Config/Syncdir.hpp"
#include "config.hpp"

#include <boost/filesystem/path.hpp>

namespace fs = boost::filesystem;

int main(int argc, char** argv)
{
    fs::path path = ARCUS_SOURCE_DIR / "tests/files/config.db";

    // Réinitialisation de la base de test
    fs::remove(path);

    // Création d’une nouvelle base
    Config::Syncdir config(path);
    config.open();

    return EXIT_SUCCESS;
}
