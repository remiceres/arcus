#ifndef ARCUS_TESTS_TOKENS
#define ARCUS_TESTS_TOKENS

namespace Tests
{
    /**
     * Jeton de test du service Dropbox. Permet d’accéder aux ressources
     * du compte de test « arcus-test\@matteodelabre.me ».
     */
    const char* DROPBOX_TOKEN =
        "xzuoZrBG8CAAAAAAAAAAe6Wtr4QGegqqWj_3SgF3ufuRHzKEKLn9hOgoLCWn_g5j";

    /**
     * Jeton de test du service OneDrive. Permet d’accéder aux ressources
     * du compte de test « arcus-test\@matteodelabre.me ».
     */
    const char* ONEDRIVE_TOKEN =
        "MCbM9ZAMUFjEotmMGMvF0DDejT46P5IyZZLU!OUad!xSH6BWXZrzM67GFbA*9O*KKefKrwCWGTXPm7rOogrqPr7QMcKoKhq9Z4zNiBhYALhkhwM4p96xwcIeF8GSkVS*t5ZTXVkgTmoja6eGqFqmSP!7vIroMrSpUeAnToYFo*glvKXmRpikBQyNxmqVCWD4Zp*YnfxPe37IDmXNbEy8r4nl6aOt9zpzn7qK443EGLf6rKB1yYa*D6ELapS9gqdVbGY0cKuAn0E33k87aPyqEhloELI!TtkKq7qffBqJ!Ugsry9CzjEP8viVVh1BnPmk4uFmtVZOhBzIX8K18JYV!iUWZPqMM2XDQyXoHCH5TXg9N8chwCpr1rGU4JSxumXq08A$$";
}

#endif // ARCUS_TESTS_TOKENS
