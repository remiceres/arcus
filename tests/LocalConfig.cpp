#include "Config/Local.hpp"
#include "config.hpp"

#include <cassert>
#include <iostream>
#include <boost/filesystem/path.hpp>

namespace fs = boost::filesystem;

int main(int argc, char** argv)
{
    fs::path path = ARCUS_SOURCE_DIR / "tests/files/config.json";

    // Réinitialisation du fichier de test
    fs::remove(path);

    Config::UID id0;
    Config::UID id1;
    Config::UID id2;
    Config::UID id3;

    {
        // Création d’un nouveau fichier de test
        Config::Local config(path);
        config.open();

        // Ajout de répertoires à surveiller
        config.addMonitoredDirectory(id0, "/test0");
        id1 = config.addMonitoredDirectory("/test1");
        id2 = config.addMonitoredDirectory("/test2");
        id3 = config.addMonitoredDirectory("/test3");
    }

    {
        // Lecture du fichier dans une autre instance
        Config::Local config(path);
        config.open();

        // Vérification de la présence des répertoires ajoutés précédemment
        std::vector<Config::UID> monitored_list(config.getMonitoredDirectories());

        assert(monitored_list.size() == 4);
        assert(std::find(
            monitored_list.begin(),
            monitored_list.end(),
            id0
        ) != monitored_list.end());

        assert(std::find(
            monitored_list.begin(),
            monitored_list.end(),
            id1
        ) != monitored_list.end());

        assert(std::find(
            monitored_list.begin(),
            monitored_list.end(),
            id2
        ) != monitored_list.end());

        assert(std::find(
            monitored_list.begin(),
            monitored_list.end(),
            id3
        ) != monitored_list.end());

        assert(config.getMonitoredDirectoryPath(id0) == "/test0");
        assert(config.getMonitoredDirectoryPath(id1) == "/test1");
        assert(config.getMonitoredDirectoryPath(id2) == "/test2");
        assert(config.getMonitoredDirectoryPath(id3) == "/test3");

        // Modification dans la deuxième instance
        config.removeMonitoredDirectory(id1);
        config.removeMonitoredDirectory(id2);
        config.addMonitoredDirectory(id2, "/newtest");
        config.addMonitoredDirectory(id3, "/replacetest");
    }

    {
        // Vérification de la prise en compte des modifications
        Config::Local config(path);
        config.open();

        std::vector<Config::UID> monitored_list(config.getMonitoredDirectories());

        assert(std::find(
            monitored_list.begin(),
            monitored_list.end(),
            id0
        ) != monitored_list.end());

        assert(std::find(
            monitored_list.begin(),
            monitored_list.end(),
            id2
        ) != monitored_list.end());

        assert(std::find(
            monitored_list.begin(),
            monitored_list.end(),
            id3
        ) != monitored_list.end());

        assert(config.getMonitoredDirectoryPath(id0) == "/test0");
        assert(config.getMonitoredDirectoryPath(id2) == "/newtest");
        assert(config.getMonitoredDirectoryPath(id3) == "/replacetest");
    }

    return EXIT_SUCCESS;
}
