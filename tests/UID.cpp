#include "Config/UID.hpp"

#include <iostream>

int main(int argc, char** argv)
{
    std::cout << "Test conversion identifiant invalide" << std::endl;

    try
    {
        Config::UID invalid("invalide-identifiant");
    }
    catch (std::runtime_error& err)
    {
        std::cout << "Exception attrapée : " << err.what() << std::endl;
    }

    std::cout << "== Génération de 10 identifiants uniques ==" << std::endl;

    for (int i = 0; i < 10; i++)
    {
        Config::UID guid;
        std::cout << (std::string) guid << std::endl;
    }

    return EXIT_SUCCESS;
}
