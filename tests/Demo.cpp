#include "HTTP/Sender.hpp"
#include "Services/Quota.hpp"
#include "Services/Entry.hpp"
#include "Services/DropboxService.hpp"
#include "Services/OneDriveService.hpp"

#include <boost/filesystem/path.hpp>
#include <boost/thread.hpp>
#include <iostream>
#include <fstream>
#include <memory>

namespace fs = boost::filesystem;

/**
 * Demande à l'utilisateur de sélectionner une valeur dans une énumération
 * avec un affichage sous forme de menu.
 *
 * @param title Titre du menu.
 * @param descriptions Association de chaque choix possible à sa description.
 * @return Valeur choisie par l'utilisateur.
 */
template<typename Enum>
Enum selectFromEnum(
    const std::string& title,
    std::map<Enum, std::string> descriptions
)
{
    Enum selected_choice;
    int selected_choice_value = 0;

    std::cout << "───────────────────────────\n\n";
    std::cout << "  ◉ " << title << "\n\n";

    for (std::pair<const Enum, std::string> &choice : descriptions)
    {
        std::cout << static_cast<int>(choice.first) << " - "
            << choice.second << "\n";
    }

    std::cout << "\nQue voulez-vous faire ?\n☞ ";
    std::cin >> selected_choice_value;

    if (!std::cin)
    {
        std::cout << "✗ Vous n'avez pas saisi un nombre valide.\n\n";

        // Vidage du tampon du flux d'entrée contenant les
        // données invalides
        while (!std::cin)
        {
            std::cin.clear();
            std::cin.ignore(
                std::numeric_limits<std::streamsize>::max(),
                '\n'
            );
        }

        return selectFromEnum(title, descriptions);
    }

    selected_choice = static_cast<Enum>(selected_choice_value);

    if (descriptions.count(selected_choice) == 0)
    {
        std::cout << "✗ Le nombre saisi ne fait pas partie des choix.\n\n";
        return selectFromEnum(title, descriptions);
    }

    return selected_choice;
}

/**
 * Types de compte.
 */
enum class AccountChoice
{
    /* Compte Dropbox */
    Dropbox = 1,
    /* Compte OneDrive */
    OneDrive,
    /* Signale la fin de la sélection de compte */
    End
};

namespace
{
    /**
     * Descriptions des types de compte.
     */
    std::map<AccountChoice, std::string> account_choices_text = {
        {AccountChoice::Dropbox, "Connecter un compte Dropbox"},
        {AccountChoice::OneDrive, "Connecter un compte OneDrive"},
        {AccountChoice::End, "Terminer la connexion des comptes"}
    };
}

/**
 * Connecte l'utilisateur à un ensemble de comptes et renvoie un tableau
 * contenant la liste des comptes connectés.
 *
 * @param sender Envoyeur de requêtes HTTP à utiliser.
 * @return Liste des comptes connectés.
 */
std::vector<std::unique_ptr<Services::Service>> connectAccounts(
    HTTP::Sender& sender
)
{
    std::vector<std::unique_ptr<Services::Service>> result;

    do
    {
        AccountChoice choice = selectFromEnum(
            "Connecter des comptes",
            account_choices_text
        );

        std::unique_ptr<Services::Service> service;
        std::string service_name;

        switch (choice)
        {
        case AccountChoice::Dropbox:
            service.reset(new Services::DropboxService(sender));
            service_name = "Dropbox";
            break;

        case AccountChoice::OneDrive:
            service.reset(new Services::OneDriveService(sender));
            service_name = "OneDrive";
            break;

        case AccountChoice::End:
            return result;
        }

        std::cout << "\n";

        try
        {
            service->login().get();
            result.push_back(std::move(service));

            std::cout << "\n✓ Connexion au service " << service_name
                << " réussie !";
        }
        catch (std::exception& err)
        {
            std::cout << "\n✗ Impossible de se connecter au service "
                << service_name << ". Une erreur s'est produite :\n  "
                << err.what();
        }

        std::cout << "\n\n";
    }
    while (true);
}

/**
 * Types d'action.
 */
enum class ActionChoice
{
    /* Récupère le quota d'espace alloué et utilisé sur un compte */
    GetQuota = 1,
    /* Liste les fichiers et sous-répertoires d'un répertoire */
    ListDir,
    /* Télécharge un fichier d'un service vers la machine */
    FetchFile,
    /* Téléverse un fichier de la machine vers un service */
    PutFile,
    /* Supprime un fichier ou répertoire distant */
    RemoveFile,
    /* Signale la fin de la sélection des actions */
    End
};

namespace
{
    /**
     * Descriptions des types d'actions.
     */
    std::map<ActionChoice, std::string> action_choice_text = {
        {ActionChoice::GetQuota, "Obtenir les quotas des comptes"},
        {ActionChoice::ListDir, "Lister le contenu d'un répertoire"},
        {ActionChoice::FetchFile, "Télécharger un fichier distant"},
        {ActionChoice::PutFile, "Téléverser un fichier local"},
        {ActionChoice::RemoveFile, "Supprimer un fichier distant"},
        {ActionChoice::End, "Terminer le programme"}
    };
}

void doActions(
    std::vector<std::unique_ptr<Services::Service>>& services
)
{
    do
    {
        ActionChoice choice = selectFromEnum(
            "Effectuer des actions",
            action_choice_text
        );

        boost::future<void> on_finished;

        switch (choice)
        {
        case ActionChoice::GetQuota:
        {
            // Lancement des requêtes d'obtention des quotas de chaque service
            std::vector<boost::future<Services::Quota>> futures_list;
            futures_list.reserve(services.size());

            std::transform(
                std::begin(services),
                std::end(services),
                std::back_inserter(futures_list),
                [](const std::unique_ptr<Services::Service> &service)
                {
                    return service->getQuota();
                }
            );

            on_finished = boost::when_all(
                std::begin(futures_list),
                std::end(futures_list)
            ).then(
                [](
                    boost::future<std::vector<boost::future<Services::Quota>>>
                    on_on_quotas
                )
                {
                    unsigned int i = 1;
                    std::vector<boost::future<Services::Quota>> on_quotas
                        = on_on_quotas.get();

                    std::cout << "\n";

                    for (boost::future<Services::Quota>& on_quota : on_quotas)
                    {
                        Services::Quota quota = on_quota.get();

                        std::cout << "Service n°" << i << " - "
                            << quota.getUsed() / 1048576.l
                            << " Mio utilisés sur "
                            << quota.getAllocation() / 1048576.l << " Mio\n";

                        ++i;
                    }

                    std::cout << "\n";
                }
            );

            break;
        }
        case ActionChoice::ListDir:
        {
            std::string path;
            std::cout << "Entrez le chemin du répertoire à lister :\n☞ ";
            std::cin >> path;
            std::cout << "\n";

            if (path == "/")
            {
                path = "";
            }

            // Lancement des requêtes de listage de chaque service
            std::vector<boost::future<std::vector<Services::Entry>>>
                futures_list;
            futures_list.reserve(services.size());

            std::transform(
                std::begin(services),
                std::end(services),
                std::back_inserter(futures_list),
                [path](const std::unique_ptr<Services::Service> &service)
                {
                    return service->listDir(path);
                }
            );

            on_finished = boost::when_all(
                std::begin(futures_list),
                std::end(futures_list)
            ).then(
                [path](
                    boost::future<std::vector<
                        boost::future<std::vector<Services::Entry>>
                    >> on_on_entries_list
                )
                {
                    unsigned int i = 1;
                    std::vector<boost::future<std::vector<Services::Entry>>>
                        on_entries_list = on_on_entries_list.get();

                    std::cout << "\n";

                    for (boost::future<std::vector<Services::Entry>> &on_entries
                            : on_entries_list)
                    {
                        std::vector<Services::Entry> entries = on_entries.get();

                        std::cout << "Service n°" << i << " - Contenu du "
                            << "répertoire " << path << "\n";

                        for (const Services::Entry& entry : entries)
                        {
                            std::cout << " - " << entry.getPath() << "\n";
                        }

                        if (entries.size() == 0)
                        {
                            std::cout << " (vide)\n";
                        }

                        std::cout << "\n";
                        ++i;
                    }
                }
            );

            break;
        }
        case ActionChoice::FetchFile:
        {
            std::string path;
            std::cout << "Entrez le chemin du fichier à télécharger :\n☞ ";
            std::cin >> path;
            std::cout << "\n";

            // Ouverture des flux d'écriture vers les fichiers locaux
            std::string filename = fs::path(path).filename().string();
            std::vector<fs::path> files;

            files.reserve(services.size());

            for (unsigned int i = 1; i <= services.size(); ++i)
            {
                files.push_back(
                    ARCUS_SOURCE_DIR / "tests/files/"
                        / (std::to_string(i) + "-" + filename)
                );
            }

            // Lancement des requêtes de téléchargement depuis chaque service
            std::vector<boost::future<void>> futures_list;
            futures_list.reserve(services.size());

            unsigned int i = 0;

            std::transform(
                std::begin(services),
                std::end(services),
                std::back_inserter(futures_list),
                [path, &files, &i](
                    const std::unique_ptr<Services::Service>& service
                ) mutable
                {
                    return service->fetchFile(path, files[i++]);
                }
            );

            on_finished = boost::when_all(
                std::begin(futures_list),
                std::end(futures_list)
            ).then(
                [](boost::future<std::vector<boost::future<void>>> future)
                {
                    for (boost::future<void> &sub_future : future.get())
                    {
                        sub_future.get();
                    }

                    std::cout << "\n✓ Téléchargement terminé !\n\n";
                }
            );

            break;
        }
        case ActionChoice::PutFile:
        {
            std::string path;
            std::cout << "Entrez le chemin du fichier à téléverser :\n☞ ";
            std::cin >> path;
            std::cout << "\n";

            // Ouverture des flux de lecture depuis les fichiers locaux
            fs::path full_path = ARCUS_SOURCE_DIR / "tests/files" / path;

            // Lancement des requêtes de téléversement vers chaque service
            std::vector<boost::future<void>> futures_list;
            futures_list.reserve(services.size());

            unsigned int i = 0;

            std::transform(
                std::begin(services),
                std::end(services),
                std::back_inserter(futures_list),
                [path, full_path, &i](
                    const std::unique_ptr<Services::Service>& service
                ) mutable
                {
                    return service->putFile(full_path, path);
                }
            );

            on_finished = boost::when_all(
                std::begin(futures_list),
                std::end(futures_list)
            ).then(
                [](boost::future<std::vector<boost::future<void>>> future)
                {
                    for (boost::future<void> &sub_future : future.get())
                    {
                        sub_future.get();
                    }

                    std::cout << "\n✓ Téléversement terminé !\n\n";
                }
            );

            break;
        }
        case ActionChoice::RemoveFile:
        {
            std::string path;
            std::cout << "Entrez le chemin du fichier à supprimer :\n☞ ";
            std::cin >> path;
            std::cout << "\n";

            // Lancement des requêtes de suppression depuis chaque service
            std::vector<boost::future<void>> futures_list;
            futures_list.reserve(services.size());

            std::transform(
                std::begin(services),
                std::end(services),
                std::back_inserter(futures_list),
                [path](const std::unique_ptr<Services::Service>& service)
                {
                    return service->removeFile(path);
                }
            );

            on_finished = boost::when_all(
                std::begin(futures_list),
                std::end(futures_list)
            ).then(
                [](boost::future<std::vector<boost::future<void>>> future)
                {
                    for (boost::future<void> &sub_future : future.get())
                    {
                        sub_future.get();
                    }

                    std::cout << "\n✓ Suppression effectuée !\n\n";
                }
            );

            break;
        }
        case ActionChoice::End:
            return;
        }

        try
        {
            on_finished.get();
        }
        catch (std::exception& err)
        {
            std::cout << "\n✗ Impossible de réaliser l'action demandée. "
                << "Une erreur s'est produite :\n  " << err.what() << "\n\n";
        }
    }
    while (true);
}

int main(int argc, char* argv[])
{

    std::cout << "DÉMONSTRATION DES MODULES SERVICES ET HTTP\n\n";

    HTTP::Sender sender;

    // Activation du mode verbeux si demandé dans les options
    if (argc == 2 && strcmp(argv[1], "-v") == 0)
    {
        std::cout << "Mode verbeux activé.\n\n";
        sender.setVerbose(true);
    }
    
    std::vector<std::unique_ptr<Services::Service>> services
        = connectAccounts(sender);

    if (services.size() > 0)
    {
        doActions(services);
    }
    else
    {
        std::cout << "✗ Aucun service ajouté.\n";
    }

    return EXIT_SUCCESS;
}
